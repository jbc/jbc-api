class XdrpcController < ApplicationController
  layout false
  def show
    # hard code since easyxdm doesn't change per environment, just put it in staging cloudfront and be done with it
    @host = "//d2qcl68vqp717u.cloudfront.net"
    if ENV['RAILS_ENV'] != 'development'
      @version = "min"
    else
      @version = "debug"
    end
    expires_in 1.year, public: true

    # remove X-Frame-Options header that's added by default by Rails 4 because
    # it'd break easyXDM
    response.headers.except! 'X-Frame-Options'
  end
end