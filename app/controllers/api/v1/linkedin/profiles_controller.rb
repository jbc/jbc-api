class Api::V1::Linkedin::ProfilesController < Api::V1::Linkedin::BaseController

  # we authorize by filter, so we have to skip CanCan
  skip_authorization_check only: [:index, :show]

  before_filter :require_user_authentication
  before_filter :add_http_caching

  caches_action :show, cache_path: :profile_cache_path, expires_in: 1.day

  # Display linkedin profile of a user. Currently logged in user profile is available under me.json
  # similar to LinkedIn API. Current user profile will include email_address field. Other profiles
  # will include related connection fields
  def show
    raise CanCan::AccessDenied unless params[:id]
    if params[:id] == 'me'
      logger.info "Fetching LinkedIn profile of currently logged in user: #{current_session.user_id}"
      @profile = wrap_with_openstruct(linkedin_client.profile(fields: ["#{CURRENT_PROFILE_FIELDS.join(",")}"]))
    else
      logger.info "Fetching LinkedIn profile by LinkedIn id: #{params[:id]}"
      @profile = wrap_with_openstruct(linkedin_client.profile(id: params[:id], fields: ["#{CONNECTION_WITH_RELATED_FIELDS.join(",")}"]))
    end
    logger.info "Profile: #{params[:id]} is private" if @profile and @profile.id == 'private'
  end

  private
  # return caching path for show profile action. If we receive me.json request, replace 'me' with linkedin
  # id of currently logged in user. Also include job board uuid because linkedin id is not unique across application
  def profile_cache_path_url
    if params[:id] == 'me'
      "job_boards/#{params[:job_board_id]}/linkedin/profiles/#{current_session.user_id}_private"
    else
      "job_boards/#{params[:job_board_id]}/linkedin/profiles/#{params[:id]}"
    end
  end
end