require 'ostruct'
require 'uri'
class Api::V1::Linkedin::ConnectionsController < Api::V1::Linkedin::BaseController
 
  skip_authorization_check only: [:related, :index, :show]

  before_filter :require_user_authentication
  before_filter :add_http_caching

  caches_action :index, cache_path: :connections_by_company_cache_path, expires_in: 1.day

  caches_action :show, cache_path: :connection_cache_path, expires_in: 1.day

  # List connections of currently logged in user
  # This method requires an param[:company] otherwise an CanCan::AccessDenied will be thrown. This may change in the future
  # If the user has both first and second degree connections at the company, only first degree connections will be returned.
  # Connection information can be private based on user settings. As of version 1.2, private connections will be skipped
  # because we cannot identify which bucket the connection belong to
  def index
    raise ArgumentError, 'Company parameter is missing' unless params[:company]

    logger.info "Finding company=\"#{params[:company]}\" connections for linkedin_user=#{current_user.linkedin_id}"

    results = linkedin_client.search({fields: ["people:(#{CONNECTION_FIELDS.join(",")}),num-results,facets:(code,buckets:(code,name,count))"],
                              company_name: URI.escape(params[:company]).sub('&', '%26'),
                              sort: 'distance',
                              facets: 'network'})

    f_count = s_count = 0
    f_people = s_people = []

    if results['total_results'] == 0
      logger.info "No connections returned for company: #{params[:company]} for user: #{current_user.linkedin_id}"
    else
      # count and connections list for first degree & second degree connections
      network_facets = results['facets']['all'][0]['buckets']['all']
      f_count,s_count = network_facets[0]['count'], network_facets[1]['count']

      logger.info "LinkedIn User: #{current_user.linkedin_id} has #{f_count} first connections & #{s_count} second connections at #{params[:company]}"

      # go through connections and categorize them by degree
      f_people = []
      s_people = []
      results['people']['all'].each do |person|
        next if person.id == 'private' # See above docs about private connection
        f_people << person if person['relation-to-viewer']['distance'] == 1
        s_people << person if person['relation-to-viewer']['distance'] == 2
      end
    end

    @firstDegreeCount = f_count

    @secondDegreeCount = s_count

    @connections = (f_people.empty? ? s_people : f_people).map do |connection|
      insert_connection_show_url(connection)
      wrap_with_openstruct(connection)
    end
  end

  # Given an id of a connection, display more information about it than what is returned in #index method
  # as of version 1.2, the only difference is this api include related_connections fields. The connection information
  # can be private based on user settings. Private connections will be returned as is and it's the job of JS front end
  # to properly handle it.
  def show
    raise CanCan::AccessDenied unless current_session
    logger.info "Fetching information about connection=#{params[:id]} for user_id=#{current_session.user_id}"

    @connection = wrap_with_openstruct(linkedin_client.profile(id: params[:id],
                                                       fields: ["#{CONNECTION_WITH_RELATED_FIELDS.join(",")}"]))
    logger.info "connection=#{params[:id]} is private" if @connection.id == 'private'
  end

  private
  # Given a linkedin connection, inject an connection_url property that point to the show url of that particular connection
  # Connection needs to be not nil and has an id, otherwise nothing happens
  # This method will modify the original connection and doesn't return the connection object
  def insert_connection_show_url(connection)
    unless connection.nil? or connection.id.nil? then
      connection[:connection_url] = api_v1_job_board_linkedin_connection_url(job_board.uuid, connection.id)
    end
  end

  # return caching path for connection search by company name, e.g. index action. Cache path include job board uuid,
  # current user uuid and company name param
  def connections_by_company_cache_path_url
    "job_boards/#{params[:job_board_id]}/linkedin/connections?company=#{params[:company]}-user_id=#{current_session.user_id}"
  end

  def connection_cache_path_url
    "job_boards/#{params[:job_board_id]}/linkedin/connections/#{params[:id]}-user_id=#{current_session.user_id}"
  end
end
