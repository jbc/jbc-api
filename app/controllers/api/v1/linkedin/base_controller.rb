class Api::V1::Linkedin::BaseController < Api::V1::BaseController
  OAUTH1_OPTIONS = {request_token_path: "/uas/oauth/requestToken?scope=r_basicprofile+r_emailaddress+r_network"}

  skip_authorization_check only: [:callback, :new, :destroy]

  before_filter :mock_if_load_testing

  protected
  def require_user_authentication
    raise CanCan::AccessDenied unless current_session
  end

  def add_http_caching
    expires_in 1.day, public: false
  end

  # If load testing, we will need to
  # - authenticate client, making sure current_client is admin
  # - mock all linkedin calls and return appropriate data that other code is expecting
  # - put in delay of a few hundreds milliseconds to have realistic load test
  def mock_if_load_testing
    if params[:load_test]
      # just having params[:load_test] is not enough for security reason,
      # need to verify if auth_token of an admin is provided
      return unless current_client.admin?

      @client = MockedLinkedInClient.new(self)

      # add 300 milliseconds delay to be realistic when load testing
      sleep 0.3
    end
  end

  def access_denied_handler(exception)
    log_linkedin_error(exception)
    super
  end

  def internal_server_error_handler(exception)
    log_linkedin_error(exception)
    super
  end

  def log_linkedin_error(exception)
    if exception.is_a? LinkedIn::Errors::LinkedInError
      msg = "LinkedIn Error raised. Data: #{exception.data}"
      if current_user
        msg += ", User: #{current_user.uuid}, session: #{current_user.session}"
      end
      logger.error msg
    end
  end
end

class MockedLinkedInClient

  def initialize(controller)
    @controller = controller
  end

  def authorize_from_access(access_secret, access_token)
  end

  def profile(options = {})
    linkedin_profile = ActiveSupport::OrderedOptions.new
    linkedin_profile.id = @controller.params[:linkedin_id]
    linkedin_profile
  end

  def authorize_from_request(request_token, request_secret, verifier)
    ["Foo", "Bar"]
  end

  def request_token(options = {})
    request_token = OAuth::RequestToken.new nil, "LOAD_TEST_#{UUIDTools::UUID.random_create.to_s}", UUIDTools::UUID.random_create.to_s
    class <<request_token
      def authorize_url
        token
      end
    end
    request_token
  end
end