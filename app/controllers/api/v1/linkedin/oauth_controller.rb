class Api::V1::Linkedin::OauthController < Api::V1::Linkedin::BaseController

  skip_authorization_check only: [:switch_user]
  # This method is called after LinkedIn user has authorized our api to access their data.
  # LinkedIn will send to us two parameter <code>oauth_verifier</code< and <code>oauth_token</code>.
  # If either of these parameter is missing, an <code>ArgumentError</code> will be raised. This method
  # will create a new user if doesn't already exist based on LinkedIn id and create new session for the user
  # and drops it to user's cookies.
  def callback
    if params[:oauth_problem]
      logger.warn "LinkedIn error=#{params[:oauth_problem]}"
      redirect_to "#{job_board.linkedin_redirect_url}&error=#{params[:oauth_problem]}"
      return
    end

    pin = params[:oauth_verifier]
    rtoken = params[:oauth_token]

    unless pin and rtoken
      logger.info 'Either oauth_verifier or oauth_token is missing'
      raise ArgumentError, 'Either oauth_verifier or oauth_token is missing'
    end
    logger.info "Received callback request from LinkedIn for oauth token: #{rtoken} and pin: #{pin}"

    unless cache_entry = get_request_token_and_destroy(rtoken)
      msg = "Cannot find request cache_entry object for request_token=#{rtoken}"
      logger.info msg
      raise ArgumentError, msg
    end
    rsecret = cache_entry[:request_secret]

    begin
      atoken, asecret = linkedin_client.authorize_from_request(rtoken, rsecret, pin)
      atoken_expires_at = 60.days.from_now
      linkedin_profile = linkedin_client.profile(:fields => %w(id))
    rescue LinkedIn::Errors::LinkedInError => error
      logger.error "Unable to authorize user with LinkedIn. rtoken=#{rtoken}"
      raise error
    end
    logger.info "linkedin_user=#{linkedin_profile.id} is authorized"

    if user = job_board.users.find_by_linkedin_id(linkedin_profile.id)
      logger.info "user=#{user.uuid} is associated with linkedin_id=#{linkedin_profile.id}"
    else
      logger.info "No user found for linkedin_id=#{linkedin_profile.id}"
      user = User.create!(linkedin_id: linkedin_profile.id, job_board_id: job_board.id)
      logger.info "New user=#{user.uuid} is created for linkedin_id=#{linkedin_profile.id}"
    end

    user.create_new_session({linkedin_atoken: atoken, linkedin_asecret: asecret, linkedin_expires_at: atoken_expires_at})

    logger.info "Setting cookie for session=#{user.session}"
    cookies.permanent.signed[job_board.session_cookie.to_sym] = user.session.uuid

    # if job board has provided a redirect url, use that instead of job_board.linkedin_redirect_url
    url = cache_entry[:redirect_url] || job_board.linkedin_redirect_url
    redirect_to "#{url}#{user.session.uuid}"
  end

  # Return LinkedIn oauth url or renew url if there is a current user. If format is html, we'll redirect to the url
  # If user_uuid param is given from a job board, we'll make sure current user is the same as the requested user,
  # otherwise, we'll redirect to job board's homepage url. This scenario is not expected to happen frequently, so log them
  def new
    request_token = create_request_token()
    @url = request_token.authorize_url

    respond_to do |format|
      format.html { redirect_to @url }
      format.json do
      end
    end
  end

  # given a secured token belonging to a user, log the new user in, e.g.
  # update the current cookie to new user's session if an session exists and has linkedin
  # access
  def switch_user
    user = nil
    if params[:user_uuid] and params[:signature] and params[:timestamp]
      Rails.logger.info "Received client_signature=#{params[:signature]} for user_uuid=#{params[:user_uuid]}"
      timestamp = Time.now().to_i
      data = "#{params[:user_uuid]}#{params[:timestamp]}"
      hmac = OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new("sha256"), job_board.client.authentication_token, data)
      our_signature = Base64.encode64(hmac).strip()
      if params[:signature] == our_signature
        if (time_difference = timestamp - params[:timestamp].to_i) < 10
          user = User.find_by_uuid(params[:user_uuid])
        else
          Rails.logger.warn("Signature matches but time_difference=#{time_difference} is over limit")
        end
      else
        Rails.logger.warn("signature=#{our_signature} is not the same as client_signature=#{params[:signature]}")
      end
    end
    if user and user.session.has_linkedin_access?
      cookies.permanent.signed[job_board.session_cookie.to_sym] = user.session.uuid
      render json: {message: "OK"}, status: 200
    else
      raise CanCan::AccessDenied
    end
  end

  private
  # Get request token from LinkedIn and create an oauth token
  def create_request_token
    logger.info "Requesting new oauth request token for job_board=#{job_board.name}"
    request_token = linkedin_client.request_token(oauth_callback: callback_api_v1_job_board_linkedin_oauth_url(job_board.uuid))
    logger.info "request_token=#{request_token.token} is created for job_board=#{@job_board.name}"
    Rails.cache.write(request_token_cache_path(request_token.token), {request_secret: request_token.secret, redirect_url: params['redirect_url']})
    return request_token
  end

  # cache path for request token. This is not for caches_action. This is use to store request token cross requests
  def request_token_cache_path(token)
    "linkedin/oauth/request_token/#{token}"
  end

  def get_request_token_and_destroy(token)
    path = request_token_cache_path(token)
    cache_entry = Rails.cache.read(path)
    Rails.cache.delete(path)
    return cache_entry
  end
end
