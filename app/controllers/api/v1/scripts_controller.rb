class Api::V1::ScriptsController < Api::V1::BaseController
  layout false

  before_filter :set_variables_and_cache

  skip_authorization_check :only => [:show]

  def show
  end

  private
  # override job_board method to fetch job board & js_version in 1 query
  def job_board
    @job_board ||= JobBoard.active.joins(:js_version).where(uuid: params[:job_board_id]).select("job_boards.*, js_versions.version")[0]
  end

  # this filter set all necessary filter and also add http header tags. this is to make sure http header is
  # always set whether show action gets to run or not
  def set_variables_and_cache
    unless job_board
      render nothing: true
      return
    end

    @js_host = ENV['JBC_JS_HOST']
    @js_version = job_board.version
    @minify = true
    @host ||= (root_url only_path: false)[0..-2]
    if ENV['RAILS_ENV'] != "development"
      easyXDM_env = "min.js"
    else
      easyXDM_env = "debug.js"
    end
    @easyXDM_path = ActionController::Base.helpers.asset_path("vendor/easyXDM/easyXDM.#{easyXDM_env}", only_path: false)

    #load templates
    @templates = job_board.templates
    logger.info "Returning #{@templates.length} templates for job_board=#{params[:job_board_id]}"

    expires_in 1.day, public: true
    fresh_when job_board
  end
end