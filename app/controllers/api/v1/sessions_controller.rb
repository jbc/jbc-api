class Api::V1::SessionsController < Api::V1::BaseController
  load_resource :find_by => :uuid

  authorize_resource

  caches_action :show, cache_path: :session_cache_path

  # display a session identified by its uuid in params[:id]
  # Because this controller uses cancan load_resource, @session is already set to the correct session and 404
  # will be returned if it cannot be found
  # However, we still need to make sure that the user of the session belong to one of the current client's job boards
  def index
    @user = User.find_by_uuid(params[:user_uuid])
    if @user and @user.session
      raise CanCan::AccessDenied unless current_client and (current_client.admin? or @user.job_board.client_id == current_client.id)
      @sessions = [@user.session]
    else
      raise ActiveRecord::RecordNotFound
    end
  end

  def show
    @user = User.find(@session.user_id)
    raise CanCan::AccessDenied unless current_client and (current_client.admin? or @user.job_board.client_id == current_client.id)
  end

  private
  def session_cache_path_url
    "#{current_client.cache_key}/job_boards/#{params[:job_board_id]}/#{@session.cache_key}" # Stale result will occur when job board's client is changed but that should never happen
  end

end
