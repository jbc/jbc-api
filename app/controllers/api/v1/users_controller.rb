class Api::V1::UsersController < Api::V1::BaseController
  # only authenticate user (stored in cookies) for show action because
  # we don't look at cookies value in index action
  before_filter :authenticate_user_unless_show_by_id, only: :show

  before_filter :load_user, only: [:show, :update]

  skip_authorization_check only: :cor

  authorize_resource

  # cache if we are searching by linkedin_id and auth_token is not provided
  # override the cache path because rails by default ignores url params
  caches_action :index, if: Proc.new { |x| true if params[:linkedin_id] and not current_client },
                cache_path: :users_by_linkedin_id_cache_path

  caches_action :show, if: Proc.new { |x| true if params[:id] == '~' },
                cache_path: :show_current_user_cache_path

  def index
    # this api is only for authenticated client otherwise a linkedin_id has to be provided
    raise CanCan::AccessDenied unless current_client or params[:linkedin_id]

    if params[:linkedin_id]
      logger.info "Finding user by linkedin_id=#{params[:linkedin_id]}"
      if user = job_board.users.find_by_linkedin_id(params[:linkedin_id])
        logger.info "User #{user.uuid} is found with linkedin_id=#{params[:linkedin_id]}"
        @users = [user]
        add_caching(user)
      else
        logger.info "No user found with linkedin_id=#{params[:linkedin_id]}"
        @users = []
      end
    elsif current_client
      # todo I don't think this api is being used by any client
      # CanCan somehow can't authorize @users, so do it manually here
      # it's better to be specific and test against and instance of user
      # however, job_board may not have any users, so do this ugly check here
      if job_board.users.first
        authorize! :index, job_board.users.first
      else
        authorize! :index, job_board
      end

      logger.info "Returning all users for job board #{job_board.uuid}"
      @users = job_board.users
    end
  end

  # Load user and assign to @user. params[:id] can be ~ for current user or uuid
  def load_user
    if params[:id] == '~'
      logger.info 'Finding currently logged in user'
      raise CanCan::AccessDenied unless current_user
      @user = current_user
    else
      logger.info "Finding user by user_uuid=#{params[:id]}"
      @user = User.find_by_uuid(params[:id])
    end
    raise ActiveRecord::RecordNotFound unless @user
  end

  def show
    if params[:id] != '~'
      add_caching(@user)
    else
      # add social network profile
      @linkedin_profile = wrap_with_openstruct(linkedin_client.profile(fields: ["#{CURRENT_PROFILE_FIELDS.join(",")}"]))
    end
  end

  # update user's type
  def update
    logger.info "Updating user_type of user=#{@user.uuid} to #{params[:user][:user_type]} "
    @user.user_type = params[:user][:user_type]
    @user.save!
  end

  private

  def add_caching(user)
    expires_in 1.year, public: true
    fresh_when user
  end
  #  if using ~ in url for current user, get uuid of current user from sessions
  def authenticate_user_unless_show_by_id
    if params[:id] == '~'
      require_user_authentication
    end
  end

  def users_by_linkedin_id_cache_path_url
    "users?linkedin_id=#{params[:linkedin_id]}"
  end

  def show_current_user_cache_path_url
    "#{current_user.cache_key}"
  end
end
