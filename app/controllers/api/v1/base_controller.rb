require 'actionpack/action_caching'

class Api::V1::BaseController < ActionController::Base
  respond_to :json, :xml
  self.responder = ApiResponder

  BASE_CONNECTION_FIELDS = %w(id first-name last-name formatted-name headline picture-url public-profile-url)

  CURRENT_PROFILE_FIELDS = BASE_CONNECTION_FIELDS + %w(email-address relation-to-viewer:(distance))

  CONNECTION_FIELDS = BASE_CONNECTION_FIELDS + %w(relation-to-viewer:(distance))

  # we have to use site-standard-profile-request here because public profile url is not available for related connection.
  # The difference is site-standard-profile-request is an object with url property, and it requires authentication. The
  # user should already be authenticated at this point, so it's ok to use this URL
  CONNECTION_WITH_RELATED_FIELDS = BASE_CONNECTION_FIELDS + %w(relation-to-viewer:(distance,related-connections:(id,first-name,last-name,public-profile-url,picture-url,formatted-name)))

  check_authorization

  rescue_from CanCan::AccessDenied, LinkedIn::Errors::UnauthorizedError, LinkedIn::Errors::AccessDeniedError, with: :access_denied_handler

  rescue_from ActiveRecord::RecordNotFound, LinkedIn::Errors::NotFoundError, with: :not_found_handler

  rescue_from ArgumentError, with: :bad_request_handler

  rescue_from ActiveRecord::RecordInvalid, with: :bad_request_handler

  rescue_from LinkedIn::Errors::GeneralError, LinkedIn::Errors::InformLinkedInError,  LinkedIn::Errors::UnavailableError, with: :internal_server_error_handler

  after_filter :add_origin_headers

  helper_method :current_user, :job_board
  protected
  # Because RABL doesn't work with hash out of the box, we'll need to wrap LinkedIn::Mash with OpenStruct
  # This method accepts a connection and will wrap the connection itself and following attributes of the connection
  # with openstruct
  #  * relation_to_viewer
  # This method will not modify the original connection and will return a new OpenStruct object
  def wrap_with_openstruct(connection)
    wrapped_connection = OpenStruct.new(connection)
    wrapped_connection.relation_to_viewer = OpenStruct.new(wrapped_connection.relation_to_viewer)
    wrapped_connection
  end

  def linkedin_client
    # do not modify existing client state. Otherwise it can cause a subtle bug when there is an existing authenticated
    # user and we're processing callback for another user. The user being authenticating will have linkedin id of the
    # old user (as identified by the session)
    unless @client
      @client = LinkedIn::Client.new(job_board.linkedin_api_key,
                                       job_board.linkedin_api_secret,
                                       request_token_path: "/uas/oauth/requestToken?scope=r_basicprofile+r_emailaddress+r_network")
      if current_session and current_session.has_linkedin_access?
        @client.authorize_from_access(current_session.linkedin_atoken, current_session.linkedin_asecret)
      end
    end
    @client
  end

  def authenticate_client
    @current_client = Client.find_by_authentication_token(auth_token) if auth_token
  end

  def current_ability
    @current_ability ||= Ability.new(current_client)
  end

  # returns current client using provided authentication token
  def current_client
    auth_token = (params[:auth_token] || request.headers["auth_token"])
    @current_client ||= Client.find_by_authentication_token(auth_token) if auth_token
  end

  def current_user
    @current_user ||= User.find_by_session_uuid(cookies.signed[job_board.session_cookie.to_sym])
  end

  # Alternative to current_user. This method should be used instead of current_user.session if
  # we don't need user information. An example is in setting up linkedin_client object for linkedin api controllers.
  # We only need linkedin atoken & asecret to do that
  def current_session
      @current_session ||= Session.find_by_uuid(cookies.signed[job_board.session_cookie.to_sym])
  end

  def require_user_authentication
    raise CanCan::AccessDenied unless current_user
  end

  def job_board
    @job_board ||= JobBoard.active.find_by_uuid(params[:job_board_id])
    raise ActiveRecord::RecordNotFound unless @job_board
    @job_board
  end

  def debug?
    @debug ||= job_board.debug? and ENV['DEV_IPS'] and
        (ENV['DEV_IPS'].index(request.remote_ip) != nil or
            (request.headers['X-Forwarded-For'] and # if we're behind a load balancer
                # ELB can set X-Forwarded-For header to multiple comma-separated ip's with the first one being the real ip
                ENV['DEV_IPS'].index(request.headers['X-Forwarded-For'].split(', ')[0]) != nil))
  end

  # Create a mixpanel client object if one doesn't already exists and return. It will use current_job_board.mixpanel_token
  # To be safe, if current_job_board is nil, client will be created with nil token which doesn't do anything
  def mixpanel
    token = job_board.mixpanel_token if job_board
    @mixpanel ||= Mixpanel::Tracker.new token, { :env => request.env }
  end

  # error handler
  def not_found_handler(exception)
    respond_to do |format|
      format.any { render :template => 'api/v1/errors/not_found', :status => :ok }
    end
  end

  def access_denied_handler(exception)
    respond_to do |format|
      format.any { render :template => 'api/v1/errors/unauthorized', :status => :ok }
    end
  end

  def bad_request_handler(error)
    logger.error "Bad request error, error=#{error.message}"
    logger.error error.backtrace.join("\r\n")
    respond_to do |format|
      format.any { render :template => 'api/v1/errors/bad_request', :status => :ok }
    end
  end

  def internal_server_error_handler(exception)
    respond_to do |format|
      format.any { render :template => 'api/v1/errors/internal_server_error', :status => :ok }
    end
  end

  private
  def add_origin_headers
    response.headers['Access-Control-Allow-Origin'] = "*"
  end
end