class Api::Internal::BaseInternalController < ActionController::Base
  respond_to :json, :xml

  check_authorization

  rescue_from CanCan::AccessDenied, with: :access_denied_handler

  rescue_from ActiveRecord::RecordNotFound, with: :not_found_handler

  before_filter :authenticate_client!

  def current_ability
    @current_ability ||= Ability.new(current_client)
  end

  protected
  def job_board
    @job_board ||= JobBoard.find_by_uuid(params[:job_board_id])
    authorize! :show, @job_board
    @job_board
  end

  def not_found_handler(exception)
    respond_to do |format|
      format.all { render  :template => 'api/v1/errors/not_found', status: :not_found }
    end
  end

  def access_denied_handler(exception)
    respond_to do |format|
      format.any { render :template => 'api/v1/errors/unauthorized', :status => :unauthorized }
    end
  end
end