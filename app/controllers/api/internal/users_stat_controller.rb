require 'ostruct'

class Api::Internal::UsersStatController < Api::Internal::BaseInternalController
    def user_count_by_type
      @user_types = OpenStruct.new({employer: job_board.users.get_user_type('employer'),
                              job_seeker: job_board.users.get_user_type('job_seeker')})
    end

end