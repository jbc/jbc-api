class Api::Internal::InternalSessionsController < Api::Internal::BaseInternalController
  skip_authorization_check only: [:create, :destroy, :show]
  skip_before_filter :authenticate_client!, only: [:create]
  before_filter :check_params_exist, only: [:create]

  def create
    resource = Client.find_for_database_authentication(email: params[:email])
    return invalid_login unless resource

    if resource.valid_password?(params[:password])
      sign_in("client", resource)
      @current_client = resource
      return
    end
    invalid_login
  end

  def destroy
    sign_out("client")
    render :json=> {:success=>true}
  end

  def show
    @current_client = current_client
    @job_boards = @current_client.job_boards
  end

  protected
  def check_params_exist
    return unless params[:email].blank? or params[:password].blank?
    raise CanCan::AccessDenied
  end

  def invalid_login
    warden.custom_failure!
    raise CanCan::AccessDenied
  end

end