require 'ostruct'

class Api::Internal::EventsStatController < Api::Internal::BaseInternalController

  def events_history
    start_date = (params[:start_date] == nil or params[:start_date].empty?) ? 1.month.ago : params[:start_date].to_date
    end_date = (params[:end_date] == nil or params[:end_date].empty?) ? Date.today : params[:end_date].to_date

    return_value = {}
    TrackingEvent.get_events(start_date, end_date, job_board.id).each do |h|
      return_value[h.action] ||= []
      return_value[h.action] << {date: h.recorded_at.to_date, data: h.count ? h.count : 0}
    end
    @events = OpenStruct.new(return_value)
    @event_attributes = return_value.keys
  end
end