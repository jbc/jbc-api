class User < ActiveRecord::Base
  validates :user_type, presence: true, inclusion: {in: ['job_seeker', 'employer']}

  validates :linkedin_id, presence: true

  validates_uniqueness_of :linkedin_id, scope: :job_board_id

  belongs_to :job_board

  has_one :session

  before_save :populate_uuid

  # Delete an existing session associated with this user (if any) and create a new one
  def create_new_session(session)
    if self.session
      logger.info "Found existing session: #{self.session}. Deleting"
      self.session.destroy
    end
    self.session = Session.new(linkedin_atoken: session[:linkedin_atoken], linkedin_asecret: session[:linkedin_asecret],
                               linkedin_expires_at: session[:linkedin_expires_at], user_id: self.id)
    logger.info "New session #{self.session} created"
  end

  def self.find_by_session_uuid(session_uuid)
    User.joins(:session).where('sessions.uuid' => session_uuid)[0]
  end

  def self.get_user_type(type)
    where("user_type = ?", type).count()
  end

  def self.find_by_authentication_token(token)
    user = User.where(authentication_token: token).first
    if user
      # prevent token from being reused
      user.authentication_token = SecureRandom.urlsafe_base64
      user.save
    end
    return user
  end

  def self.linkedin_users_count_by_day(start_date, end_date, job_board_id)
    User.where({created_at: start_date.beginning_of_day..end_date.end_of_day, job_board_id: job_board_id}).group('created_at').order('created_at ASC').count
  end

  private
  def populate_uuid
    self.uuid ||= SecureRandom.urlsafe_base64
  end
end
