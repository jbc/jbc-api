class JobBoard < ActiveRecord::Base
  scope :active, -> {where(active: true)}

  belongs_to :client

  has_many :users, dependent: :destroy
  has_many :templates, dependent: :destroy
  has_many :tracking_events, dependent: :destroy

  belongs_to  :js_version

  validates :name, presence: true
  validates :url, presence: true

  validates :session_cookie, presence: true, uniqueness: true, format: /\A\w+\z/, length: 1..10

  validates_presence_of :client

  before_save :populate_uuid
  before_save :populate_js_version

  after_save :update_cache_async

  def is_debug?
    return self.debug
  end

  def self.update_cache(id)
    sweeper = JobBoardCacheSweeper.new()
    sweeper.sweep(JobBoard.find(id))
  end

  private
  def update_cache_async
    JobBoard.delay.update_cache(self.id)
  end

  def populate_uuid
    self.uuid = UUIDTools::UUID.random_create.to_s if self.uuid.blank?
  end

  # populate js_version with the latest version if availalbe
  def populate_js_version
    self.js_version = JsVersion.latest unless self.js_version
  end

  def self.get_id_by_uuid(uuid)
    job_boards = JobBoard.select(:id).where(uuid: uuid)
    raise ActiveRecord::RecordNotFound unless job_boards[0]
    return job_boards[0].id
  end
end
