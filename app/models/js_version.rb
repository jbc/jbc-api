class JsVersion < ActiveRecord::Base
  validates :version, presence: true, uniqueness: true

  def self.latest
    JsVersion.order('created_at DESC').first
  end

  def to_s
    self.version
  end
end
