class Ability
  include CanCan::Ability

  def initialize(client)
    client ||= Client.new
    if client.admin?
      can :manage, :all
    elsif not client.new_record? # if client was authenticated
      can [:read, :update], JobBoard, client_id: client.id
      can [:read, :update], User, :job_board => { client_id: client.id }
      can [:read], Session # Session resource needs further test to make sure client can only read his job boards' users' session
                           # because we don't know how to express it in this format
    else
      # when client auth token isn't provided, users can be read as long as
      # reader knows the uuid of the users
      can :read, User
    end
  end
end