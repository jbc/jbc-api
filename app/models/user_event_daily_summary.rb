class UserEventDailySummary < ActiveRecord::Base
  belongs_to :user
  belongs_to :job_board

  validates :event, presence: true
  validates :date, presence: true
  validates :count, presence: true, numericality: { greater_than: 0 }
end
