class Client < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  has_many :job_boards, dependent: :destroy

  before_save { |client| client.email = client.email.downcase }

  before_save :ensure_authentication_token

  # VALIDATION
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: true

  validates :name, presence: true

  # override Devise method
  def password_required?
    if new_record? or password.present? or password_confirmation.present?
      true
    else
      false
    end
  end

  def ensure_authentication_token
    self.authentication_token = SecureRandom.urlsafe_base64 if self.authentication_token.blank?
  end
end
