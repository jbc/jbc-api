class Session < ActiveRecord::Base
  validates :user_id, presence: true

  before_save :populate_uuid

  def delete_linkedin_access!
    self.linkedin_expires_at = nil
    self.linkedin_asecret = nil
    self.linkedin_atoken = nil
    self.save()
  end

  def has_linkedin_access
    not self.linkedin_asecret.blank? and not self.linkedin_atoken.blank? and not self.linkedin_expires_at.nil? and
        self.linkedin_expires_at >= Time.new
  end

  alias_method :has_linkedin_access?, :has_linkedin_access

  private
  def populate_uuid
    self.uuid ||= SecureRandom.urlsafe_base64
  end
end
