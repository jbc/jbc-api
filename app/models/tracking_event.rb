class TrackingEvent < ActiveRecord::Base
  belongs_to :job_board

  validates :category, presence: true, inclusion: ['LinkedIn']
  validates :action, presence: true
  validates :recorded_at, presence: true
  validates :job_board, presence: true
  validates :count, presence: true, numericality: {greater_than_or_equal_to: 0}

  def self.total_on(job_board, category, action, date)
    #where("job_board_id = ? AND category = ? AND action = ? and date(recorded_at) = ?", job_board.id, category, action, date.at_midnight).sum(:count)
    TrackingEvent.group("date(recorded_at)").select("recorded_at, sum(count) as count").first.count.to_i
  end

  def self.upsert(options = {})
    day = options[:recorded_at]
    event = options[:action]
    job_board_id = options[:job_board_id]
    count = options[:count]
    category = options[:category]
    inserted_event = TrackingEvent.where('recorded_at = ? and action = ? and job_board_id = ?', day, event, job_board_id)
    if inserted_event.length == 0
      TrackingEvent.create!(category: category,
                            action: event,
                            job_board_id: job_board_id,
                            recorded_at: day,
                            count: count)
    else
      inserted_event[0].category =  category
      inserted_event[0].action =  event
      inserted_event[0].job_board_id =  job_board_id
      inserted_event[0].recorded_at =  day
      inserted_event[0].count =  count
      inserted_event[0].save!
      Rails.logger.info "Update Action= #{event},  job_board_id= #{job_board_id},  recorded_at= #{day}, count= #{count}"
    end
  end

  def self.get_events(start_date, end_date, job_board_id)
    TrackingEvent.where(recorded_at: start_date.beginning_of_day..end_date.end_of_day, job_board_id: job_board_id).to_a
  end
end
