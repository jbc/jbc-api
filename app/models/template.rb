class Template < ActiveRecord::Base
  belongs_to :job_board, touch: true

  validates :name, presence: true, format: /\A\w+\z/
  validates :value, presence: true
  validates_presence_of :job_board

  validates_uniqueness_of :name, scope: :job_board_id

  after_save :update_cache_async

  private
  def update_cache_async
    JobBoard.delay.update_cache(self.job_board_id)
  end
end
