object @session

attributes :uuid, :linkedin_atoken, :linkedin_asecret, :has_linkedin_access

child @user do
  attributes :uuid, :linkedin_id, :authentication_token
end