object @user
attributes :uuid, :linkedin_id, :created_at, :user_type
child @linkedin_profile => :linkedin_profile do
  extends "api/v1/linkedin/profiles/show"
end