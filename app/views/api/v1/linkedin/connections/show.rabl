object @connection => :connection

attributes :id, :first_name, :last_name, :formatted_name, :headline, :public_profile_url, :connection_url

# move properties of relation_to_viewer to the top level so it's easier for client js to use
glue :relation_to_viewer do
  attributes :distance => :distance
  attributes :related_connections => :related_connections
end

node :errors do |o|
  o.errors
end

