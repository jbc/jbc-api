object false

node :connections do
  @connections.map do |s|
    {
        id: s['id'],
        first_name: s['first_name'],
        last_name: s['last_name'],
        picture_url: s['picture_url'],
        public_profile_url: s['public_profile_url'],
    }
  end
end

