object false

node(:firstDegreeCount) { |m| @firstDegreeCount }

node(:secondDegreeCount) { |m| @secondDegreeCount }

child @connections => :connections do
  extends "api/v1/linkedin/connections/show"
end
