object @job_board

attributes :uuid, :name, :url, :linkedin_api_key, :linkedin_api_secret, :linkedin_redirect_url, :sign_out_url, :created_at

node(:errors, :if => lambda { |o| not o.valid? } ) do |o|
  o.errors
end