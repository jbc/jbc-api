object @current_client

attributes :email

child :job_boards do
  attributes :uuid, :name, :logo_url
end

#node :job_boards do
#  @job_boards.map do |s|
#    {
#        uuid: s.uuid
#    }
#  end
#end