module ApplicationHelper

  # prepare template value so it can be correctly output to the browser
  # Template value is an html document. To turn that into a javascript string, we need:
  #  - remove all \r\n
  #  - escape all double quotes
  def prepare_template(template)
    template.delete("\r\n").gsub(/\\|"/) { |c| "\\#{c}" }
  end
end
