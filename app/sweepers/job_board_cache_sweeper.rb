class JobBoardCacheSweeper

  def sweep(job_board)
    begin
      sweep_rack_cache(job_board)
      sweep_cloud_flare(job_board)
      warm_cache(job_board)
    rescue StandardError => ex
      Delayed::Worker.logger.error ex
    end
  end

  private
  def get_job_board_url(job_board)
    JbcApi::Application.config.action_controller.asset_host +
        Rails.application.routes.url_helpers.api_v1_job_board_sdk_path(job_board.uuid, format: :js)
  end

  def sweep_cloud_flare(job_board)
    cf = CloudFlare.new(ENV['CLOUDFLARE_API_KEY'], ENV['CLOUDFLARE_EMAIL'])
    cf_url = get_job_board_url(job_board)
    begin
      Delayed::Worker.logger.info "Deleting #{cf_url} from cloudflare cache ..."
      output = cf.zone_file_purge("jobboardcloud.com", cf_url)
      if output
        Delayed::Worker.logger.info "  #{output['result'].upcase}"
        Delayed::Worker.logger.info ": #{output['msg']}\n" if output['msg']
      else
        Delayed::Worker.logger.error ' ERROR - Received nothing'
      end
    rescue StandardError => ex
      Delayed::Worker.logger.error "Error deleting cloudflare_url=#{cf_url}. Continue..."
    end
  end

  def sweep_rack_cache(job_board)
    response = rest_client.get get_job_board_url(job_board)
    if response.code == 200
      Delayed::Worker.logger.info response.headers
      content_digest = response.headers[:x_content_digest]
      if content_digest
        Delayed::Worker.logger.info "job_board=#{job_board.id}, digest=#{content_digest} found. Clearing"
        Rails.cache.delete(content_digest)
      else
        Delayed::Worker.logger.error "Didn't find X-Content-Digest header"
      end
    else
      Delayed::Worker.logger.error "url=#{get_job_board_url(job_board)} returns response_code=#{response.code}"
    end
  end

  def warm_cache(job_board)
    begin
      rest_client.get get_job_board_url(job_board)
    rescue Exception => e
      Delayed::Worker.logger.error "Exception caught warming cache, exception=#{e}"
    end
  end

  def rest_client
    return RestClient
  end
end