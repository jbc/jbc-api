ActiveAdmin.register JobBoard do
  permit_params :name, :url, :js_version, :linkedin_api_key, :linkedin_api_secret, :linkedin_redirect_url, :sign_out_url,
                :logo_url, :mixpanel_api_key, :mixpanel_api_secret, :mixpanel_token, :session_cookie, :allow_switch_user,
                :js_version_id, :active

  scope_to :current_user

  index do
    column :name
    column :client
    column :url
    column :js_version
    column :active
    default_actions
  end

  show :title => :name do
    attributes_table do
      row :client
      row :name
      row :uuid
      row :url
      row :active
      row :js_version
      row :linkedin_api_key
      row :linkedin_api_secret
      row :linkedin_redirect_url
      row :linkedin_unlink_url
      row :sign_out_url
      row :created_at
      row :logo_url
      row :mixpanel_api_key
      row :mixpanel_api_secret
      row :mixpanel_token
    end
  end

  form do |f|
    f.inputs "Basic Settings" do
      f.input :client if can?(:create, JobBoard)
      f.input :name
      f.input :url
      f.input :linkedin_api_key
      f.input :linkedin_api_secret
    end
    f.inputs "Advanced Settings" do
      f.input :js_version
      f.input :linkedin_redirect_url
      f.input :linkedin_unlink_url
      f.input :linkedin_unlink_url
      f.input :sign_out_url
      f.input :logo_url
      f.input :session_cookie
      f.input :debug
      f.input :allow_switch_user
      f.input :active
    end
    f.inputs "Mixpanel Settings" do
      f.input :mixpanel_api_key
      f.input :mixpanel_api_secret
      f.input :mixpanel_token
    end
    f.actions
  end

  controller do
    def current_user
      unless can? :manage, :all
        current_client
      end
    end
  end
end