ActiveAdmin.register JsVersion do
  permit_params :version
  index do
    column :id
    column :version
    column :created_at
    default_actions
  end

  show :title => :version do
    attributes_table do
      row :created_at
    end
  end

  form do |f|
    f.inputs "" do
      f.input :version
    end
    f.actions
  end
end