ActiveAdmin.register Client do
  permit_params :name, :email, :password
  # Cancan Integration
  menu :if => proc{ can?(:manage, Client) }

  index do
    column :name
    column :email
    column "Administrator?", :admin
    column "Sign up Date", :created_at
    default_actions
  end

  show :title => :name do
    attributes_table do
      row :name
      row :email
      row :authentication_token
      row :created_at
    end
  end

  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :email
    end
    f.inputs "Security" do
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
