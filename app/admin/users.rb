ActiveAdmin.register User do
  index do
    column :uuid
    column :job_board
    column :linkedin_id
    column :created_at
    default_actions
  end

  show :title => :uuid do
    attributes_table do
      row :job_board
      row :linkedin_id
      row :created_at
      row :user_type
    end
  end

  form do |f|
    f.inputs "" do
      f.input :job_board
      f.input :linkedin_id
    end
    f.actions
  end
end