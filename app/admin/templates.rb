ActiveAdmin.register Template do
  permit_params :name, :job_board, :value
  index do
    column :job_board
    column :name
    column "Template" do |template|
      truncate(template.value)
    end
    default_actions
  end

  show :title => :name do
    attributes_table do
      row :job_board
      row :name
      row :value
    end
  end

  form do |f|
    f.inputs "" do
      f.input :job_board
      f.input :name
      f.input :value
    end
    f.actions
  end
end