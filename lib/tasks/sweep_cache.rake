desc "Sweep all cache, should be used at deploy time"
namespace :cache do
  task :sweep => :environment do
    p "Script Sweeper"
    ScriptSweeper.sweep_all
  end
end