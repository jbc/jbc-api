require 'peach'
require 'csv'
namespace :load_test do
  desc "Generate sample data used for load testing"
  task :generate_data, [:job_board_count, :users_per_job_board, :jb_copy] => :environment do |t, args|
    client = Client.create! email: "test_email_#{rand(10000)}@foo.com",
                            name: "Load Test Client #{rand(10000)}",
                            password: "test123",
                            password_confirmation: "test123"

    CSV.open("jmeter/jbc/data/sample_data.csv", "w") do |out|
      (1..args[:job_board_count].to_i).to_a.peach(5) do|jb_counter|
        p "Generating job board: #{jb_counter}"
        begin
          jb = JobBoard.create!(name: "Test Job Board #{jb_counter}",
                                url: "Test Job Board URL #{jb_counter}",
                                linkedin_redirect_url: "http://example.com",
                                client_id: client.id,
                                linkedin_api_key: "FOO",
                                linkedin_api_secret:"BAR")
        rescue Exception => e
          p "Exception creating user #{jb_counter}: #{e}"
          next
        end

        JobBoard.find(args[:jb_copy]).templates.each do |template|
          p "Copying template #{template.name}"
          Template.create(name: template.name, job_board_id: jb.id, value: template.value)
        end

        (1..args[:users_per_job_board].to_i).to_a.peach(30) do |user_counter|
          p "Generating user #{user_counter} for job board: #{jb_counter}"
          begin
            linkedin_id = SecureRandom.urlsafe_base64
            if jb_counter % 2 == 0
              User.create!(linkedin_id: linkedin_id,
                           linkedin_atoken: "FOO",
                           linkedin_asecret: "BAR",
                           job_board_id: jb.id)
            end
            out << [jb.uuid, linkedin_id]
          rescue Exception => e
            p "Exception creating user #{user_counter} for #{jb.name}: #{e}"
            next
          end
        end
      end
    end
  end

  task :cleanup => :environment do |t, args|
    Client.where("name like ?", "%Load Test Client%").all.each {|c| c.destroy}
  end
end
