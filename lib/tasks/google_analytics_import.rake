class BucketNotFound < StandardError; end
class KeyFileNotFound < StandardError; end

def load_key_file
  Rails.logger.info 'Loading key file from S3'
  connection = Fog::Storage.new({
                                    :provider                 => 'AWS',
                                    :aws_access_key_id        => ENV['AWS_ACCESS_KEY_ID'],
                                    :aws_secret_access_key    => ENV['AWS_SECRET_ACCESS_KEY']
                                })
  folder = "#{ENV['GOOGLE_KEY_FOLDER']}"
  bucket ||= connection.directories.get(folder)
  raise BucketNotFound.new("AWS Bucket: #{folder} not found.") unless bucket
  key_file = bucket.files.get("google_api_keys/#{ENV['GOOGLE_API_KEY_FILE_NAME']}")
  raise KeyFileNotFound.new("google_api_keys/#{ENV['GOOGLE_API_KEY_FILE_NAME']} not found") unless key_file
  Rails.logger.info 'Key file is loaded'
  OpenSSL::PKCS12.new(key_file.body, 'notasecret').key
end

def import_google_analytics_events(client, analytics_service, profileID, start_date, task_status_object, job_boards_by_uuid)
  end_date = start_date + 1.day
  Rails.logger.info "Importing Google Analytics events from last_successful=#{start_date} to end_date=#{end_date}"
  task_status_object.transaction do
    events = client.execute(:api_method => analytics_service.data.ga.get, :parameters => {
        'ids' => "ga:" + profileID,
        'start-date' => start_date.strftime("%Y-%m-%d"),
        'end-date' => end_date.strftime("%Y-%m-%d"),
        'dimensions' => "ga:eventCategory,ga:eventAction,ga:eventLabel",
        'metrics' => "ga:totalEvents"
    })

    Rails.logger.info "#{events.data['totalResults']} total events"
    count = 0
    events.data.rows.each do |r|
      job_board = job_boards_by_uuid[r[2]]
      if not job_board
        Rails.logger.warn("job_board_uuid=#{r[2]} cannot be found. Skipping")
        next
      end
      begin
        TrackingEvent.create!(category: r[0],
                              action: r[1],
                              job_board: job_board,
                              recorded_at: start_date,
                              count: r[3])

        count += 1
      rescue StandardError => ex
        Rails.logger.error "Exception=\"#{ex}\" importing category=\"#{r[0]}\", action=\"#{r[1]}\" for job_board=\"#{job_board.name}\", last_successful=\"#{start_date}\", count=#{r[3]}"
      end
    end
    Rails.logger.info "#{count} events imported for last_successful=#{start_date} to end_date=#{end_date}"
    task_status_object.last_successful = end_date
    task_status_object.save
    DeadMansSnitch.report task_status_object.snitch_id, "Finished import for date=#{end_date}"
  end
end

desc 'Import Google Analytics data'
task :ga_import => :environment do
  # Inspired by https://gist.github.com/3166610
  require 'google/api_client'
  require 'date'

  service_account_email = ENV['GOOGLE_SERVICE_ACCOUNT_EMAIL'] # Email of service account
  profileID = ENV['GOOGLE_ANALYTICS_PROFILE_ID'] # Analytics profile ID.

  client = Google::APIClient.new()

# Load our credentials for the service account
  key = load_key_file

  asserter = Google::APIClient::JWTAsserter.new(
      service_account_email,
      'https://www.googleapis.com/auth/analytics.readonly',
      key)

# Request a token for our service account
  client.authorization = asserter.authorize()
  Rails.logger.info 'Authorized'

  timezone = 'Central Time (US & Canada)'
  task_status = ImportTaskStatus.find_by_task_name('ga_import')
  last_successful = task_status.last_successful.to_date
  end_date = DateTime.now.in_time_zone(timezone).to_date
  Rails.logger.info "Found last_succcessful=#{last_successful}"

  analytics = client.discovered_api('analytics','v3')
  job_boards_by_uuid = {}
  JobBoard.all.each do |job_board|
    job_boards_by_uuid[job_board.uuid] = job_board
  end
  (last_successful..end_date).each do |current_date|
    import_google_analytics_events(client, analytics, profileID, current_date, task_status, job_boards_by_uuid)
  end
end