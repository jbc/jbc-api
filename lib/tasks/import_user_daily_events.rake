EVENTS_WITH_USER_UUID = %w(view_relationship view_company_connections)

namespace :analytics do
  desc "Get active users tracked by mixpanel and populate our db"
  task :import_user_daily_events => :environment do
    timezone = 'Central Time (US & Canada)'
    task_status = ImportTaskStatus.find_by_task_name('import_user_daily_events')
    unless task_status
      Rails.logger.error('ImportTaskStatus=import_user_daily_events cannot be found')
      next
    end
    last_successful = task_status.last_successful.to_date
    Rails.logger.info "Found import_user_daily_events task status, last_successful=#{last_successful}"

    end_date = DateTime.now.in_time_zone(timezone).to_date
    day_interval = (end_date - last_successful).to_i
    task_status.transaction do
      total_count = 0
      JobBoard.all.each do |job_board|
        next if job_board.mixpanel_api_key.blank? or job_board.mixpanel_api_secret.blank?
        Rails.logger.info "Importing user event from MixPanel for job_board=\"#{job_board.name}\""
        client = Mixpanel::Client.new({api_key: job_board.mixpanel_api_key, api_secret: job_board.mixpanel_api_secret})

        EVENTS_WITH_USER_UUID.each do |event_name|
          Rails.logger.info "Requesting event=#{event_name} for interval=#{day_interval}"
          props = client.request('events/properties', {
              event: event_name,
              name: 'user_uuid',
              type: 'general',
              unit: 'day',
              interval: day_interval,
          })
          Rails.logger.info "Received #{props['data']['values'].length} users"
          props['data']['values'].each do |user_uuid, days|
            user = User.find_by_uuid(user_uuid)
            unless user
              Rails.logger.warn "Cannot find user by uuid=#{user_uuid}. Skipping"
              next
            end
            days.each do |day, count|
              next unless count > 0
              Rails.logger.info "User=#{user} has count=#{count} events, date=#{day}"
              UserEventDailySummary.create(user: user,
                                           job_board_id: user.job_board_id,
                                           date: Date.parse(day),
                                           event: event_name,
                                           count: count)
              total_count += 1
            end
          end
        end
      end
      Rails.logger.info "#{total_count} records imported from last_successful=#{last_successful} to end_date=#{end_date}"
      task_status.last_successful = end_date
      task_status.save
      DeadMansSnitch.report task_status.snitch_id, "Finished import for date=#{end_date}"
    end
  end
end