desc "Copy all templates from one job board to another"
task :copy_templates, [:from, :to] => :environment do |t, args|
  p "Copy from #{args[:from]} to #{args[:to]}"
  to_job_board = JobBoard.find(args[:to])
  JobBoard.find(args[:from]).templates.each do |template|
    p "Copying template #{template.name}"
    Template.create(name: template.name, job_board_id: to_job_board.id, value: template.value)
  end
end