desc "Populate fake events data"
task :populate_events => :environment do
  TrackingEvent.delete_all
  JobBoard.all.each do |jb|
    p "Generating events for #{jb.name}"
    (1.month.ago.to_date..Date.today).each do |d|
      count = 20000
      ['Loaded', 'Authenticated', 'Redirect', 'Sign-In', 'Sign-Out'].each do |action|
        count = rand(count || 20000)
        p "#{action}, #{d}, #{count}"
        TrackingEvent.create!(category: 'LinkedIn', action: action, recorded_at: d, count: count, job_board: jb)
      end
    end
  end
end