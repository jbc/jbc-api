require 'rake'
require 'mixpanel_client'
require 'spec_helper'

describe 'analytics:import_user_daily_events' do

  let :run_rake_task do
    Rake::Task['analytics:import_user_daily_events'].reenable
    Rake.application.invoke_task 'analytics:import_user_daily_events'
  end

  before :all do
    Rake.application.rake_require 'tasks/import_user_daily_events'
    Rake::Task.define_task(:environment)
  end

  describe 'rake analytics:import_user_daily_events' do
    before do
      mocked_client.stub(:request).and_return({"legend_size"=>1, "data"=>{"series"=>["2013-04-09"],
                                                                                          "values"=>{"View Company Connections"=>{"2013-04-09"=>0}}}})
      Mixpanel::Client.stub(:new).and_return(mocked_client)

      @job_board = FactoryGirl.create(:job_board, mixpanel_api_key: 'foo', mixpanel_api_secret: 'bar')
    end

    let(:mocked_client) { double("Mixpanel::Client") }

    it 'should find latest ImportTaskStatus' do
      ImportTaskStatus.should_receive(:find_by_task_name).with('import_user_daily_events')
    end

    it 'stops if correct ImportTaskStatus is not found' do
      ImportTaskStatus.should_receive(:find_by_task_name).with('import_user_daily_events').and_return(nil)
      Rails.logger.should_receive(:error).with('ImportTaskStatus=import_user_daily_events cannot be found')
    end

    after { run_rake_task }
  end
end
