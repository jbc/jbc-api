FactoryGirl.define do
  factory :client do
    sequence(:name)  { |n| "client #{n}" }
    sequence(:email) { |n| "client_#{n}@example.com"}
    password "foobar"
    password_confirmation "foobar"
    admin false

    factory :admin do
      admin true
    end
  end

  factory :job_board do
    name "foo"
    url "bar"
    linkedin_api_key "foobar"
    linkedin_api_secret "foobar"
    linkedin_redirect_url "redirect_url"
    sign_out_url "sign_out_url"
    sequence(:session_cookie) { |n| "session#{n}" }
    client
  end

  factory :js_version do
    sequence(:version) { |n| "version" }
  end

  factory :user do
    sequence(:linkedin_id) { |n| "linkedin_id_#{n}" }
    job_board
  end

  factory :session do
    sequence(:linkedin_atoken) {|n| "linkedin_atoken_#{n}" }
    user_id 1
  end

  factory :tracking_event do
    category "LinkedIn"
    action "Loaded"
    recorded_at DateTime.now
    count 2
    job_board
  end

  factory :template do
    sequence(:name)  { |n| "name#{n}" }
    sequence(:value) { |n| "value #{n}"}
    job_board
  end

  factory :oauth_token do
    sequence(:request_token) { |n| "request_token_#{n}" }
    sequence(:request_secret) { |n| "request_secret_#{n}" }
  end

  factory :linkedin_profile, class: Hash do
    sequence(:id) { |n| n }
    sequence(:first_name) {|n| "First name #{n}"}
    sequence(:last_name) {|n| "Last name #{n}"}
    sequence(:headline) {|n| "Headline #{n}"}
    sequence(:public_profile_url) {|n| "Public profile url #{n}"}

    factory :my_linkedin_profile, class: Hash do
      sequence(:email_address) { |n| "Email address #{n}" }

      relation_to_viewer do
        {distance: 0}
      end
    end

    factory :linkedin_connection_profile, class: Hash do
      relation_to_viewer do
        {distance: 1}
      end
    end

    initialize_with { attributes }

    to_create {} # empty to_create so FactoryGirl doesn't call save!
  end

end