
module ApiHelper
  include Rack::Test::Methods
  def app
    Rails.application
  end

  def unauthorized_error
    error = { errors: { message: 'Unauthorized'} }

    last_response.body.should == error.to_json
    last_response.status.should == 200
  end

  def bad_request_error
    error = { errors: { message: 'Bad request'} }

    last_response.body.should == error.to_json
    last_response.status.should == 200
  end

  def not_found_error
    error = { errors: { message: 'Not found'} }

    last_response.body.should == error.to_json
    last_response.status.should == 200
  end

  def internal_server_error
    error = { errors: { message: 'Internal server error'} }

    last_response.body.should == error.to_json
    last_response.status.should == 200
  end
end

RSpec.configure do |c|
  c.include ApiHelper, :type => :api
end
