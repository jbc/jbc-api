describe JobBoardCacheSweeper do

  let(:job_board) { FactoryGirl.create(:job_board) }

  let(:target) { JobBoardCacheSweeper.new }

  let(:rest_client) { double('rest_client') }

  before do
    target.stub(:rest_client).and_return(rest_client)
  end

  describe '#sweep' do

    before do
      # stub out method that calls out to cloudflare
      target.stub(:sweep_rack_cache).and_return('foo')
      target.stub(:sweep_cloud_flare).and_return('foo')
      target.stub(:warm_cache).and_return('foo')
    end
    it 'calls sweep_rack_cache' do
      target.should_receive(:sweep_rack_cache).with(job_board)
    end

    it 'calls sweep_cloud_flare' do
      target.should_receive(:sweep_rack_cache).with(job_board)
    end

    it 'calls warm_cache' do
      target.should_receive(:sweep_rack_cache).with(job_board)
    end

    it 'catch exception' do
      target.should_receive(:sweep_rack_cache).and_throw(StandardError)
    end

    after { target.sweep(job_board) }
  end

  # this test times out while running rspec. So uncomment for now
  describe '#sweeep_rack_cache' do
    before do
      @response = double('response', code: 200, headers: {x_content_digest: digest_header})
      rest_client.stub(:get).and_return(@response)
      Rails.cache.stub(:delete)
    end

    let(:digest_header) { 'foo' }

    context 'when receive 200 and has x_content_digest header' do
      it 'calls Rails.cache.delete' do
        Rails.cache.should_receive(:delete).at_least(1).with(digest_header)
        target.send(:sweep_rack_cache, job_board)
      end
    end

    context 'when not 200' do
      before do
        @response = double('response', code: 401, headers: {x_content_digest: digest_header})
        rest_client.stub(:get).and_return(@response)
      end

      it 'doesn\'t clear Rails.cache' do
        Rails.cache.should_not_receive(:delete).with(digest_header)
        target.send(:sweep_rack_cache, job_board)
      end
    end

    context 'when not 200' do
      before do
        @response = double('response', code: 401, headers: {x_content_digest: digest_header})
        rest_client.stub(:get).and_return(@response)
      end

      it 'doesn\'t clear Rails.cache' do
        Rails.cache.should_not_receive(:delete).with(digest_header)
        target.send(:sweep_rack_cache, job_board)
      end
    end

  end

  describe '#sweep_cloud_flare' do
    before do
      @cloudflare = double("cloudflare", zone_file_purge: true)
      ENV['CLOUDFLARE_API_KEY'] = "foo"
      ENV['CLOUDFLARE_EMAIL'] = "bar"
      target.stub(:get_job_board_url).and_return("url")
    end

    it 'creates new cloudflare instance' do
      CloudFlare.should_receive(:new).with("foo", "bar").and_return(@cloudflare)
      @cloudflare.should_receive(:zone_file_purge).with("jobboardcloud.com", "url")
      target.send(:sweep_cloud_flare, job_board)
    end
  end

  describe "#warm_cache" do
    it 'gets job_board url' do
      target.should_receive(:get_job_board_url).with(job_board).and_return("url")
      rest_client.should_receive(:get).with("url")
      target.send(:warm_cache, job_board)
    end
  end
end