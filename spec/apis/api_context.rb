# Shared context used by all linkedin controller specs. This will add
# client, admin, job board, user and cookies variables through let
# NOTE: this may very well be shared context for all API controller
shared_context 'API Context' do
  let(:client) { FactoryGirl.create(:client) }

  let(:admin) { FactoryGirl.create(:admin) }
  let(:js_version) {FactoryGirl.create(:js_version)}

  let(:job_board) { FactoryGirl.create(:job_board, client: client,
                                       linkedin_api_key: 'FOO',
                                       linkedin_api_secret: 'BAR',
                                       js_version: js_version) }

  let(:user) { FactoryGirl.create(:user, job_board: job_board) }

  let(:cookies) { ActionDispatch::Cookies::CookieJar.new(ActiveSupport::KeyGenerator.new(Rails.configuration.secret_token, iterations: 1000), 'localhost', false, signed_cookie_salt: "foo") }

  let(:session) { FactoryGirl.create(:session, user_id: user.id) }
end
