require 'spec_helper'
require File.dirname(__FILE__) + '/api_context'

def test_json(json, user)
  json['user']['uuid'].should == user.uuid
  json['user']['linkedin_id'].should == user.linkedin_id
end

describe Api::V1::UsersController, type: :api do

  include_context 'API Context'

  let(:url) { "/api/v1/job_boards/#{job_board.uuid}/users" }

  before do
    Api::V1::UsersController.any_instance.stub(:cookies).and_return(cookies)
  end

  describe "#index" do
    context "when authenticated" do
      let(:authentication_token) { client.authentication_token }

      context "accessing own job boards" do

        describe "lists all users" do

          before { get "#{url}.json", auth_token: authentication_token }

          let(:json) { JSON.parse(last_response.body) }

          subject { last_response }

          its(:status) { should == 200 }

          it "returns correct number of users" do
            json.length.should == job_board.users.length
          end
        end
      end

      context "accessing other clients' job boards" do

        let(:other_jb) { FactoryGirl.create(:job_board) }

        let(:url) { "/api/v1/job_boards/#{other_jb.uuid}/users" }

        before { get "#{url}.json", auth_token: authentication_token }

        it "cannot list all users" do
          unauthorized_error
        end

        #todo: continue rspec refactor
      end
    end

    context "non-client" do
      it "can search users by linkedin_id" do
        get "#{url}.json", linkedin_id: user.linkedin_id
        json = JSON.parse(last_response.body)
        json.length.should == 1
        test_json(json[0], user)
        last_response.status.should == 200
      end

      it "can search and returns empty result" do
        get "#{url}.json", linkedin_id: "foo"
        json = JSON.parse(last_response.body)
        json.length.should == 0
      end

      it "cannot search users without linkedin_id" do
        get "#{url}.json"
        unauthorized_error
      end
    end
  end

  describe "#show" do
    let(:authentication_token) { client.authentication_token }
    context "using user's uuid" do
      context "with correct job board" do
        context "with authentication token" do
          before do
            get "#{url}/#{user.uuid}.json", auth_token: client.authentication_token
            @json = JSON.parse(last_response.body)
          end

          it("returns user") { test_json(@json, user) }

          it 'should have cache-control header' do
            last_response.headers['Cache-Control'].should == "max-age=#{1.year.to_i}, public"
          end
        end

        context "without authentication token" do
          before do
            get "#{url}/#{user.uuid}.json"
            @json = JSON.parse(last_response.body)
          end

          it("returns user") { test_json(@json, user) }
        end
      end
    end

    context "using ~" do
      let(:url) { "/api/v1/job_boards/#{job_board.uuid}/users/~.json" }

      let(:my_profile) { FactoryGirl.build(:my_linkedin_profile) }

      context "with a logged in user" do
        before do
          cookies.signed[job_board.session_cookie.to_sym] = session.uuid
          # stub linkedin api calls
          LinkedIn::Client.any_instance.stub(:profile).and_return(my_profile)
          get url
          @json = JSON.parse(last_response.body)
        end

        it('return users') { test_json(@json, user) }

        it('doesn\'t contain caching') do
          last_response.headers['Cache-Control'].should == 'max-age=0, private, must-revalidate'
          last_response.headers['Last-Modified'].should be_nil
        end

        it('calls LinkedIn profile api') do
          LinkedIn::Client.any_instance.should_receive(:profile)
          get url
        end

        it('does\'t raise exception when LinkedIn profile raise Exception') do
          LinkedIn::Client.any_instance.stub(:profile).and_raise(LinkedIn::Errors::NotFoundError)
          get url
          last_response.status.should == 200
          JSON.parse(last_response.body)['errors'].should_not be_nil
        end
      end

      context "without a logged in user" do
        before do
          get url
        end

        it("returns unauthorized") { unauthorized_error }
      end
    end
  end

  shared_examples "update user type" do
    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/users/#{user.uuid}.json" }
    context 'with authentication' do
      context 'with valid user_type' do
        before do
          if m == "POST"
            post url, auth_token: client.authentication_token, user: {user_type: "employer"}
          else
            put url, auth_token: client.authentication_token, user: {user_type: "employer"}
          end
        end

        it('returns 200') { last_response.status.should == 200 }

        it('saves user_type') do
          get url
          JSON.parse(last_response.body)["user"]["user_type"].should == "employer"
        end
      end

      context "with invalid user_type" do
        before do
          if m == "POST"
            post url, auth_token: client.authentication_token, user: {user_type: "foo"}
          else
            put url, auth_token: client.authentication_token, user: {user_type: "foo"}
          end
        end

        it 'bad request' do
          bad_request_error
        end

        it('should not save user_type') do
          get url
          JSON.parse(last_response.body)["user"]["user_type"].should_not == "foo"
        end
      end
    end

    context "without authentication" do
      before do
        put url, user: {user_type: "employer"}
      end

      it 'unauthorized' do
        unauthorized_error
      end
    end
  end

  describe '#update' do
    describe "with POST" do
      let(:m) { "POST" }
      it_behaves_like "update user type"
    end

    describe "with PUT" do
      let(:m) { "put" }
      it_behaves_like "update user type"
    end
  end
end
