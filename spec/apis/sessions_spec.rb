require 'spec_helper'
require File.dirname(__FILE__) + '/api_context'

def validate_session(json, session)
  session_json = json['session']
  session_json['uuid'].should == session.uuid
  session_json['linkedin_atoken'].should == session.linkedin_atoken
  session_json['has_linkedin_access'].should == session.has_linkedin_access
  user = User.find(session.user_id)
  session_json['user']['uuid'].should == user.uuid
  session_json['user']['linkedin_id'].should == user.linkedin_id
  session_json['user']['authentication_token'].should == user.authentication_token
end

describe Api::V1::SessionsController, type: :api do

  include_context 'API Context'


  describe '#index' do
    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/sessions"}

    context 'without auth_token' do
      before { get url }

      subject { last_response }

      it { unauthorized_error }
    end

    context 'with correct auth_token' do
      before do
        @session = session
        get url, auth_token: client.authentication_token, user_uuid: user.uuid
      end

      subject { last_response }

      its(:status) { should == 200 }

      its(:length) {should > 0 }

      it 'returns session' do
        json_session = JSON.parse(last_response.body)[0]
        validate_session(json_session, session)
      end
    end

    context 'when user doesn\'t have session' do
      before do
        session.delete
        get url, auth_token: client.authentication_token, user_uuid: user.uuid
      end

      subject { last_response }

      it { not_found_error }
    end

    context 'with incorrect user_uuid' do

      before do
        get url, auth_token: client.authentication_token, user_uuid: 'foo'
      end

      subject { last_response }

      it { not_found_error }
    end

  end

  describe '#show' do
    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/sessions/#{session.uuid}.json"}

    context 'without auth_token' do
      before { get url }

      subject { last_response }

      it { unauthorized_error }
    end

    context 'with incorrect auth_token' do # auth token of a different client
      let(:other_client) { FactoryGirl.create(:client) }

      before { get url, auth_token: other_client.authentication_token }

      subject { last_response }

      it { unauthorized_error }
    end

    context 'with correct auth_token' do
      before do
        get url, auth_token: client.authentication_token
      end

      subject { last_response }

      its(:status) { should == 200 }

      it 'returns session' do
        validate_session(JSON.parse(last_response.body), session)
      end
    end

    context 'with admin auth_token' do
      before do
        get url, auth_token: admin.authentication_token
      end

      subject { last_response }

      its(:status) { should == 200 }

      it 'returns session' do
        validate_session(JSON.parse(last_response.body), session)
      end
    end
  end

  describe '#session_cache_path_url' do
    let(:controller) { Api::V1::SessionsController.new }

    before do
      controller.stub(:params).and_return({auth_token: client.authentication_token,
                                           job_board_id: job_board.uuid,
                                           id: session.uuid})
      controller.stub(:current_client).and_return(client)
      controller.stub(:current_session).and_return(session)
      controller.instance_variable_set(:@session, session)
      @url = controller.send(:session_cache_path_url)
    end

    it 'should return correct caching path' do
      @url.should == "#{client.cache_key}/job_boards/#{job_board.uuid}/#{session.cache_key}"
    end
  end
end
