require 'spec_helper'

def test_linkedin_profile_json(json, user, fields)
  fields.each do |field|
    json['profile'][field].should == user[field.to_sym]
  end
end

# require shared context that define variables shared by all LinkedIn api specs
require File.dirname(__FILE__) + '/linkedin_api_context'

describe Api::V1::Linkedin::ProfilesController, type: :api do

  include_context 'LinkedIn API Context'

  let(:my_profile) { FactoryGirl.build(:my_linkedin_profile) }

  let(:other_profile) { FactoryGirl.build(:linkedin_connection_profile) }

  before do
    Api::V1::Linkedin::ProfilesController.any_instance.stub(:cookies).and_return(cookies)
  end

  describe '#show' do
    let(:base_url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/profiles" }

    context 'when not authenticated' do
      subject { last_response }

      context 'when access my profile' do
        let(:url) { "#{base_url}/me.json" }

        before { get url }

        it { unauthorized_error }
      end

      context 'when access other profile' do
        let(:url)  { "#{base_url}/foo.json" }

        before { get url }

        it { unauthorized_error }
      end
    end

    context 'when authenticated' do
      let(:url)  { "#{base_url}/foo.json" }

      before do
        cookies.signed[job_board.session_cookie.to_sym] = api_session.uuid
      end

      context 'when access my profile do' do
        let(:url) { "#{base_url}/me.json" }

        before do
          # stub linkedin api calls
          LinkedIn::Client.any_instance.stub(:profile).and_return(my_profile)
          get url
        end

        subject { last_response }

        it 'should call LinkedIn profile api' do
          LinkedIn::Client.any_instance.should_receive(:profile).
              with(hash_including(fields: ["#{Api::V1::Linkedin::BaseController::CURRENT_PROFILE_FIELDS.join(',')}"]))
          get url
        end

        it 'returns profile json' do
          test_linkedin_profile_json(JSON.parse(last_response.body), my_profile, Api::V1::Linkedin::BaseController::CURRENT_PROFILE_FIELDS)
        end

        it 'sets caching headers' do
          last_response.headers['Cache-Control'].should == "max-age=#{1.days}, private"
        end
      end

      context 'when access other profile' do
        before do
          # stub linkedin api calls
          LinkedIn::Client.any_instance.stub(:profile).and_return(other_profile)

          get url
        end

        subject { last_response }

        it 'should call LinkedIn profile api' do
          LinkedIn::Client.any_instance.should_receive(:profile).with(hash_including(id: 'foo')).
              with(hash_including(fields: ["#{Api::V1::Linkedin::BaseController::CONNECTION_WITH_RELATED_FIELDS.join(',')}"]))
          get url
        end

        it 'returns profile json' do
          test_linkedin_profile_json(JSON.parse(last_response.body), other_profile, Api::V1::Linkedin::BaseController::CONNECTION_WITH_RELATED_FIELDS)
        end

        it 'sets caching headers' do
          last_response.headers['Cache-Control'].should == "max-age=#{1.days}, private"
        end
      end

      context 'when there is an unknown error' do
        before do
          LinkedIn::Client.any_instance.stub(:profile).and_raise(LinkedIn::Errors::GeneralError.new("data"))
          get url
        end

        subject { last_response }

        it { internal_server_error }

        it('Includes a generic error message') do
          JSON.parse(last_response.body)['errors']['message'].should_not be_empty
        end
      end

      context 'when searching for invalid id' do
        before do
          LinkedIn::Client.any_instance.stub(:profile).and_raise(LinkedIn::Errors::NotFoundError)
          get url
        end

        subject { last_response }

        it { not_found_error }
      end
    end
  end

  describe '#profile_cache_path_url' do
    let(:controller) { Api::V1::Linkedin::ProfilesController.new }

    let(:user) { FactoryGirl.create(:user) }

    before do
      controller.stub(:current_session).and_return(api_session)
    end

    context 'when access my profile' do
      before { controller.stub(:params).and_return({id: 'me', job_board_id: 'foobar'}) }

      subject { controller.send(:profile_cache_path_url) }

      it { should == "job_boards/foobar/linkedin/profiles/#{api_session.user_id}_private" }
    end

    context 'when access other profile' do
      before { controller.stub(:params).and_return({id: 'foo', job_board_id: 'foobar'}) }

      subject { controller.send(:profile_cache_path_url) }

      it { should == "job_boards/foobar/linkedin/profiles/foo" }
    end
  end
end