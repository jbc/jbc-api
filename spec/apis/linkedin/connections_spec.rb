require 'spec_helper'

# require shared context that define variables shared by all LinkedIn api specs
require File.dirname(__FILE__) + '/linkedin_api_context'

describe Api::V1::Linkedin::ConnectionsController, type: :api do

  include_context 'LinkedIn API Context'

  before do
      Api::V1::Linkedin::ConnectionsController.any_instance.stub(:cookies).and_return(cookies)
      Api::V1::Linkedin::ConnectionsController.any_instance.stub(:linkedin_client).and_return(linkedin_client)
  end

  let(:linkedin_client) { double('linkedin_client')}

  # create 5 first degree connections
  let(:first_connections) do
    (1..5).to_a.map do |n|
      # use build instead of create because linkedin_connection is not ActiveRecord
      FactoryGirl.build(:linkedin_connection_profile)
    end
  end

  # 5 second degree connections
  let(:second_connections) do
    (1..10).to_a.map do |n|
      FactoryGirl.build(:linkedin_connection_profile, relation_to_viewer: {distance: 2})
    end
  end

  # empty shell for LinkedIn people search result. Used as return value for mocked LinkedIn search api
  let(:search_result) do
    LinkedIn::Mash.new({
                           'facets' => {
                               'total' => 0,
                               'all' => [
                                   {
                                       'buckets' => {
                                           'total' => 0,
                                           'all' => [
                                               {
                                                   'code' => 'F',
                                                   'count' => 0,
                                                   'name' => '1st Connections'
                                               },
                                               {
                                                   'code' => 'S',
                                                   'count' => 0,
                                                   'name' => '2nd Connections'
                                               },
                                               {
                                                   'code' => 'A',
                                                   'count' => 0,
                                                   'name' => 'Group Members'
                                               },
                                               {
                                                   'code' => 'O',
                                                   'count' => 0,
                                                   'name' => '3rd + Everyone Else'
                                               },
                                           ]},
                                       'code' => 'network'}]
                           },
                           'total_results' => 10, # assign non-zero total results count otherwise a not found will be returned
                           'people' => {
                               '_count' => 0,
                               '_start' => 0,
                               'total' => 0,
                               'all' => []
                           }
                       })
  end

  describe '#index' do

    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/connections.json" }

    context 'when not authenticated' do

      before do
        get url
      end

      it 'returns unauthorized' do
        unauthorized_error
      end
    end

    context 'when authenticated' do

      before do
        cookies.signed[job_board.session_cookie.to_sym] = api_session.uuid
      end

      context 'with company parameter' do

        before do
          # set count for 1st degree network facet
          search_result['facets']['all'][0]['buckets']['all'][0]['count'] = 5
          # set count for 2nd degree network facet
          search_result['facets']['all'][0]['buckets']['all'][1]['count'] = 10
          search_result['people']['all'] = first_connections

          # stub linkedin api calls
          linkedin_client.stub(:search).and_return(search_result)
        end

        it 'calls LinkedIn people search' do
          linkedin_client.should_receive(:search)
          get url, company: 'company'
        end

        context 'when there aren\'t any connections' do
          before do
            search_result['total_results'] = 0
            get url, company: 'company'
            @json = JSON.parse(last_response.body)
          end

          subject { last_response }

          its(:status) { should == 200 }

          it 'should returns empty list of connections' do
            @json['connections'].size.should == 0
          end

          it 'return 0 as firstDegreeCount' do
            @json['firstDegreeCount'].should == 0
          end

          it 'return 0 as secondDegreeCount' do
            @json['secondDegreeCount'].should == 0
          end
        end

        context 'when there are only first connections' do
          before do
            get url, company: 'company'
            @json = JSON.parse(last_response.body)
          end

          subject { last_response }

          its(:status) { should == 200 }

          it 'should returns only 5 first connections' do
            @json['connections'].size.should == 5
          end

          it 'return 5 as firstDegreeCount' do
            @json['firstDegreeCount'].should == 5
          end

          it 'return 10 as secondDegreeCount' do
            @json['secondDegreeCount'].should == 10
          end
        end

        context 'when there are both first and second connections' do

          before do
            search_result['people']['all'] = first_connections.concat(second_connections)
            get url, company: 'company'
            @json = JSON.parse(last_response.body)
          end

          subject { last_response }

          its(:status) { should == 200 }

          it 'should returns only 5 first connections' do
            @json['connections'].size.should == 5
          end

          it 'return 5 as firstDegreeCount' do
            @json['firstDegreeCount'].should == 5
          end

          it 'return 10 as secondDegreeCount' do
            @json['secondDegreeCount'].should == 10
          end
        end

        context 'when there aren\'t first connections' do
          before do
            search_result['people']['all'] = second_connections
            get url, company: 'company'
            @json = JSON.parse(last_response.body)
          end

          subject { last_response }

          its(:status) { should == 200 }

          it 'should returns second connections' do
            @json['connections'].size.should == 10
          end

          it 'return 5 as firstDegreeCount' do
            @json['firstDegreeCount'].should == 5
          end

          it 'return 10 as secondDegreeCount' do
            @json['secondDegreeCount'].should == 10
          end
        end

        context 'when there are private connections' do
          before do
            search_result['people']['all'] = first_connections + [FactoryGirl.build(:linkedin_connection_profile, id: 'private')]
            get url, company: 'company'
          end

          subject { last_response }

          its(:status) { should == 200 }

          it 'should not returns private connections' do
            JSON.parse(last_response.body)['connections'].size.should == 5
          end
        end
      end

      context 'without company parameter' do

        before do
          get url
        end

        subject { last_response }

        it 'returns bad request error' do
          bad_request_error
        end

      end
    end
  end

  describe '#show' do

    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/connections/foo.json" }

    context 'when not authenticated' do

      before do
        get url
      end

      it 'returns unauthorized error' do
        unauthorized_error
      end
    end

    context 'when authenticated' do

      let(:connection) { FactoryGirl.build(:linkedin_connection_profile) }

      before do
        cookies.signed[job_board.session_cookie.to_sym] = api_session.uuid
        linkedin_client.stub(:profile).and_return(connection)
      end

      it 'should call LinkedIn profile api' do
        linkedin_client.should_receive(:profile).with(hash_including(id: 'foo'))
        get url
      end

      it 'should use CONNECTION_WITH_RELATED_FIELDS as fields param' do
        linkedin_client.should_receive(:profile).
            with(hash_including(fields: ["#{Api::V1::Linkedin::BaseController::CONNECTION_WITH_RELATED_FIELDS.join(',')}"]))
        get url
      end

      it 'should return profile' do
        get url
        JSON.parse(last_response.body)['id'].should == connection['id']
      end


      context 'when profile is private' do

        before do
          linkedin_client.stub(:profile) do
            LinkedIn::Mash.new(id: 'private')
          end
          get url
        end

        it 'should return profile' do
          JSON.parse(last_response.body).size.should == 1
        end

        it 'should have cache-control header' do
          last_response.headers['Cache-Control'].should == "max-age=#{24.hours}, private"
        end
      end

      context 'when searching for invalid id' do
        before do
          linkedin_client.stub(:profile).and_raise(LinkedIn::Errors::NotFoundError)
          get url
        end

        it 'returns not found error' do
          not_found_error
        end
      end

      context 'when there is an unknown error' do
        before do
          linkedin_client.stub(:profile).and_raise(LinkedIn::Errors::GeneralError.new("data"))
          get url
        end

        subject { last_response }

        it 'returns internal server error' do
          internal_server_error
        end
      end
    end
  end

  describe '#connections_by_company_cache_path_url' do
    let(:controller) { Api::V1::Linkedin::ConnectionsController.new }

    before do
      controller.stub(:params).and_return({company: 'foo', job_board_id: 'foobar'})
      controller.stub(:current_session).and_return(api_session)
      @url = controller.send(:connections_by_company_cache_path_url)
    end

    it 'should return correct caching path' do
      @url.should == "job_boards/foobar/linkedin/connections?company=foo-user_id=#{user.id}"
    end
  end

  describe '#connection_cache_path_url' do
    let(:controller) { Api::V1::Linkedin::ConnectionsController.new }

    before do
      controller.stub(:params).and_return({id: 'foo', job_board_id: 'foobar'})
      controller.stub(:current_session).and_return(api_session)
      @url = controller.send(:connection_cache_path_url)
    end

    it 'should return correct caching path' do
      @url.should == "job_boards/foobar/linkedin/connections/foo-user_id=#{user.id}"
    end
  end
end