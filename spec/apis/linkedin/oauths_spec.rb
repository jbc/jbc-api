require 'spec_helper'

require File.dirname(__FILE__) + '/linkedin_api_context'

describe Api::V1::Linkedin::OauthController, type: :api do

  include_context 'LinkedIn API Context'

  before do
    Api::V1::Linkedin::OauthController.any_instance.stub(:cookies).and_return(cookies)
    Api::V1::Linkedin::OauthController.any_instance.stub(:linkedin_client).and_return(linkedin_client)
  end

  let(:linkedin_client) { double('linkedin_client')}

  let(:linkedin_profile) do
    linkedin_profile = ActiveSupport::OrderedOptions.new
    linkedin_profile.id = 'LINKEDIN_ID'
    linkedin_profile
  end

  describe '#linkedin_client' do
    before do
      Api::V1::Linkedin::OauthController.any_instance.stub(:linkedin_client).and_call_original
      Api::V1::Linkedin::OauthController.any_instance.stub(:current_session).and_return(session)
      Api::V1::Linkedin::OauthController.any_instance.stub(:job_board).and_return(job_board)
    end

    let(:session) { double('session') }
    let(:job_board) { double('job_board') }

    it 'when linkedin_client is set, it won\'t reauthorize' do
      c = Api::V1::Linkedin::OauthController.new()
      session.should_receive(:has_linkedin_access?).and_return(true)
      session.should_receive(:linkedin_atoken).and_return("atoken")
      session.should_receive(:linkedin_asecret).and_return("asecret")
      job_board.should_receive(:linkedin_api_key).and_return("foo")
      job_board.should_receive(:linkedin_api_secret).and_return("bar")
      LinkedIn::Client.any_instance.should_receive(:authorize_from_access)
      c.linkedin_client
      LinkedIn::Client.any_instance.should_not_receive(:authorize_from_access)
      c.linkedin_client
    end
  end

  describe '#new' do

    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/oauth/new.json" }

    # mocked request token
    let(:request_token) do
      request_token = OAuth::RequestToken.new nil, UUIDTools::UUID.random_create.to_s, 'BAR'

      class <<request_token
        # override authorize_url of request token. Is this necessary? Why can't we stub it?
        def authorize_url
          "/authorize/#{token}"
        end
      end

      request_token
    end

    before do
      linkedin_client.stub(:request_token).and_return(request_token)
    end

    context 'when current_session is nil' do
      before { Api::V1::Linkedin::OauthController.any_instance.stub(:current_session).and_return(nil) }

      it 'Get request token from LinkedIn' do
        linkedin_client.should_receive(:request_token)
        get url
      end

      it 'returns 200' do
        get url
        last_response.status.should == 200
      end

      it 'returns non_empty authorize_url' do
        get url
        json = JSON.parse(last_response.body)
        json['url'].should ==  "/authorize/#{request_token.token}"
      end

      it 'stores request token in Rails.cache' do
        Rails.cache.read("linkedin/oauth/request_token/#{request_token.token}").should be_nil
        get url
        cache_entry = Rails.cache.read("linkedin/oauth/request_token/#{request_token.token}")
        cache_entry.should_not be_nil
        cache_entry[:request_secret].should == 'BAR'
      end
    end

    context 'when has redirect_url parameter' do
      before { get url, redirect_url: 'foo' }

      it 'adds redirect_url to Rails cache' do
        cache_entry = Rails.cache.read("linkedin/oauth/request_token/#{request_token.token}")
        cache_entry[:redirect_url].should == 'foo'
      end
    end

    context 'html format' do
      let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/oauth/new.html" }

      before do
        get url
      end

      subject { last_response }

      its(:status) { should == 302 }
    end
  end

  describe '#callback' do
    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/oauth/callback.json"}

    let(:request_token) { "foo234" }

    let(:request_secret) { "secret123" }

    before do
      linkedin_client.stub(:authorize_from_request).and_return ['ATOKEN', 'ASECRET']

      linkedin_client.stub(:profile).and_return(linkedin_profile)

      Rails.cache.write("linkedin/oauth/request_token/#{request_token}", {request_secret: request_secret})

      Timecop.freeze(DateTime.now)
    end


    context 'With oauth problem' do
      before { get url, oauth_problem: 'user_refused'}

      subject { last_response }

      its(:status) { should == 302 }

      its(:location) { should  == "http://example.org#{job_board.linkedin_redirect_url}&error=user_refused"}
    end

    context 'Validations' do
      subject { last_response.status }

      context 'Missing parameters' do
        before { get url }

        it { bad_request_error }
      end

      describe 'missing oauth verifier' do
        before { get url, oauth_token: 'Foo' }

        it { bad_request_error }
      end

      context 'with an invalid oauth_verifier' do
        before do
          linkedin_client.stub(:authorize_from_request).
              with(request_token, request_secret, 'Invalid verifier').
              and_raise(LinkedIn::Errors::AccessDeniedError.new('Data'))

          get url, {oauth_token: request_token, oauth_verifier: 'Invalid verifier'}
        end

        it { unauthorized_error }
      end

      context 'oauth_token' do
        describe 'returns 401 if missing oauth_token' do
          before { get url, oauth_verifier: 'Foo' }

          it { bad_request_error }
        end

        describe 'returns bad request if oauth_token is invalid' do
          before { get url, {oauth_token: 'FOO', oauth_verifier: '234234'} }

          it { bad_request_error }
        end
      end
    end

    it 'Deletes oauth token record' do
      get url, oauth_token: request_token, oauth_verifier: 'foo'
      Rails.cache.read("linkedin/oauth/request_token/#{request_token}").should be_nil
    end

    it 'Authorizes with LinkedIn after looking up token' do
      linkedin_client.should_receive(:authorize_from_request).
          with(request_token, request_secret, 'foo').
          and_return(['ATOKEN', 'ASECRET'])
    end

    it 'Fetches LinkedIn profile after authorizing' do
      linkedin_client.should_receive(:profile).
          with(:fields => %w(id)).and_return(linkedin_profile)
    end

    it 'Finds user using linkedin profile id' do
      User.should_receive(:find_by_linkedin_id).
          with(linkedin_profile.id)
    end

    context 'when user doesn\'t exist' do
      before do
        User.should_receive(:find_by_linkedin_id).
            with(linkedin_profile.id).and_return(nil)
      end

      it 'creates a new user' do
        User.should_receive(:create!).with(linkedin_id: linkedin_profile.id, job_board_id: job_board.id). and_return(user)
      end
    end

    context 'when user exists' do
      before do
        User.should_receive(:find_by_linkedin_id).with(linkedin_profile.id).and_return(user)
      end

      it "doesn't create new user" do
        User.should_not_receive(:create!)
      end
    end

    describe 'redirection' do
      context 'when job board provides redirect url' do
        it 'redirect to provided url' do
          Rails.cache.write("linkedin/oauth/request_token/#{request_token}", {request_secret: request_secret, redirect_url: 'foobar'})
          get url, {oauth_token: request_token, oauth_verifier: 'foo'}
          last_response.headers['location'].should start_with 'http://example.orgfoobar'
        end
      end

      context 'when job boards doesn\'t provide redirect url' do
        it 'redirect to job board\'s default url' do
          get url, {oauth_token: request_token, oauth_verifier: 'foo'}
          last_response.headers['location'].should start_with "http://example.org#{job_board.linkedin_redirect_url}"
        end
      end
    end

    after do
      get url, {oauth_token: request_token, oauth_verifier: 'foo'}
    end
  end

  describe '#switch_user' do
    before do
      @timestamp = Time.now().to_i
      @signature = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new("sha256"),
                                                                job_board.client.authentication_token,
                                                                "#{user.uuid}#{@timestamp.to_s}")).strip()
    end

    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/linkedin/oauth/switch_user"}

    it 'returns 200 with error if user_uuid is missing' do
      post url, {signature: 'foo', timestamp: '123'}
      unauthorized_error
    end

    it 'returns 200 with error if signature is missing' do
      post url, {user_uuid: 'foo', timestamp: '123'}
      unauthorized_error
    end

    it 'returns 200 with error if signature is missing' do
      post url, {user_uuid: 'foo', timestamp: '123'}
      unauthorized_error
    end

    it 'returns 200 with error if signature doesn\'t match' do
      post url, {user_uuid: user.uuid, timestamp: Time.now().to_i, signature: 'foo'}
      unauthorized_error
    end

    it 'returns 200 with error if params are tempered with' do
      post url, {user_uuid: "foo", timestamp: @timestamp, signature: @signature}
      unauthorized_error
    end

    it 'returns 200 with error if timestamp is too old' do
      timestamp = Time.now().to_i - 500
      signature = Base64.encode64(OpenSSL::HMAC.digest(OpenSSL::Digest::Digest.new("sha256"),
                                                               job_board.client.authentication_token,
                                                               "#{user.uuid}#{timestamp.to_s}")).strip()
      post url, {user_uuid: user.uuid, timestamp: timestamp, signature: signature}
      unauthorized_error
    end

    context 'when params are valid' do
      before do
        User.stub(:find_by_uuid).and_return(user)
        user.session = api_session
      end

      it 'looks up user using uuid' do
        User.should_receive(:find_by_uuid).with(user.uuid)
        post url, {user_uuid: user.uuid, timestamp: @timestamp, signature: @signature}
      end

      it 'does not set cookie if user cannot be found' do
        User.stub(:find_by_uuid).and_return(nil)
        post url, {user_uuid: user.uuid, timestamp: @timestamp, signature: @signature}
        cookies.signed[job_board.session_cookie.to_sym].should be_nil
        unauthorized_error
      end

      it 'does not set cookie if user\'s session does not have linkedin access' do
        api_session.stub(:has_linkedin_access?).and_return(false)
        post url, {user_uuid: user.uuid, timestamp: @timestamp, signature: @signature}
        cookies.signed[job_board.session_cookie.to_sym].should be_nil
        unauthorized_error
      end

      it 'check session has linkedin access' do
        api_session.should_receive(:has_linkedin_access?)
        post url, {user_uuid: user.uuid, timestamp: @timestamp, signature: @signature}
      end

      it 'set cookie if session has linkedin access' do
        api_session.stub(:has_linkedin_access?).and_return(true)
        post url, {user_uuid: user.uuid, timestamp: @timestamp, signature: @signature}
        cookies.signed[job_board.session_cookie.to_sym].should == user.session.uuid
      end
    end
  end
end
