require 'spec_helper'
require File.dirname(__FILE__) + '/api_context'

describe Api::V1::ScriptsController, type: :api do

  include_context 'API Context'

  before do
    ENV['JBC_JS_HOST'] = 'REAL_HOST'
  end

  describe '#show' do
    let(:url) { "/api/v1/job_boards/#{job_board.uuid}/sdk.js" }

    subject { last_response }

    describe 'with valid job_board uuid' do

      before { get url }

      its(:status) { should == 200 }

      its(:body) { should_not be_empty }

      it 'returns script with ' do
        last_response.body.index('REAL_HOST').should_not be_nil
      end

      it 'should have cache-control header' do
        last_response.headers['Cache-Control'].should == "max-age=#{1.days}, public"
      end

      it 'should have etag header' do
        last_response.headers['Etag'].should_not be_nil
      end

      it 'should have last-modified header' do
        last_response.headers['last-modified'].should_not be_nil
        DateTime.parse(last_response.headers['last-modified']).to_i.should == job_board.updated_at.to_i
      end
    end

    describe 'with invalid job_board uuid' do
      let(:url) { "/api/v1/job_boards/foo/sdk.js" }

      before { get url }

      its(:status) { should == 200 }

      its(:body) { should == ' ' }
    end

    describe 'with inactive job_board' do
      before do
        job_board.active = false
        job_board.save
        get url
      end

      its(:status) { should == 200 }

      its(:body) { should == ' ' }
    end
  end
end
