require 'spec_helper'
require File.dirname(__FILE__) + '/../api_context'

describe Api::Internal::UsersStatController, type: :api do
  include_context 'API Context'

  let(:url) { "/api/internal/job_boards/#{job_board.uuid}/user_count_by_type.json" }

  let(:other_client) { FactoryGirl.create(:client) }
  let(:other_job_board) { FactoryGirl.create(:job_board, client: other_client) }
  let(:other_url) { "/api/internal/job_boards/#{other_job_board.uuid}/user_count_by_type.json" }

  context "unauthenticated" do
    before do
      get url
    end

    subject { last_response }

    its(:status) { should == 401 }
  end

  context "authenticated" do
    before do
      FactoryGirl.create(:user, job_board: job_board)
      login_as(client)
      get url
      @json = JSON.parse(last_response.body)
    end

    it "should return 200" do
      last_response.status.should == 200
    end

    it "should return json 2 elements" do
      @json["user_count_by_type"].length.should == 2
    end

    it "should return correct number of user type" do
      @json["user_count_by_type"]['job_seeker'].should == 1
      @json["user_count_by_type"]['employer'].should == 0
    end
  end

  context "wrong client" do
    before do
        login_as(client)
      get other_url
    end

    subject { last_response }

    its(:status) { should == 401 }
  end

  context "admin" do
    let(:admin) { FactoryGirl.create(:client, admin: true) }

    before do
      login_as(admin)
      get other_url
    end

    subject { last_response }

    its(:status) { should == 200 }
  end
end