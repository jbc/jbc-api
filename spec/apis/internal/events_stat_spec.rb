require 'spec_helper'
require File.dirname(__FILE__) + '/../api_context'

describe Api::Internal::EventsStatController, type: :api do
  include_context 'API Context'

  let(:url) { "/api/internal/job_boards/#{job_board.uuid}/events_history.json" }

  let(:other_client) { FactoryGirl.create(:client) }
  let(:other_job_board) { FactoryGirl.create(:job_board, client: other_client) }
  let(:other_url) { "/api/internal/job_boards/#{other_job_board.uuid}/events_history.json" }
  let(:event) do
    FactoryGirl.create(:tracking_event, action: "Sign in with LinkedIn",
                       recorded_at: 1.day.ago, job_board: job_board, count: 4)
  end

  context "unauthenticated" do
    before do
      get url
    end

    subject { last_response }

    its(:status) { should == 401 }
  end

  context "authenticated" do
    before do
      @event = event
      login_as(client)
      get url
      @json = JSON.parse(last_response.body)
    end

    it "should return 200" do
      last_response.status.should == 200
    end

    it "should return json more than 1" do
      @json["events"]["Sign in with LinkedIn"].length.should >= 0
    end
  end

  context "wrong client" do

    before do
      login_as(client)
      get other_url
    end

    subject { last_response }

    its(:status) { should == 401 }
  end

  context "admin" do
    let(:admin) { FactoryGirl.create(:client, admin: true) }
    let(:other_job_board) { FactoryGirl.create(:job_board, client: client) }

    before do
      login_as(admin)
      get "/api/internal/job_boards/#{other_job_board.uuid}/events_history.json"
    end

    subject { last_response }

    its(:status) { should == 200 }
  end
end