require 'spec_helper'
require File.dirname(__FILE__) + '/../api_context'

describe Api::Internal::InternalSessionsController, type: :api do
  include_context 'API Context'
  let(:url) { "api/internal/session.json" }

  describe "#create" do
    context "with correct email" do
      before do
        post url, email: client.email, password: client.password
        @json = JSON.parse(last_response.body)
      end

      context "with correct password" do
        it "should return 200" do
          last_response.status.should == 200
        end

        it "should post successfully" do
          @json["client"]["email"].should == client.email
        end

        it "should call sign_in devise" do
          Api::Internal::InternalSessionsController.any_instance.should_receive(:sign_in).with("client", client)
          post url, email: client.email, password: client.password
        end
      end

      context "with incorrect password" do
        before do
          post url, email: client.email, password: "bar"
        end

        it "should return 401" do
          last_response.status.should == 401
        end
      end
    end

    context "with incorrect email" do
      before do
        post url, email: "foo", password: "bar"
        @json = JSON.parse(last_response.body)
      end

      it "should return 401" do
        last_response.status.should == 401
      end
    end
  end

  describe "#delete" do
    before do
      login_as(client)
    end

    it "should return 200" do
      delete url, email: client.email, password: client.password
      last_response.status.should == 200
    end

    it "should call sign_out devise" do
      Api::Internal::InternalSessionsController.any_instance.should_receive(:sign_out).with("client")
      delete url, email: client.email, password: client.password
    end
  end

  describe "#show" do
    context "valid authentication" do
      before do
        login_as(client)
        get url
        @json = JSON.parse(last_response.body)
      end

      it "should return 200" do
        last_response.status.should == 200
      end

      it "should get successfully" do
        @json["client"]["email"].should == client.email
      end
    end

    context "invalid authentication" do
      before do
        get url
      end

      it "should return 401" do
        last_response.status.should == 401
      end
    end
  end
end