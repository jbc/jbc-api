require 'spec_helper'

describe UserEventDailySummary do
  let(:job_board) { FactoryGirl.create(:job_board) }
  let(:user) { FactoryGirl.create(:user, job_board: job_board) }

  let(:daily_summary) do
    UserEventDailySummary.new(user: user,
                              job_board: job_board,
                              date: DateTime.now,
                              event: 'Foo',
                              count: 4)
  end

  subject { daily_summary }

  it { should respond_to(:event) }
  it { should respond_to(:date) }
  it { should respond_to(:job_board) }
  it { should respond_to(:user) }
  it { should respond_to(:count) }
  it { should respond_to(:created_at) }
  it { should respond_to(:updated_at) }
  it { should be_valid }
end
