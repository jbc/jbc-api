require 'spec_helper'

describe Client do

  before { @client = Client.new(name: "Example User", email: "user@example.com", password: "foobar", password_confirmation: "foobar") }

  subject { @client }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:encrypted_password) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:admin?) }

  #devise related method
  it { should respond_to(:send_reset_password_instructions) }
  it { should respond_to(:valid_password?) }
  it { should respond_to(:password_required?) }
  it { should respond_to(:ensure_authentication_token) }

  it { should be_valid }
  it { should_not be_admin }

  describe "with admin attribute set to 'true'" do
    before { @client.toggle!(:admin) }

    it { should be_admin }
  end

  describe "When email is not present" do
    before do
      @client.email = ""
    end
    it { should_not be_valid }
  end

  describe "When name is not present" do
    before do
      @client.name = ""
    end
    it { should_not be_valid }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @client.email = invalid_address
        @client.should_not be_valid
      end
    end
  end

  describe "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @client.email = valid_address
        @client.should be_valid
      end
    end
  end

  describe "when email address is already taken" do
    before do
      user_with_same_email = @client.dup
      user_with_same_email.save
    end

    it { should_not be_valid }
  end


  describe "#create_auth_token" do
    before { @client.ensure_authentication_token }

    its(:authentication_token) { should_not be_blank}
  end

  describe "authentication" do
    before do
      @client.save
      @found_client = Client.find_by_email(@client.email)
    end

    it "with valid password" do
      @found_client.valid_password?(@client.password).should == true
    end

    it "with invalid password" do
      @found_client.valid_password?("invalid").should == false
    end
  end

  describe "Job Boards association" do
    before do
      @client.save
      @job_board = FactoryGirl.create(:job_board, client: @client)
    end

    it "should have 1 or many job_board" do
      @client.job_boards.should == [@job_board]
    end

    it "should destroy associated job boards" do
      job_boards = @client.job_boards
      @client.destroy
      job_boards.each do |job_board|
        JobBoard.find_by_id(job_board.id).should be_nil
      end
    end
  end
end