require 'spec_helper'

describe Template do
  let(:job_board) { FactoryGirl.create(:job_board) }
  let(:template) { Template.new(name: "name", value: "value") }
  
  before do
    template.job_board = job_board
  end

  subject { template }

  it { should respond_to(:name) }
  it { should respond_to(:value) }
  it { should respond_to(:job_board) }
  it { should be_valid }

  context "validation" do
    it "name is required" do
      template.name = nil
      template.should_not be_valid
    end

    it "value is required" do
      template.value = nil
      template.should_not be_valid
    end

    it "job board is required" do
      template.job_board = nil
      template.should_not be_valid
    end

    it "name is unique for each job board" do
      template.save!
      template2 = Template.new(name: "name", value: "value")
      template2.job_board = job_board
      template2.should_not be_valid
    end

    it "name is not unique across job board" do
      template.save!
      template2 = Template.new(name: "name", value: "value")
      template2.job_board = FactoryGirl.create(:job_board)
      template2.should be_valid
    end
  end

  describe '#update_cache_async' do
    before do
      @delay = double('delay', update_cache: true)
      template.save
    end

    it 'calls update_cache delayed' do
      JobBoard.should_receive(:delay).and_return(@delay)
      expect(@delay).to receive(:update_cache).with(job_board.id)
      template.save
    end
  end
end
