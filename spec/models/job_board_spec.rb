require 'spec_helper'

describe JobBoard do
  let(:client) { FactoryGirl.create(:client) }

  let(:job_board) { JobBoard.new(name: "foo",
                                 url: "bar",
                                 linkedin_api_key: "foobar",
                                 linkedin_api_secret: "foobar",
                                 session_cookie: "foobar") }


  before { job_board.client = client }

  subject { job_board }

  it { should respond_to(:name) }
  it { should respond_to(:url) }
  it { should respond_to(:linkedin_api_key) }
  it { should respond_to(:linkedin_api_secret) }
  it { should respond_to(:mixpanel_api_key) }
  it { should respond_to(:mixpanel_api_secret) }
  it { should respond_to(:mixpanel_token) }
  it { should respond_to(:linkedin_redirect_url) }
  it { should respond_to(:uuid) }
  it { should respond_to(:client) }
  it { should respond_to(:sign_out_url) }
  it { should respond_to(:linkedin_unlink_url) }
  it { should respond_to(:logo_url) }
  it { should respond_to(:debug) }
  it { should respond_to(:is_debug?) }

  it { should be_valid }

  describe "Validation" do
    describe "empty name" do
      before { job_board.name = "" }

      it { should_not be_valid }
    end

    describe 'empty url' do
      before { job_board.url = "" }

      it { should_not be_valid }
    end

    describe 'empty session cookie' do
      before { job_board.session_cookie = ""}

      it { should_not be_valid }
    end

    describe 'duplicatedsession cookie' do
      before { @another_job_board = FactoryGirl.create(:job_board, session_cookie: 'foobar') }

      it "should not be valid" do
        job_board.should_not be_valid
      end
    end

    describe 'session cookie contains special characters' do
      before { job_board.session_cookie = "+++@" }

      it { should_not be_valid }
    end

    describe 'session cookie is longer than 10' do
      before { job_board.session_cookie = "1234567890abc" }

      it { should_not be_valid }
    end
  end

  describe '#is_debug?' do
    it 'returns debug' do
      job_board.debug.should == job_board.is_debug?
      job_board.debug = false
      job_board.is_debug?.should == false
    end
  end

  describe '#users' do
    let(:user) { FactoryGirl.create(:user) }

    its(:users) { should be_empty }

    describe 'many-to-many relationship' do
      before do
        job_board.users << user
        job_board.users.should_not be_empty
        job_board.users[0].should == user

        5.times do
          job_board.users << FactoryGirl.create(:user)
        end
      end

      its(:users) { should_not be_empty }

      it 'users should be 6' do
        job_board.users.size.should == 6
      end

    end
  end

  describe '#tracking_event' do
    let(:tracking_event) { FactoryGirl.create(:tracking_event) }

    its(:tracking_events) { should be_empty }

    describe 'many-to-many relationship' do
      before do
        job_board.tracking_events << tracking_event
        job_board.tracking_events.should_not be_empty
        job_board.tracking_events[0].should == tracking_event

        5.times do
          job_board.tracking_events << FactoryGirl.create(:tracking_event)
        end
      end

      its(:tracking_events) { should_not be_empty }

      it 'should have 6 tracking_events' do
        job_board.tracking_events.size.should == 6
      end
    end
  end

  describe '#save' do
    before do
      @js_version = FactoryGirl.create(:js_version)
      job_board.save
    end

    it 'populates uuid' do
      job_board.uuid.should_not be_nil
    end

    it 'populates uuid' do
      job_board.js_version.should == @js_version
    end
  end

  describe "#get_id_by_uuid" do
    before { job_board.save }

    it 'returns id of job board by uuid' do
      JobBoard.get_id_by_uuid(job_board.uuid).should == job_board.id
    end

    it 'raise RecordNotFound exception if given invalid uuid' do
      expect {JobBoard.get_id_by_uuid('foo')}.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  describe '#after_save' do
    before do
      job_board.save # pre-save to get id
    end
    it 'calls update_cache_async job' do
      job_board.should_receive(:update_cache_async)
      job_board.save
    end
  end

  describe '#update_cache_async' do
    before do
      @delay = double('delay', update_cache: true)
      job_board.save
    end

    it 'calls update_cache delayed' do
      JobBoard.should_receive(:delay).and_return(@delay)
      expect(@delay).to receive(:update_cache).with(job_board.id)
      job_board.save
    end
  end

  describe '.update_cache' do
    let(:job_board) { FactoryGirl.create(:job_board) }

    it 'creates new instance of JobBoardCacheSweeper' do
      JobBoardCacheSweeper.should_receive(:new).and_call_original
      JobBoard.update_cache(job_board.id)
    end
    it 'calls sweep method of JobBoardCacheSweeper' do
      JobBoardCacheSweeper.any_instance.should_receive(:sweep).with(job_board)
      JobBoard.update_cache(job_board.id)
    end
  end
end