require 'spec_helper'

describe JsVersion do

  before { @js_version = JsVersion.new(version: 'foo_234') }

  subject { @js_version }

  it { should be_valid }
  it { should respond_to(:version) }

  describe '#version' do
    describe 'is unique' do
      before do
        @js_version.save()
        @another = JsVersion.new(version: 'foo_234')
      end

      subject { @another }

      it { should_not be_valid }
    end
    describe 'cannot be empty' do
      before do
        @js_version.version = nil
      end
      it { should_not be_valid }
    end
  end

  describe '#latest' do
    before do
      @first = JsVersion.create(version: 'test1')
      @second = JsVersion.new(version: 'test2')
      @second.created_at = 1.day.from_now
      @second.save()
    end

    it 'returns the latest version' do
      JsVersion.latest.should == @second
    end
  end

  describe '#to_s' do
    it 'returns version' do
      @js_version.to_s.should == @js_version.version
    end
  end
end