require 'spec_helper'

describe TrackingEvent do

  let(:job_board) { FactoryGirl.create(:job_board) }

  let(:event) do
    TrackingEvent.new(category: "LinkedIn",
                      action: "Loaded",
                      recorded_at: DateTime.now,
                      job_board: job_board,
                      count: 4)
  end

  subject { event }

  it { should respond_to(:category) }
  it { should respond_to(:action) }
  it { should respond_to(:recorded_at) }
  it { should respond_to(:job_board) }
  it { should respond_to(:count) }
  it { should be_valid }

  describe "Validation" do
    describe "category" do
      describe "is required" do
        before { event.category = "" }
        it { should_not be_valid }
      end
    end

    describe "action" do
      before { event.action = "" }
      it { should_not be_valid }
    end

    describe "job_board" do
      before { event.job_board = nil }
      it { should_not be_valid }
    end

    describe "count" do
      describe "is required" do
        before { event.count = nil }
        it { should_not be_valid }
      end
      describe "is greater or equal than 0" do
        before { event.count = -5 }
        it { should_not be_valid }
        it "" do
          event.count = 0
          event.should be_valid
        end
      end
    end
  end
end
