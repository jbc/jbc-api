require 'spec_helper'

describe Session do

  let(:user) { FactoryGirl.create(:user) }

  let(:session) do
    session = Session.new
    session.linkedin_atoken = "foo"
    session.linkedin_asecret = "foo"
    session.user_id = user.id
    session
  end

  let(:job_board) {FactoryGirl .create(:job_board) }

  subject { session }

  it { should respond_to(:linkedin_atoken) }
  it { should respond_to(:linkedin_asecret) }
  it { should respond_to(:linkedin_expires_at) }
  it { should respond_to(:has_linkedin_access) }
  it { should respond_to(:has_linkedin_access?) }
  it { should respond_to(:user_id) }
  it { should respond_to(:created_at) }
  it { should be_valid }

  describe "#validation" do
    context "empty user_id" do
      before { session.user_id = '' }
      it { should_not be_valid }
    end
  end

  describe '#delete_linkedin_access' do
    before { session.delete_linkedin_access! }

    its(:linkedin_atoken) { should be_nil}
    its(:linkedin_asecret) { should be_nil}
    its(:linkedin_expires_at) { should be_nil}
  end

  describe '#has_linkedin_access?' do
    before { session.linkedin_expires_at = 10.days.from_now }
    subject { session.has_linkedin_access? }

    it { should be_true }

    context 'when atoken is nil' do
      before { session.linkedin_atoken = nil }
      it { should be_false }
    end

    context 'when asecret is nil' do
      before { session.linkedin_asecret = nil }
      it { should be_false }
    end

    context 'when expires_at is nil' do
      before { session.linkedin_expires_at = nil }
      it { should be_false }
    end

    context 'when expires_at is in the past' do
      before { session.linkedin_expires_at = 10.days.ago }
      it { should be_false }
    end
  end

  describe "#save" do
    before { session.save }

    its(:uuid) { should_not be_nil }
  end
end
