require 'spec_helper'

describe ImportTaskStatus do
  let (:ImportTaskStatus) { ImportTaskStatus.new(task_name: 'foo', snitch_id: 'bar', last_successful: DateTime.now) }

  it { should respond_to(:task_name) }
  it { should respond_to(:snitch_id) }
  it { should respond_to(:last_successful) }
end
