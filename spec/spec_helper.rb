require 'rubygems'
require 'spork'
require 'simplecov'
#uncomment the following line to use spork with the debugger
#require 'spork/ext/ruby-debug'

Spork.prefork do
  if ENV["RUBYMINE_HOME"]
    #puts File.expand_path("rb/testing/patch/common", ENV["RUBYMINE_HOME"])
    $:.unshift(File.expand_path("rb/testing/patch/common", ENV["RUBYMINE_HOME"]))
    $:.unshift(File.expand_path("rb/testing/patch/bdd", ENV["RUBYMINE_HOME"]))
  end
  # simplecov configuration specific for spork
  # https://github.com/colszowka/simplecov/issues/42#issuecomment-4440284
  unless ENV['DRB']
    SimpleCov.start 'rails' do
      add_filter '/app/admin'
    end
  end
# This file is copied to spec/ when you run 'rails generate rspec:install'
  ENV["RAILS_ENV"] ||= 'test'
  require File.expand_path("../../config/environment", __FILE__)
  require 'rspec/rails'
  require 'rspec/autorun'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
  Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

  RSpec.configure do |config|
    # ## Mock Framework
    #
    # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
    #
    # config.mock_with :mocha
    # config.mock_with :flexmock
    # config.mock_with :rr

    # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
    config.fixture_path = "#{::Rails.root}/spec/fixtures"

    # If you're not using ActiveRecord, or you'd prefer not to run each of your
    # examples within a transaction, remove the following line or assign false
    # instead of true.
    config.use_transactional_fixtures = true

    # If true, the base class of anonymous controllers will be inferred
    # automatically. This will be the default behavior in future versions of
    # rspec-rails.
    config.infer_base_class_for_anonymous_controllers = false
    config.include Warden::Test::Helpers
    Warden.test_mode!
  end

end

Spork.each_run do
  # This code will be run each time you run your specs.
  # simplecov configuration specific for spork
  # https://github.com/colszowka/simplecov/issues/42#issuecomment-4440284
  if ENV['DRB']
    SimpleCov.start 'rails' do
      add_filter '/app/admin'
    end
  end
end

SimpleCov.minimum_coverage 85
SimpleCov.refuse_coverage_drop
