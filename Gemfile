source 'https://rubygems.org'
ruby '2.1.3'

gem 'rails', '4.0.3'
gem 'unicorn', '4.7.0'
gem 'mixpanel_client'

gem 'pg'
gem 'rabl'

gem 'mixpanel'
gem 'whenever'
# gems for google api
gem 'signet', '0.4.5'
gem 'google-api-client', '~> 0.6.2'
gem 'fog', '~> 1.10.0'

# gems for activeadmin
gem 'activeadmin', github: 'gregbell/active_admin', branch: 'master'
gem 'formtastic', github: 'justinfrench/formtastic', branch: 'master'
#gem 'meta_search',  '>= 1.1.3'
gem 'cancan', '~>1.6.10'
gem 'dead_mans_snitch'

gem 'linkedin'
gem 'uuidtools', '2.1.2'

gem 'log4r', '1.1.10'
gem 'lograge'

# used to speedn up generate_sample_data
gem 'peach'
gem "health_check"
# caching
gem 'memcachier'
gem 'dalli'

group :development, :test do
  gem 'rspec-rails', '~> 2.14'
  gem 'guard-rspec', "~> 1.0.1"
  gem 'timecop'
end


# Gems used only for assets and not required
# in production environments by default.
gem 'sass-rails',   '4.0.1'
gem 'coffee-rails', '4.0.1'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', '0.12.0', :platforms => :ruby

gem 'uglifier', '2.3.2'

gem "figaro"
gem 'jquery-rails'

group :test do
  gem 'rb-fsevent', :require => false
  gem 'ruby_gntp'
  gem 'guard-spork', '0.3.2'
  gem "spork-rails"
  gem 'factory_girl_rails', '4.1.0'
  gem 'simplecov', :require => false
end

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

gem 'cloudflare'

gem 'capistrano', '2.15.5'

group :staging, :production do
  gem 'newrelic_rpm'
end

# add these gems to help with the transition to Rails 4
gem 'actionpack-page_caching'
gem 'actionpack-action_caching'

gem 'daemons'
gem 'delayed_job_active_record'
gem 'rest-client'