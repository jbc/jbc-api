module Mixpanel
  module Labels
    SIGN_IN_WITH_LINKEDIN = "Sign in with LinkedIn"
    VIEW_COMPANY_CONNECTIONS = "View company connections"
    VIEW_RELATIONSHIP = "View Relationship"
    SIGN_IN_BUTTON_DISPLAYED = "Sign in button displayed"
  end
end