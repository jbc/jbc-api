class InternalDeviseFailure < Devise::FailureApp
  def failure
    respond_to do |format|
      format.html {super}
      format.json {
        warden.custom_failure!
        render :json => {:success => false, :errors => ["Login Failed"]}
      }
    end
  end
end