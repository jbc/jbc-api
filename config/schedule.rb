# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "/home/deployer/apps/jbc-api/current/log/cron.log"
job_type :rake_with_monitoring,    "cd :path && :environment_variable=:environment bundle exec rake :task --silent :output && curl https://nosnch.in/:snitch"
#
every 1.day, at: '4:00 am' do
  rake "analytics:import_user_daily_events"
end
every 1.day, at: '5:00 am' do            # midnight central time
  rake "ga_import"
end
