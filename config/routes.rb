JbcApi::Application.routes.draw do
  devise_for :clients, {
      class_name: 'Client',
      module: :devise
  }.merge(ActiveAdmin::Devise.config)

  ActiveAdmin.routes(self)

  root to: "admin/clients#index"

  # this route is for templates.js, we nest it under job_boards resource so we can have /:job_board_id/templates.json,
  # but we don't care about /:job_board_id.json hence only: []
  resources :job_boards, only: [] do
  end

  get 'xdrpc' => "xdrpc#show"

  # api routings
  namespace :api do
    namespace :internal do
      resources :job_boards, only: [:show, :index, :update] do
        get 'user_count_by_type' => "users_stat#user_count_by_type"
        get 'events_history' => "events_stat#events_history"
      end
      resource :internal_sessions, path: "session",  only: [:create, :destroy, :show]
    end
    namespace :v1 do
      resources :job_boards, only: [] do
        resources :users, only: [:show, :index, :update] do
          member do
            # use both POST and PUT for update because it's easier for our client to submit POST request
            post '' => 'users#update'
          end
        end
        namespace :linkedin do
          resource :oauth, path: 'oauth', only: [:destroy, :new] do
            member do
              get 'callback'
              post 'switch_user'
            end
          end

          resources :connections, only: [:show, :index]

          resources :profiles, only: [:show]
        end

        # client script
        get "sdk" => "scripts#show"

        #session
        resources :sessions, only: [:show, :index]
      end

    end
  end
end
