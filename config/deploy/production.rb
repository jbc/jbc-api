set :rails_env, 'production'
default_environment["RAILS_ENV"] = rails_env
set :branch, 'release'
set :unicorn_workers, 10

server "prod-api1", :web, :app, :db, primary: true
server "prod-api2", :web, :app