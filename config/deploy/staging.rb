set :rails_env, 'staging'
default_environment["RAILS_ENV"] = rails_env
set :branch, "master"

set :unicorn_workers, 3

server "staging-api1", :web, :app, :cron, :db, primary: true
#server "staging-api2", :web, :app