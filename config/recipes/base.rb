def template(from, to)
  erb = File.read(File.expand_path("../templates/#{from}", __FILE__))
  put ERB.new(erb).result(binding), to
end

def set_default(name, *args, &block)
  set(name, *args, &block) unless exists?(name)
end

##
# Run a command and ask for input when input_query is seen.
# Sends the response back to the server.
#
# +input_query+ is a regular expression that defaults to /^Password/.
# Can be used where +run+ would otherwise be used.
# run_with_input 'ssh-keygen ...', /^Are you sure you want to overwrite\?/
def run_with_input(shell_command, input_query=/^password/, response=nil)
  handle_command_with_input(:run, shell_command, input_query, response)
end

##
# Does the actual capturing of the input and streaming of the output.
#
# local_run_method: run or sudo
# shell_command: The command to run
# input_query: A regular expression matching a request for input: /^Please enter your password/
def handle_command_with_input(local_run_method, shell_command, input_query, response=nil)
  send(local_run_method, shell_command, {:pty => true}) do |channel, stream, data|
    if data =~ input_query
      if response
        logger.info "#{data} #{"*"*(rand(10)+5)}", channel[:host]
        channel.send_data "#{response}\n"
      else
        logger.info data, channel[:host]
        response = ::Capistrano::CLI.password_prompt "#{data}"
        channel.send_data "#{response}\n"
      end
    else
      logger.info data, channel[:host]
    end
  end
end

namespace :deploy do
  task :install do
    run "#{sudo} apt-get -y update"
    run "#{sudo} apt-get -y install python-software-properties"
    run "#{sudo} apt-get -y install curl"
    run "#{sudo} apt-get -y install vim"
  end

  task :install_pg, roles: :app do
    run "#{sudo} apt-get -y install postgresql-client"
    run "#{sudo} apt-get -y install libpq-dev"
  end
  after 'deploy:install', 'deploy:install_pg'

end

namespace :utils do
  desc "Symlink the database.yml and application.yml file into latest release"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    run "ln -nfs #{shared_path}/config/application.yml #{release_path}/config/application.yml"
  end
  after "deploy:finalize_update", "utils:symlink"
end