namespace :newrelic do
  desc 'Install newrelic server monitor package'
  task :install do
    run "#{sudo} wget -O /etc/apt/sources.list.d/newrelic.list http://download.newrelic.com/debian/newrelic.list"
    run "#{sudo} apt-key adv --keyserver hkp://subkeys.pgp.net --recv-keys 548C16BF"
    run "#{sudo} apt-get update"
    run "#{sudo} apt-get install newrelic-sysmond"
  end
  after 'deploy:install', 'newrelic:install'

  task :setup do
    run "#{sudo} nrsysmond-config --set license_key=#{newrelic_key}"
  end
  after 'newrelic:install', 'newrelic:setup'

  %w[start stop].each do |command|
    desc "#{command} newrelic"
    task command do
      run "#{sudo} /etc/init.d/newrelic-sysmond #{command}"
    end
  end
end