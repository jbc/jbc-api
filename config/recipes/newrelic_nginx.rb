
namespace :nginx_newrelic do
  desc 'Install nginx plugin for newrelic'
  task :install, roles: :web do
    run "#{sudo} rm -rf /usr/local/newrelic_nginx_agent"
    run "wget -O /tmp/newrelic_nginx_agent.tar.gz http://nginx.com/download/newrelic/newrelic_nginx_agent.tar.gz"
    run "gzip -dc /tmp/newrelic_nginx_agent.tar.gz | tar xvf -"
    run "rm -rf /tmp/newrelic_nginx_agent.tar.gz"
    run "#{sudo} mv newrelic_nginx_agent /usr/local"
    run "#{sudo} chown deployer:admin /usr/local/newrelic_nginx_agent"
    template 'nginx_newrelic.erb', '/tmp/newrelic_plugin.yml'
    run "mv /tmp/newrelic_plugin.yml /usr/local/newrelic_nginx_agent/config"
    run "cd /usr/local/newrelic_nginx_agent;bundle install"

    run "printf \"#{nginx_status_username}:$(openssl passwd -apr1 #{nginx_status_password})\n\" >> /tmp/.htpasswd"
    run "#{sudo} mv /tmp/.htpasswd /etc/nginx/.htpasswd"
    run "#{sudo} chown www-data /etc/nginx/.htpasswd"
    run "#{sudo} chmod 600 /etc/nginx/.htpasswd"
  end

  %w[start stop].each do |command|
    desc "#{command} nginx newrelic plugin"
    task command do
      run "/usr/local/newrelic_nginx_agent/newrelic_nginx_agent.daemon #{command}"
    end
  end
end