class AddPercentageToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :percentage, :integer, default: 100
    JobBoard.all.each do |jb|
      jb.percentage = 100
      jb.save
    end
  end
end
