class CreateTemplates < ActiveRecord::Migration
  def up
    create_table :templates do |t|
      t.string :name
      t.integer :job_board_id
      t.text :value

      t.timestamps
    end

    add_index :templates, [:job_board_id, :name], unique: true
  end

  def down
    remove_index :templates, column: [:job_board_id, :name]
    drop_table :templates
  end
end
