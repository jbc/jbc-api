class AddClientIdIndexToJobBoards < ActiveRecord::Migration
  def change
    add_index :job_boards, :client_id
  end
end
