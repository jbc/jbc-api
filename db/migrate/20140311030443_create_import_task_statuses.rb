class CreateImportTaskStatuses < ActiveRecord::Migration
  def change
    create_table :import_task_statuses do |t|
      t.string :task_name, unique: true
      t.datetime :last_successful
      t.string :snitch_id
      t.timestamps
    end
    add_index :import_task_statuses, :last_successful
  end
end
