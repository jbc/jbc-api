class AddServerSideAuthToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :server_side_auth, :boolean
  end
end
