class FixJsVersionIdColumn < ActiveRecord::Migration
  def change
    add_column :job_boards, :js_version_id_tmp, :integer
    JobBoard.reset_column_information
    JobBoard.all.each do |j|
      j.js_version_id_tmp = j.js_version_id
      j.save
    end
    remove_column :job_boards, :js_version_id
    rename_column :job_boards, :js_version_id_tmp, :js_version_id
  end
end
