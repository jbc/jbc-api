class CreateUserEventDailySummaries < ActiveRecord::Migration
  def change
    create_table :user_event_daily_summaries do |t|
      t.integer :user_id, null: false
      t.integer :job_board_id, null: false
      t.string :event, null: false
      t.date :date, null: false
      t.integer :count, default: 0
      t.timestamps
    end
    add_index :user_event_daily_summaries, [:job_board_id, :date, :event, :user_id], name: 'user_event_daily_summaries_uniq'
  end
end
