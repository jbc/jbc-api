class RemoveSignedInFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :signed_in
  end
end
