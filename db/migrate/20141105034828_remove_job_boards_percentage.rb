class RemoveJobBoardsPercentage < ActiveRecord::Migration
  def change
    remove_column :job_boards, :percentage
  end
end
