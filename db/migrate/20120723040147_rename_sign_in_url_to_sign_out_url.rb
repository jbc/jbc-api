class RenameSignInUrlToSignOutUrl < ActiveRecord::Migration
  def up
    remove_column :job_boards, :sign_in_url
    add_column :job_boards, :sign_out_url, :string
  end

  def down
    add_column :job_boards, :sign_in_url, :string
    remove_column :job_boards, :sign_out_url
  end
end
