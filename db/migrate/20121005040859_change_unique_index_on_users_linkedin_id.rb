class ChangeUniqueIndexOnUsersLinkedinId < ActiveRecord::Migration
  def change
    remove_index :users, :linkedin_id
    add_index :users, [:job_board_id, :linkedin_id], unique: true
  end
end
