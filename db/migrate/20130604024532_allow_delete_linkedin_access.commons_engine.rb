# This migration comes from commons_engine (originally 20130604024429)
class AllowDeleteLinkedinAccess < ActiveRecord::Migration
  def up
    change_column :sessions, :linkedin_atoken, :string, null: true
    change_column :sessions, :linkedin_asecret, :string, null: true
  end

  def down
    change_column :sessions, :linkedin_atoken, :string, null: false
    change_column :sessions, :linkedin_asecret, :string, null: false
  end
end
