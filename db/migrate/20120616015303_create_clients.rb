class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :email
      t.string :name
      t.string :authentication_token
      t.boolean :admin, default: false
      t.timestamps
    end

    add_index :clients, :authentication_token, unique: true
    add_index :clients, :email, unique: true

  end
end
