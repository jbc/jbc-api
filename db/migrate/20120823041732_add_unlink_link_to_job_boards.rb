class AddUnlinkLinkToJobBoards < ActiveRecord::Migration
  def up
    add_column :job_boards, :linkedin_unlink_url, :string
  end

  def down
    remove_column :job_boards, :linkedin_unlink_url
  end
end
