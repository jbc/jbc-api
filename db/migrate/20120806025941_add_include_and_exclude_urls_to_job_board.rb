class AddIncludeAndExcludeUrlsToJobBoard < ActiveRecord::Migration
  def up
    add_column :job_boards, :include_urls, :string, default: ".*"
    add_column :job_boards, :exclude_urls, :string
  end

  def down
    remove_column :job_boards, :include_urls
    remove_column :job_boards, :exclude_urls
  end
end
