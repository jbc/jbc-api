class CreateJsVersions < ActiveRecord::Migration
  def change
    create_table :js_versions do |t|
      t.string :version
      t.boolean :stable

      t.timestamps
    end
    add_index :js_versions, :version, :unique => true
    add_index :js_versions, :created_at, :unique => false
    add_column :job_boards, :js_version_id, :string
  end
end
