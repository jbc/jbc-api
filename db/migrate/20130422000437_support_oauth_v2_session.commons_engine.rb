# This migration comes from commons_engine (originally 20130421235531)
class SupportOauthV2Session < ActiveRecord::Migration
  def change
    remove_column :sessions, :linkedin_asecret
    add_column :sessions, :linkedin_expires_at, :datetime
  end
end