class AddActiveToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :active, :boolean, default: true
  end
end
