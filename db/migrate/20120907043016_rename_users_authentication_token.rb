class RenameUsersAuthenticationToken < ActiveRecord::Migration
  def up
    rename_column :users, :authentication_token, :access_code
  end

  def down
    rename_column :users, :access_code, :authentication_token
  end
end
