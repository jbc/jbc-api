# This migration comes from commons_engine (originally 20130324105041)
class RemoveAccessCodeFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :access_code
  end
end
