class CreateOauthTokens < ActiveRecord::Migration
  def change
    create_table :oauth_tokens do |t|
      t.string :request_token
      t.string :request_secret

      t.timestamps
    end
    add_index :oauth_tokens, :request_token, unique: true
  end
end
