class AddLinkedinAccessTokenToUsers < ActiveRecord::Migration
  def up
    add_column :users, :linkedin_atoken, :string
    add_column :users, :linkedin_asecret, :string
  end

  def down
    remove_column :users, :linkedin_atoken
    remove_column :users, :linkedin_asecret
  end
end
