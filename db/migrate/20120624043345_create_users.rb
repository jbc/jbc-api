class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uuid
      t.integer :job_board_id
      t.string :linkedin_id
      t.string :external_id
      t.timestamps
    end
    add_index :users, :uuid, unique: true
    add_index :users, :linkedin_id, unique: true
    add_index :users, :external_id, unique: false
  end
end
