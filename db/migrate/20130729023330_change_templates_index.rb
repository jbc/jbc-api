class ChangeTemplatesIndex < ActiveRecord::Migration
  def change
    remove_index :templates, column: [:job_board_id, :name]
    add_index :templates, :job_board_id
  end
end
