class CreateSessions < ActiveRecord::Migration
  def self.up
    create_table :sessions do |t|
      t.string :linkedin_atoken, null: false
      t.string :linkedin_asecret, null: false
      t.string :uuid, null: false
      t.integer :user_id, null: false

      t.timestamps
    end
    add_index :sessions, :uuid, unique: true
    add_index :sessions, :user_id, unique: true
  end

  def self.down
    drop_table :sessions
  end
end
