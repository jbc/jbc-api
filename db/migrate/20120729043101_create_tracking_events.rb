class CreateTrackingEvents < ActiveRecord::Migration
  def change
    create_table :tracking_events do |t|
      t.string :category
      t.string :action
      t.integer :job_board_id
      t.text :additional_json
      t.integer :count
      t.date :recorded_at

      t.timestamps
    end
    add_index :tracking_events, [:job_board_id, :category, :action, :recorded_at], name: "index_tracking_events_on_client_category_date"

  end
end