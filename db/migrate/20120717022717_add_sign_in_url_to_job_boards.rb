class AddSignInUrlToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :sign_in_url, :string
  end
end
