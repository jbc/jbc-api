class RemoveLinkedinColumnsFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :linkedin_atoken
    remove_column :users, :linkedin_atoken_created_at
    remove_column :users, :linkedin_asecret
  end
end
