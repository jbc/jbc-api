class CreateJobBoards < ActiveRecord::Migration
  def change
    create_table :job_boards do |t|
      t.string :uuid
      t.string :name
      t.string :url
      t.string :linkedin_api_key
      t.string :linkedin_api_secret
      t.string :linkedin_redirect_url, :string
      t.integer :client_id

      t.timestamps
    end
    add_index :job_boards, :uuid, unique: true
  end
end
