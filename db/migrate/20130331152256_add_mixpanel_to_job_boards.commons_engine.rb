# This migration comes from commons_engine (originally 20130331151705)
class AddMixpanelToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :mixpanel_api_key, :string
    add_column :job_boards, :mixpanel_api_secret, :string
    add_column :job_boards, :mixpanel_token, :string
  end
end
