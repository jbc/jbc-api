class AddSessionCookieToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :session_cookie, :string
  end
end
