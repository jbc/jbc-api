class AddLinkedinAtokenCreatedAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :linkedin_atoken_created_at, :datetime
  end
end
