# This migration comes from commons_engine (originally 20130327225625)
class AddLogoUrlToJobBoard < ActiveRecord::Migration
  def change
    add_column :job_boards, :logo_url, :string
  end
end
