class AddPercentageOfViewersToJobBoards < ActiveRecord::Migration
  def change
    add_column :job_boards, :percentage_of_viewers, :float, default: 0.0
  end
end
