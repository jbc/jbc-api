class DeleteUnusedColumns < ActiveRecord::Migration
  def up
    remove_column :job_boards, :include_urls
    remove_column :job_boards, :exclude_urls
    remove_column :job_boards, :server_side_auth
    remove_column :job_boards, :percentage_of_viewers
  end

  def down
  end
end
