# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141105034828) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: true do |t|
    t.string   "email"
    t.string   "name"
    t.string   "authentication_token"
    t.boolean  "admin",                  default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
  end

  add_index "clients", ["authentication_token"], name: "index_clients_on_authentication_token", unique: true, using: :btree
  add_index "clients", ["email"], name: "index_clients_on_email", unique: true, using: :btree
  add_index "clients", ["reset_password_token"], name: "index_clients_on_reset_password_token", unique: true, using: :btree

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "import_task_statuses", force: true do |t|
    t.string   "task_name"
    t.datetime "last_successful"
    t.string   "snitch_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "import_task_statuses", ["last_successful"], name: "index_import_task_statuses_on_last_successful", using: :btree

  create_table "job_boards", force: true do |t|
    t.string   "uuid"
    t.string   "name"
    t.string   "url"
    t.string   "linkedin_api_key"
    t.string   "linkedin_api_secret"
    t.string   "linkedin_redirect_url"
    t.integer  "client_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "sign_out_url"
    t.string   "linkedin_unlink_url"
    t.string   "logo_url"
    t.string   "mixpanel_api_key"
    t.string   "mixpanel_api_secret"
    t.string   "mixpanel_token"
    t.boolean  "debug"
    t.string   "session_cookie"
    t.integer  "js_version_id"
    t.boolean  "allow_switch_user"
    t.boolean  "active",                default: true
    t.integer  "integration_version",   default: 1
  end

  add_index "job_boards", ["client_id"], name: "index_job_boards_on_client_id", using: :btree
  add_index "job_boards", ["uuid"], name: "index_job_boards_on_uuid", unique: true, using: :btree

  create_table "js_versions", force: true do |t|
    t.string   "version"
    t.boolean  "stable"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "js_versions", ["created_at"], name: "index_js_versions_on_created_at", using: :btree
  add_index "js_versions", ["version"], name: "index_js_versions_on_version", unique: true, using: :btree

  create_table "sessions", force: true do |t|
    t.string   "linkedin_atoken"
    t.string   "uuid",                null: false
    t.integer  "user_id",             null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.datetime "linkedin_expires_at"
    t.string   "linkedin_asecret"
  end

  add_index "sessions", ["user_id"], name: "index_sessions_on_user_id", unique: true, using: :btree
  add_index "sessions", ["uuid"], name: "index_sessions_on_uuid", unique: true, using: :btree

  create_table "templates", force: true do |t|
    t.string   "name"
    t.integer  "job_board_id"
    t.text     "value"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "templates", ["job_board_id"], name: "index_templates_on_job_board_id", using: :btree

  create_table "tracking_events", force: true do |t|
    t.string   "category"
    t.string   "action"
    t.integer  "job_board_id"
    t.text     "additional_json"
    t.integer  "count"
    t.date     "recorded_at"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "tracking_events", ["job_board_id", "category", "action", "recorded_at"], name: "index_tracking_events_on_client_category_date", using: :btree

  create_table "user_event_daily_summaries", force: true do |t|
    t.integer  "user_id",                  null: false
    t.integer  "job_board_id",             null: false
    t.string   "event",                    null: false
    t.date     "date",                     null: false
    t.integer  "count",        default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_event_daily_summaries", ["job_board_id", "date", "event", "user_id"], name: "user_event_daily_summaries_uniq", using: :btree

  create_table "users", force: true do |t|
    t.string   "uuid"
    t.integer  "job_board_id"
    t.string   "linkedin_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "user_type",            default: "job_seeker"
    t.string   "authentication_token"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["job_board_id", "linkedin_id"], name: "index_users_on_job_board_id_and_linkedin_id", unique: true, using: :btree
  add_index "users", ["uuid"], name: "index_users_on_uuid", unique: true, using: :btree

end
