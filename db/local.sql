--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: clients; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE clients (
    id integer NOT NULL,
    email character varying(255),
    name character varying(255),
    authentication_token character varying(255),
    admin boolean DEFAULT false,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255)
);


ALTER TABLE public.clients OWNER TO jbc;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE clients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO jbc;

--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE clients_id_seq OWNED BY clients.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying(255),
    queue character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.delayed_jobs OWNER TO jbc;

--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delayed_jobs_id_seq OWNER TO jbc;

--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE delayed_jobs_id_seq OWNED BY delayed_jobs.id;


--
-- Name: job_boards; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE job_boards (
    id integer NOT NULL,
    uuid character varying(255),
    name character varying(255),
    url character varying(255),
    linkedin_api_key character varying(255),
    linkedin_api_secret character varying(255),
    linkedin_redirect_url character varying(255),
    client_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    sign_out_url character varying(255),
    linkedin_unlink_url character varying(255),
    logo_url character varying(255),
    mixpanel_api_key character varying(255),
    mixpanel_api_secret character varying(255),
    mixpanel_token character varying(255),
    debug boolean,
    session_cookie character varying(255),
    js_version_id integer,
    allow_switch_user boolean
);


ALTER TABLE public.job_boards OWNER TO jbc;

--
-- Name: job_boards_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE job_boards_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.job_boards_id_seq OWNER TO jbc;

--
-- Name: job_boards_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE job_boards_id_seq OWNED BY job_boards.id;


--
-- Name: js_versions; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE js_versions (
    id integer NOT NULL,
    version character varying(255),
    stable boolean,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.js_versions OWNER TO jbc;

--
-- Name: js_versions_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE js_versions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.js_versions_id_seq OWNER TO jbc;

--
-- Name: js_versions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE js_versions_id_seq OWNED BY js_versions.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


ALTER TABLE public.schema_migrations OWNER TO jbc;

--
-- Name: sessions; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE sessions (
    id integer NOT NULL,
    linkedin_atoken character varying(255),
    uuid character varying(255) NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    linkedin_expires_at timestamp without time zone,
    linkedin_asecret character varying(255)
);


ALTER TABLE public.sessions OWNER TO jbc;

--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sessions_id_seq OWNER TO jbc;

--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE sessions_id_seq OWNED BY sessions.id;


--
-- Name: templates; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE templates (
    id integer NOT NULL,
    name character varying(255),
    job_board_id integer,
    value text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.templates OWNER TO jbc;

--
-- Name: templates_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE templates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.templates_id_seq OWNER TO jbc;

--
-- Name: templates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE templates_id_seq OWNED BY templates.id;


--
-- Name: tracking_events; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE tracking_events (
    id integer NOT NULL,
    category character varying(255),
    action character varying(255),
    job_board_id integer,
    additional_json text,
    count integer,
    recorded_at date,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE public.tracking_events OWNER TO jbc;

--
-- Name: tracking_events_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE tracking_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tracking_events_id_seq OWNER TO jbc;

--
-- Name: tracking_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE tracking_events_id_seq OWNED BY tracking_events.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: jbc; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    uuid character varying(255),
    job_board_id integer,
    linkedin_id character varying(255),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_type character varying(255) DEFAULT 'job_seeker'::character varying,
    authentication_token character varying(255)
);


ALTER TABLE public.users OWNER TO jbc;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: jbc
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO jbc;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jbc
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY clients ALTER COLUMN id SET DEFAULT nextval('clients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY delayed_jobs ALTER COLUMN id SET DEFAULT nextval('delayed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY job_boards ALTER COLUMN id SET DEFAULT nextval('job_boards_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY js_versions ALTER COLUMN id SET DEFAULT nextval('js_versions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY sessions ALTER COLUMN id SET DEFAULT nextval('sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY templates ALTER COLUMN id SET DEFAULT nextval('templates_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY tracking_events ALTER COLUMN id SET DEFAULT nextval('tracking_events_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: jbc
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: clients; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY clients (id, email, name, authentication_token, admin, created_at, updated_at, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip) FROM stdin;
3	foo@bar.com	tmp	G31ov8masmp2gYdBMZyb	f	2013-02-23 03:03:44.889695	2013-02-23 03:03:44.889695	$2a$10$V1M5BcpDZkBwn85tBnEOzeHoj7evURqma00CxHu1zCKjDm9fmWobC	\N	\N	\N	0	\N	\N	\N	\N
2	test@jobboardcloud.com	HealthJobsNationwide	F6hfBseXUqeBfSoLa8s2	f	2012-07-08 17:32:39.295151	2013-08-13 04:32:37.470574	$2a$10$eGxoCzg9hii8QTZoK2ghQOmaKrCAY3Im9ftt8Xb8beb6TaOXRN7da	\N	\N	\N	36	2013-08-13 04:32:37.402739	2013-08-13 04:31:50.207328	127.0.0.1	127.0.0.1
1	tvu@jobboardcloud.com	JBC	dzWjJ1XFJKF4JrLpayzt	t	2012-07-08 15:12:12.054389	2013-12-14 04:52:47.868742	$2a$10$0vFyMFyvCOQVST5NeM5uR.4AdVuV8rQ1pcQUMccmNcl6TNPT6024W	KLpquyhdnfFq1z58daK6	2012-08-27 00:50:39.773518	2013-09-09 13:54:01.366852	215	2013-12-14 04:52:47.866693	2013-12-13 14:19:57.220195	127.0.0.1	127.0.0.1
\.


--
-- Name: clients_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('clients_id_seq', 3, true);


--
-- Data for Name: delayed_jobs; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY delayed_jobs (id, priority, attempts, handler, last_error, run_at, locked_at, failed_at, locked_by, queue, created_at, updated_at) FROM stdin;
13	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-08-28 03:42:29.816852	\N	\N	\N	\N	2013-08-28 03:42:29.817276	2013-08-28 03:42:29.817276
14	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-09-05 04:31:30.856295	\N	\N	\N	\N	2013-09-05 04:31:30.864887	2013-09-05 04:31:30.864887
15	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-09-14 02:56:26.463174	\N	\N	\N	\N	2013-09-14 02:56:26.464027	2013-09-14 02:56:26.464027
16	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-09-14 14:04:42.251697	\N	\N	\N	\N	2013-09-14 14:04:42.252588	2013-09-14 14:04:42.252588
17	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-09-14 14:06:58.212358	\N	\N	\N	\N	2013-09-14 14:06:58.21325	2013-09-14 14:06:58.21325
18	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-09-14 23:31:08.530826	\N	\N	\N	\N	2013-09-14 23:31:08.532086	2013-09-14 23:31:08.532086
19	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-10-01 02:23:21.848709	\N	\N	\N	\N	2013-10-01 02:23:21.850497	2013-10-01 02:23:21.850497
20	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-11-17 02:43:45.943787	\N	\N	\N	\N	2013-11-17 02:43:45.944381	2013-11-17 02:43:45.944381
21	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-02 04:17:13.169289	\N	\N	\N	\N	2013-12-02 04:17:13.169942	2013-12-02 04:17:13.169942
22	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 2\n	\N	2013-12-11 03:37:21.806015	\N	\N	\N	\N	2013-12-11 03:37:21.806565	2013-12-11 03:37:21.806565
23	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-11 03:37:57.220122	\N	\N	\N	\N	2013-12-11 03:37:57.220572	2013-12-11 03:37:57.220572
24	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-14 05:08:22.672602	\N	\N	\N	\N	2013-12-14 05:08:22.67318	2013-12-14 05:08:22.67318
25	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-14 05:09:08.749345	\N	\N	\N	\N	2013-12-14 05:09:08.749673	2013-12-14 05:09:08.749673
26	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-14 05:09:23.188497	\N	\N	\N	\N	2013-12-14 05:09:23.188805	2013-12-14 05:09:23.188805
27	0	0	--- !ruby/object:Delayed::PerformableMethod\nobject: !ruby/class 'JobBoard'\nmethod_name: :update_cache\nargs:\n- 1\n	\N	2013-12-14 05:09:51.602239	\N	\N	\N	\N	2013-12-14 05:09:51.602573	2013-12-14 05:09:51.602573
\.


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('delayed_jobs_id_seq', 27, true);


--
-- Data for Name: job_boards; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY job_boards (id, uuid, name, url, linkedin_api_key, linkedin_api_secret, linkedin_redirect_url, client_id, created_at, updated_at, sign_out_url, linkedin_unlink_url, logo_url, mixpanel_api_key, mixpanel_api_secret, mixpanel_token, debug, session_cookie, js_version_id, allow_switch_user) FROM stdin;
2	d7b76c6b-9c73-4950-b30b-00aba387c39c	test	test	o3ggtww7iqqp	LyHLXNElLMbZDkyZ	http://localhost:3000/jbc?jbc_id=	1	2012-07-08 19:33:25.004819	2013-12-11 03:37:21.768421		asdfasdf	http://www.healthjobsusa.com/images/logos/logo9.png				t	test	1	f
1	bce7cee8-9bbd-44c5-950f-640d5add11fe	Absolutely Health Care	http://demo-stage.jobboardcloud.com	o3ggtww7iqqp	LyHLXNElLMbZDkyZ	http://localhost:3000/jbc?jbc_id=	2	2012-07-08 17:33:08.317954	2013-12-14 05:09:51.597066	http://localhost:3000/sign_out	dsfsdfsdfadf	http://www.healthjobsusa.com/images/logos/logo9.png	c0dfe397e73621ce990f097394f50f41	a1483250ad332122bd073544a888a698	25d88eff49918dc0ba051df0157d6ce6	t	ahc	1	t
\.


--
-- Name: job_boards_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('job_boards_id_seq', 2, true);


--
-- Data for Name: js_versions; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY js_versions (id, version, stable, created_at, updated_at) FROM stdin;
1	current	\N	2013-06-21 22:24:17.798342	2013-06-21 22:24:17.798342
\.


--
-- Name: js_versions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('js_versions_id_seq', 1, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY schema_migrations (version) FROM stdin;
20120630222819
20120616033721
20120616152423
20120619025306
20120624043345
20120624234041
20120616015303
20120624050851
20120616040308
20120704170026
20120704211349
20120706015850
20120706044505
20120707051433
20120716021233
20120717022717
20120721042748
20120723040147
20120724031646
20120729043101
20120806025941
20120815033816
20120822022304
20120823041732
20120825025205
20120825210104
20120907043016
20120923012914
20121005040859
20121007195551
20121012032338
20121122203231
20130219033351
20130219135201
20130321033254
20130324110033
20130327225729
20130331152256
20130422000437
20130529035654
20130604024532
20130612021710
20130619032007
20130621221557
20130628210747
20130727222259
20130729015749
20130729023330
20130805035524
20131128231452
20131202040738
\.


--
-- Data for Name: sessions; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY sessions (id, linkedin_atoken, uuid, user_id, created_at, updated_at, linkedin_expires_at, linkedin_asecret) FROM stdin;
21	af63c85a-3c98-42a0-b3c3-c6e9a6ef14b5	pRAiR6u8HJnjmuvO9TUpWw	15	2013-03-24 21:30:22.065268	2013-03-24 21:30:22.065268	\N	\N
50	AQVBcau4Q1ndJsJX1ZLTVzAjTX3UCFXJuRS2lZShhJiHWl0I8UylelzT1zHC2a3qcAnhootuA655kmWCD7IMlG4XGq45rjbkC8LkMXwFWl1fjQUVlShEtrR-ZQRWmU73cruyPsKpKgJtLh7CYuClBDxo0KMxOHB9stWX1as9U7acdncDX_w	5w6gz1Fn5KWj1wRZR34Osg	19	2013-04-29 03:14:26.616355	2013-04-29 03:14:26.616355	2013-06-28 03:14:04	\N
54	AQVpxYc3z-SjfrCU8IM5C9gOVjXs2C5_2rINaQuJHttFvxOR8sVZBhR9m3NeHFS3mnSowOPiAgk_ooV6e1VX-Xi5fsvpMwloH_xyx7aXPEgZRP_afGcLLnK0jYZ4c_3cfU5ghrqg9ddgwFHLm52sHQiJ4q7Smykkw277y08edwp3SDTIwM4	4LjfSznrDT9JeOx-SZyDqQ	14	2013-04-29 03:54:16.670205	2013-04-29 03:54:16.670205	2013-06-28 03:54:14	\N
107	834f0d86-8bf9-4ac3-89c6-d0bdf96d3302	uFFYSV6qxbklthVD7TXp4Q	20	2013-06-11 04:46:00.322715	2013-06-11 04:46:00.322715	2013-08-10 04:45:48.458749	4d6837a7-2048-4c78-b196-41479121f328
114	62f0e98c-51b1-4480-bd7b-1432a63573ab	cfsazr2e0jqN5vOrvagF3w	18	2013-07-01 03:50:49.07726	2013-07-01 03:50:49.07726	2013-08-30 03:50:48.143712	8523e0f9-c253-4a08-b269-a4ad8f8ec977
174	af63c85a-3c98-42a0-b3c3-c6e9a6ef14b5	3-V0yVYcjK_81dHK8SbrgA	24	2013-10-01 03:16:08.411681	2013-10-01 03:16:08.411681	2013-11-30 03:16:07.58042	0d7f53b1-193d-4675-9d04-c76e9193e218
119	af63c85a-3c98-42a0-b3c3-c6e9a6ef14b5	sjNMHzitF2wh5_PDEKlTQg	21	2013-07-01 04:03:38.620678	2013-07-01 04:03:38.620678	2013-08-30 04:03:38.029217	0d7f53b1-193d-4675-9d04-c76e9193e218
151	eac2d9c1-27d7-404f-b0f2-f1a8ca0cca99	hbQ46e4SXdplVmJi0L7NDg	17	2013-09-04 02:25:16.721856	2013-09-04 02:25:16.721856	2013-11-03 02:25:15.772854	7e090947-07e8-4647-aaf3-ae453eda85f8
153	a3a934e5-dd8d-4c7f-9f97-79acf1b6136d	9VFv2picvLJkWqffSQCYWw	16	2013-09-08 15:05:35.528681	2013-09-08 15:05:35.528681	2013-11-07 15:05:34.641356	72cd596f-020d-4f96-b324-70d227e00101
182	82f6cac9-f0fc-49a0-b69f-53aa471c1487	qOK_6I2qCyYbkTUG9YZlXw	23	2013-12-05 04:46:32.252924	2013-12-05 04:46:32.252924	2014-02-03 04:46:31.825883	1ecdc616-159c-452f-8ae5-9199d92fea13
183	53decb6d-d69b-4733-be69-4497dcfdf46b	Io1asFkDWNkeJtkyw44d2w	22	2013-12-09 04:40:27.477948	2013-12-09 04:40:27.477948	2014-02-07 04:40:27.066845	a2b8b606-5b0f-4155-b4ab-eb2124bb30f6
\.


--
-- Name: sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('sessions_id_seq', 183, true);


--
-- Data for Name: templates; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY templates (id, name, job_board_id, value, created_at, updated_at) FROM stdin;
4	linkedin_welcome	1	    <table border="0" cellspacing="0" cellpadding="0">\r\n        <tbody>\r\n        <tr>\r\n            <td valign="TOP" rowspan="2">\r\n                <img src="{{unbound pictureUrl}}"></td><td rowspan="2" width="10">&nbsp;\r\n            </td>\r\n            <td valign="TOP">\r\n                <span class="nav_header_new">\r\n                    <b>Welcome LinkedIn User<br></b>\r\n                    {{firstName}} {{lastName}}\r\n                </span><br>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td valign="BOTTOM">\r\n                <table border="0" cellspacing="0" cellpadding="0">\r\n                    <tbody>\r\n                    <tr>\r\n                        <td>\r\n                            <a class="forgot_link_new" href="{{unbound signOutUrl}}">Logout</a>\r\n                        </td>\r\n                    </tr>\r\n                    </tbody>\r\n                </table>\r\n            </td>\r\n        </tr>\r\n        </tbody>\r\n    </table>\r\n	2012-08-19 02:52:16.548099	2012-08-19 02:52:16.548099
11	linkedin_unlink_account	1	<a {{bindAttr href="JBC.config.linkedin.unlinkUrl"}}>Remove</a>	2013-02-26 02:31:39.112962	2013-02-26 03:06:55.156253
8	linkedin_connection	1	<tr>\r\n    <td>\r\n        {{#if relatedConnection}}\r\n        <a {{bindAttr href="relatedConnection.siteStandardProfileRequest.url"}} target="_blank">{{relatedConnection.firstName}} {{relatedConnection.lastName}}</a>\r\n        {{#if moreThanOneRelatedConnection}}\r\n        (and {{relatedCountMinusOne}} more)\r\n        {{/if}}\r\n        knows\r\n        {{/if}}\r\n        <a {{bindAttr href="connection.publicProfileUrl"}} target="_blank">{{firstName}} {{lastName}}</a> ({{headline}})\r\n    </td>\r\n</tr>	2012-09-15 20:35:16.55412	2013-06-27 04:29:40.039699
1	linkedin_signin	1	<script type="in/Login"> </script>	2012-08-19 02:51:13.74341	2013-07-28 05:37:47.973301
2	linkedin_profile_picture	1	<img data-bind="attr: {src: pictureUrl}">	2012-08-19 02:51:31.391222	2013-06-30 17:49:06.60537
7	company_connections	1	<table>\r\n    <tr data-bind="if: format() == 'full'">\r\n      <td>Connections header: <span data-bind="visible: isLoading()">loading</span></td>\r\n    </tr>\r\n    <!-- ko foreach: profiles -->\r\n    <tr data-bind="ifnot: isPrivate()">\r\n        <td>\r\n            <!-- ko if: distance() == 2 -->\r\n            <!-- ko with: relatedConnections.profiles()[0] -->\r\n            <!-- ko ifnot: isPrivate() -->\r\n                 <script type="IN/MemberProfile" data-bind="attr: {'data-id': profileUrl, 'data-text': formattedName}" data-format="click"></script> knows\r\n            <!-- /ko -->\r\n            <!-- /ko -->\r\n            <!-- /ko -->\r\n            <script type="IN/MemberProfile" data-bind="attr: {'data-id': profileUrl, 'data-text': formattedName}" data-format="click"></script> (<span data-bind="text: headline"></span>)\r\n        </td>\r\n    </tr>\r\n   <!-- /ko -->\r\n</table>	2012-09-04 03:57:33.397731	2013-09-14 23:31:08.172756
3	linkedin_profile_plugin	1	<script type="IN/MemberProfile" data-id="{{unbound publicProfileUrl}}" data-format="click"></script>	2012-08-19 02:51:47.212629	2013-07-28 18:32:04.178113
5	signin_button	1	<a href="#" data-bind="click: $parent.onClick, visible: $parent.visible"><img src="https://developer.linkedin.com/sites/default/files/signin_with_linkedin.png"/></a>	2012-08-25 06:01:39.160684	2013-06-30 19:02:47.061442
10	accounts_summary	1	<!-- ko foreach: accounts -->\r\n    <span data-bind="text: network"></span> <a data-bind="attr: {href: profile.profileUrl}, text: profile.formattedName" target="_blank"></a> <a data-bind="attr: {href: unlinkUrl}">Unlink</a>\r\n<!-- /ko -->\r\n<!-- ko ifnot: accounts -->\r\nYou don't have any social network account\r\n<!-- /ko -->	2013-02-24 21:13:44.270323	2013-07-20 03:23:46.223744
12	tmp	1	test	2013-12-11 03:37:57.201937	2013-12-11 03:37:57.201937
9	relationship	1	<div data-bind="if: format() == 'full'">\r\n    How are you connected to <span data-bind="text: formattedName"></span>\r\n</div>\r\n<div data-bind="if: isLoading()"><img src="https://d20rshmrj2od9l.cloudfront.net/ajax-loader.gif"></img></div>\r\n<!-- ko with: targetUser -->\r\n    <!-- ko with: linkedinProfile -->\r\n        <!-- ko if: isDirectConnection() -->\r\n            You and <script type="IN/MemberProfile" data-bind="attr: {'data-id': profileUrl, 'data-text': formattedName}" data-format="click"></script></a> are friend on LinkedIn\r\n        <!-- /ko -->\r\n        <!-- ko if: distance() == 2 -->\r\n            <!-- ko with: relatedConnections -->\r\n            <span data-bind="text: total"></span> connection<span data-bind="if: total() > 1">s</span> from LinkedIn\r\n            <ul data-bind="foreach: profiles">\r\n                <li><script type="IN/MemberProfile" data-bind="attr: {'data-id': profileUrl, 'data-text': formattedName}" data-format="click"></script></li>\r\n            </ul>\r\n            <!-- /ko -->\r\n        <!-- /ko -->\r\n    <!-- /ko -->\r\n<!-- /ko -->	2013-02-07 04:06:56.068762	2013-12-14 05:09:51.592138
\.


--
-- Name: templates_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('templates_id_seq', 12, true);


--
-- Data for Name: tracking_events; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY tracking_events (id, category, action, job_board_id, additional_json, count, recorded_at, created_at, updated_at) FROM stdin;
1	LinkedIn	Loaded	2	\N	5018	2012-06-30	2012-07-30 04:14:36.298078	2012-07-30 04:14:36.298078
2	LinkedIn	Authenticated	2	\N	3136	2012-06-30	2012-07-30 04:14:36.412495	2012-07-30 04:14:36.412495
3	LinkedIn	Redirect	2	\N	2654	2012-06-30	2012-07-30 04:14:36.418854	2012-07-30 04:14:36.418854
4	LinkedIn	Sign-In	2	\N	917	2012-06-30	2012-07-30 04:14:36.425193	2012-07-30 04:14:36.425193
5	LinkedIn	Sign-Out	2	\N	229	2012-06-30	2012-07-30 04:14:36.460321	2012-07-30 04:14:36.460321
6	LinkedIn	Loaded	2	\N	8212	2012-07-01	2012-07-30 04:14:36.472981	2012-07-30 04:14:36.472981
7	LinkedIn	Authenticated	2	\N	4542	2012-07-01	2012-07-30 04:14:36.479575	2012-07-30 04:14:36.479575
8	LinkedIn	Redirect	2	\N	4391	2012-07-01	2012-07-30 04:14:36.484923	2012-07-30 04:14:36.484923
9	LinkedIn	Sign-In	2	\N	345	2012-07-01	2012-07-30 04:14:36.490035	2012-07-30 04:14:36.490035
10	LinkedIn	Sign-Out	2	\N	18	2012-07-01	2012-07-30 04:14:36.495672	2012-07-30 04:14:36.495672
11	LinkedIn	Loaded	2	\N	1311	2012-07-02	2012-07-30 04:14:36.500738	2012-07-30 04:14:36.500738
12	LinkedIn	Authenticated	2	\N	963	2012-07-02	2012-07-30 04:14:36.508553	2012-07-30 04:14:36.508553
13	LinkedIn	Redirect	2	\N	267	2012-07-02	2012-07-30 04:14:36.513421	2012-07-30 04:14:36.513421
14	LinkedIn	Sign-In	2	\N	41	2012-07-02	2012-07-30 04:14:36.519035	2012-07-30 04:14:36.519035
15	LinkedIn	Sign-Out	2	\N	9	2012-07-02	2012-07-30 04:14:36.524012	2012-07-30 04:14:36.524012
16	LinkedIn	Loaded	2	\N	3266	2012-07-03	2012-07-30 04:14:36.530012	2012-07-30 04:14:36.530012
17	LinkedIn	Authenticated	2	\N	2208	2012-07-03	2012-07-30 04:14:36.544213	2012-07-30 04:14:36.544213
18	LinkedIn	Redirect	2	\N	893	2012-07-03	2012-07-30 04:14:36.570431	2012-07-30 04:14:36.570431
19	LinkedIn	Sign-In	2	\N	786	2012-07-03	2012-07-30 04:14:36.577615	2012-07-30 04:14:36.577615
20	LinkedIn	Sign-Out	2	\N	119	2012-07-03	2012-07-30 04:14:36.61398	2012-07-30 04:14:36.61398
21	LinkedIn	Loaded	2	\N	16241	2012-07-04	2012-07-30 04:14:36.625199	2012-07-30 04:14:36.625199
22	LinkedIn	Authenticated	2	\N	13984	2012-07-04	2012-07-30 04:14:36.630954	2012-07-30 04:14:36.630954
23	LinkedIn	Redirect	2	\N	1893	2012-07-04	2012-07-30 04:14:36.637268	2012-07-30 04:14:36.637268
24	LinkedIn	Sign-In	2	\N	1413	2012-07-04	2012-07-30 04:14:36.642424	2012-07-30 04:14:36.642424
25	LinkedIn	Sign-Out	2	\N	270	2012-07-04	2012-07-30 04:14:36.647603	2012-07-30 04:14:36.647603
26	LinkedIn	Loaded	2	\N	17648	2012-07-05	2012-07-30 04:14:36.653111	2012-07-30 04:14:36.653111
27	LinkedIn	Authenticated	2	\N	3242	2012-07-05	2012-07-30 04:14:36.661342	2012-07-30 04:14:36.661342
28	LinkedIn	Redirect	2	\N	865	2012-07-05	2012-07-30 04:14:36.667318	2012-07-30 04:14:36.667318
29	LinkedIn	Sign-In	2	\N	827	2012-07-05	2012-07-30 04:14:36.67252	2012-07-30 04:14:36.67252
30	LinkedIn	Sign-Out	2	\N	801	2012-07-05	2012-07-30 04:14:36.678678	2012-07-30 04:14:36.678678
31	LinkedIn	Loaded	2	\N	17768	2012-07-06	2012-07-30 04:14:36.683703	2012-07-30 04:14:36.683703
32	LinkedIn	Authenticated	2	\N	8091	2012-07-06	2012-07-30 04:14:36.696206	2012-07-30 04:14:36.696206
33	LinkedIn	Redirect	2	\N	1552	2012-07-06	2012-07-30 04:14:36.703362	2012-07-30 04:14:36.703362
34	LinkedIn	Sign-In	2	\N	936	2012-07-06	2012-07-30 04:14:36.740844	2012-07-30 04:14:36.740844
35	LinkedIn	Sign-Out	2	\N	905	2012-07-06	2012-07-30 04:14:36.747418	2012-07-30 04:14:36.747418
36	LinkedIn	Loaded	2	\N	17072	2012-07-07	2012-07-30 04:14:36.757012	2012-07-30 04:14:36.757012
37	LinkedIn	Authenticated	2	\N	12816	2012-07-07	2012-07-30 04:14:36.762948	2012-07-30 04:14:36.762948
38	LinkedIn	Redirect	2	\N	75	2012-07-07	2012-07-30 04:14:36.768225	2012-07-30 04:14:36.768225
39	LinkedIn	Sign-In	2	\N	62	2012-07-07	2012-07-30 04:14:36.773377	2012-07-30 04:14:36.773377
40	LinkedIn	Sign-Out	2	\N	49	2012-07-07	2012-07-30 04:14:36.779942	2012-07-30 04:14:36.779942
41	LinkedIn	Loaded	2	\N	4024	2012-07-08	2012-07-30 04:14:36.785208	2012-07-30 04:14:36.785208
42	LinkedIn	Authenticated	2	\N	1934	2012-07-08	2012-07-30 04:14:36.79029	2012-07-30 04:14:36.79029
43	LinkedIn	Redirect	2	\N	1771	2012-07-08	2012-07-30 04:14:36.795727	2012-07-30 04:14:36.795727
44	LinkedIn	Sign-In	2	\N	554	2012-07-08	2012-07-30 04:14:36.800998	2012-07-30 04:14:36.800998
45	LinkedIn	Sign-Out	2	\N	366	2012-07-08	2012-07-30 04:14:36.806161	2012-07-30 04:14:36.806161
46	LinkedIn	Loaded	2	\N	12359	2012-07-09	2012-07-30 04:14:36.811048	2012-07-30 04:14:36.811048
47	LinkedIn	Authenticated	2	\N	7001	2012-07-09	2012-07-30 04:14:36.816778	2012-07-30 04:14:36.816778
48	LinkedIn	Redirect	2	\N	1475	2012-07-09	2012-07-30 04:14:36.821786	2012-07-30 04:14:36.821786
49	LinkedIn	Sign-In	2	\N	1286	2012-07-09	2012-07-30 04:14:36.826724	2012-07-30 04:14:36.826724
50	LinkedIn	Sign-Out	2	\N	742	2012-07-09	2012-07-30 04:14:36.831513	2012-07-30 04:14:36.831513
51	LinkedIn	Loaded	2	\N	19695	2012-07-10	2012-07-30 04:14:36.84718	2012-07-30 04:14:36.84718
52	LinkedIn	Authenticated	2	\N	2350	2012-07-10	2012-07-30 04:14:36.856018	2012-07-30 04:14:36.856018
53	LinkedIn	Redirect	2	\N	1904	2012-07-10	2012-07-30 04:14:36.862246	2012-07-30 04:14:36.862246
54	LinkedIn	Sign-In	2	\N	1167	2012-07-10	2012-07-30 04:14:36.867696	2012-07-30 04:14:36.867696
55	LinkedIn	Sign-Out	2	\N	48	2012-07-10	2012-07-30 04:14:36.872736	2012-07-30 04:14:36.872736
56	LinkedIn	Loaded	2	\N	3166	2012-07-11	2012-07-30 04:14:36.878243	2012-07-30 04:14:36.878243
57	LinkedIn	Authenticated	2	\N	3011	2012-07-11	2012-07-30 04:14:36.883511	2012-07-30 04:14:36.883511
58	LinkedIn	Redirect	2	\N	847	2012-07-11	2012-07-30 04:14:36.889673	2012-07-30 04:14:36.889673
59	LinkedIn	Sign-In	2	\N	740	2012-07-11	2012-07-30 04:14:36.894628	2012-07-30 04:14:36.894628
60	LinkedIn	Sign-Out	2	\N	169	2012-07-11	2012-07-30 04:14:36.900726	2012-07-30 04:14:36.900726
61	LinkedIn	Loaded	2	\N	8037	2012-07-12	2012-07-30 04:14:36.905934	2012-07-30 04:14:36.905934
62	LinkedIn	Authenticated	2	\N	2019	2012-07-12	2012-07-30 04:14:36.917392	2012-07-30 04:14:36.917392
63	LinkedIn	Redirect	2	\N	1196	2012-07-12	2012-07-30 04:14:36.92719	2012-07-30 04:14:36.92719
64	LinkedIn	Sign-In	2	\N	404	2012-07-12	2012-07-30 04:14:36.932069	2012-07-30 04:14:36.932069
65	LinkedIn	Sign-Out	2	\N	226	2012-07-12	2012-07-30 04:14:36.937196	2012-07-30 04:14:36.937196
66	LinkedIn	Loaded	2	\N	7818	2012-07-13	2012-07-30 04:14:36.947641	2012-07-30 04:14:36.947641
67	LinkedIn	Authenticated	2	\N	3728	2012-07-13	2012-07-30 04:14:36.963346	2012-07-30 04:14:36.963346
68	LinkedIn	Redirect	2	\N	886	2012-07-13	2012-07-30 04:14:36.979806	2012-07-30 04:14:36.979806
69	LinkedIn	Sign-In	2	\N	160	2012-07-13	2012-07-30 04:14:36.986423	2012-07-30 04:14:36.986423
70	LinkedIn	Sign-Out	2	\N	49	2012-07-13	2012-07-30 04:14:37.021824	2012-07-30 04:14:37.021824
71	LinkedIn	Loaded	2	\N	16837	2012-07-14	2012-07-30 04:14:37.027393	2012-07-30 04:14:37.027393
72	LinkedIn	Authenticated	2	\N	12821	2012-07-14	2012-07-30 04:14:37.032487	2012-07-30 04:14:37.032487
73	LinkedIn	Redirect	2	\N	11058	2012-07-14	2012-07-30 04:14:37.037501	2012-07-30 04:14:37.037501
74	LinkedIn	Sign-In	2	\N	5249	2012-07-14	2012-07-30 04:14:37.042394	2012-07-30 04:14:37.042394
75	LinkedIn	Sign-Out	2	\N	1632	2012-07-14	2012-07-30 04:14:37.047273	2012-07-30 04:14:37.047273
76	LinkedIn	Loaded	2	\N	7450	2012-07-15	2012-07-30 04:14:37.052809	2012-07-30 04:14:37.052809
77	LinkedIn	Authenticated	2	\N	362	2012-07-15	2012-07-30 04:14:37.058195	2012-07-30 04:14:37.058195
78	LinkedIn	Redirect	2	\N	282	2012-07-15	2012-07-30 04:14:37.063459	2012-07-30 04:14:37.063459
79	LinkedIn	Sign-In	2	\N	97	2012-07-15	2012-07-30 04:14:37.068709	2012-07-30 04:14:37.068709
80	LinkedIn	Sign-Out	2	\N	21	2012-07-15	2012-07-30 04:14:37.073707	2012-07-30 04:14:37.073707
81	LinkedIn	Loaded	2	\N	14482	2012-07-16	2012-07-30 04:14:37.080351	2012-07-30 04:14:37.080351
82	LinkedIn	Authenticated	2	\N	7905	2012-07-16	2012-07-30 04:14:37.085299	2012-07-30 04:14:37.085299
83	LinkedIn	Redirect	2	\N	555	2012-07-16	2012-07-30 04:14:37.090133	2012-07-30 04:14:37.090133
84	LinkedIn	Sign-In	2	\N	132	2012-07-16	2012-07-30 04:14:37.095327	2012-07-30 04:14:37.095327
85	LinkedIn	Sign-Out	2	\N	55	2012-07-16	2012-07-30 04:14:37.10014	2012-07-30 04:14:37.10014
86	LinkedIn	Loaded	2	\N	3833	2012-07-17	2012-07-30 04:14:37.105211	2012-07-30 04:14:37.105211
87	LinkedIn	Authenticated	2	\N	1681	2012-07-17	2012-07-30 04:14:37.110051	2012-07-30 04:14:37.110051
88	LinkedIn	Redirect	2	\N	1418	2012-07-17	2012-07-30 04:14:37.115307	2012-07-30 04:14:37.115307
89	LinkedIn	Sign-In	2	\N	1374	2012-07-17	2012-07-30 04:14:37.120095	2012-07-30 04:14:37.120095
90	LinkedIn	Sign-Out	2	\N	23	2012-07-17	2012-07-30 04:14:37.125139	2012-07-30 04:14:37.125139
91	LinkedIn	Loaded	2	\N	230	2012-07-18	2012-07-30 04:14:37.129997	2012-07-30 04:14:37.129997
92	LinkedIn	Authenticated	2	\N	121	2012-07-18	2012-07-30 04:14:37.145253	2012-07-30 04:14:37.145253
93	LinkedIn	Redirect	2	\N	55	2012-07-18	2012-07-30 04:14:37.151608	2012-07-30 04:14:37.151608
94	LinkedIn	Sign-In	2	\N	25	2012-07-18	2012-07-30 04:14:37.161084	2012-07-30 04:14:37.161084
95	LinkedIn	Sign-Out	2	\N	1	2012-07-18	2012-07-30 04:14:37.167005	2012-07-30 04:14:37.167005
96	LinkedIn	Loaded	2	\N	8030	2012-07-19	2012-07-30 04:14:37.172288	2012-07-30 04:14:37.172288
97	LinkedIn	Authenticated	2	\N	603	2012-07-19	2012-07-30 04:14:37.178289	2012-07-30 04:14:37.178289
98	LinkedIn	Redirect	2	\N	244	2012-07-19	2012-07-30 04:14:37.183826	2012-07-30 04:14:37.183826
99	LinkedIn	Sign-In	2	\N	206	2012-07-19	2012-07-30 04:14:37.189757	2012-07-30 04:14:37.189757
100	LinkedIn	Sign-Out	2	\N	13	2012-07-19	2012-07-30 04:14:37.196066	2012-07-30 04:14:37.196066
101	LinkedIn	Loaded	2	\N	4451	2012-07-20	2012-07-30 04:14:37.201991	2012-07-30 04:14:37.201991
102	LinkedIn	Authenticated	2	\N	4209	2012-07-20	2012-07-30 04:14:37.208184	2012-07-30 04:14:37.208184
103	LinkedIn	Redirect	2	\N	2609	2012-07-20	2012-07-30 04:14:37.213987	2012-07-30 04:14:37.213987
104	LinkedIn	Sign-In	2	\N	842	2012-07-20	2012-07-30 04:14:37.220056	2012-07-30 04:14:37.220056
105	LinkedIn	Sign-Out	2	\N	49	2012-07-20	2012-07-30 04:14:37.225547	2012-07-30 04:14:37.225547
106	LinkedIn	Loaded	2	\N	11392	2012-07-21	2012-07-30 04:14:37.230624	2012-07-30 04:14:37.230624
107	LinkedIn	Authenticated	2	\N	6001	2012-07-21	2012-07-30 04:14:37.235668	2012-07-30 04:14:37.235668
108	LinkedIn	Redirect	2	\N	3419	2012-07-21	2012-07-30 04:14:37.250547	2012-07-30 04:14:37.250547
109	LinkedIn	Sign-In	2	\N	1103	2012-07-21	2012-07-30 04:14:37.255694	2012-07-30 04:14:37.255694
110	LinkedIn	Sign-Out	2	\N	642	2012-07-21	2012-07-30 04:14:37.262088	2012-07-30 04:14:37.262088
111	LinkedIn	Loaded	2	\N	19134	2012-07-22	2012-07-30 04:14:37.268457	2012-07-30 04:14:37.268457
112	LinkedIn	Authenticated	2	\N	12084	2012-07-22	2012-07-30 04:14:37.273417	2012-07-30 04:14:37.273417
113	LinkedIn	Redirect	2	\N	6071	2012-07-22	2012-07-30 04:14:37.280032	2012-07-30 04:14:37.280032
114	LinkedIn	Sign-In	2	\N	486	2012-07-22	2012-07-30 04:14:37.285181	2012-07-30 04:14:37.285181
115	LinkedIn	Sign-Out	2	\N	213	2012-07-22	2012-07-30 04:14:37.304942	2012-07-30 04:14:37.304942
116	LinkedIn	Loaded	2	\N	19418	2012-07-23	2012-07-30 04:14:37.310377	2012-07-30 04:14:37.310377
117	LinkedIn	Authenticated	2	\N	12250	2012-07-23	2012-07-30 04:14:37.318377	2012-07-30 04:14:37.318377
118	LinkedIn	Redirect	2	\N	935	2012-07-23	2012-07-30 04:14:37.333062	2012-07-30 04:14:37.333062
119	LinkedIn	Sign-In	2	\N	406	2012-07-23	2012-07-30 04:14:37.340175	2012-07-30 04:14:37.340175
120	LinkedIn	Sign-Out	2	\N	280	2012-07-23	2012-07-30 04:14:37.34575	2012-07-30 04:14:37.34575
121	LinkedIn	Loaded	2	\N	6859	2012-07-24	2012-07-30 04:14:37.351675	2012-07-30 04:14:37.351675
122	LinkedIn	Authenticated	2	\N	181	2012-07-24	2012-07-30 04:14:37.762099	2012-07-30 04:14:37.762099
123	LinkedIn	Redirect	2	\N	87	2012-07-24	2012-07-30 04:14:37.768405	2012-07-30 04:14:37.768405
124	LinkedIn	Sign-In	2	\N	86	2012-07-24	2012-07-30 04:14:37.78692	2012-07-30 04:14:37.78692
125	LinkedIn	Sign-Out	2	\N	34	2012-07-24	2012-07-30 04:14:37.79276	2012-07-30 04:14:37.79276
126	LinkedIn	Loaded	2	\N	13487	2012-07-25	2012-07-30 04:14:37.798511	2012-07-30 04:14:37.798511
127	LinkedIn	Authenticated	2	\N	2761	2012-07-25	2012-07-30 04:14:37.804365	2012-07-30 04:14:37.804365
128	LinkedIn	Redirect	2	\N	296	2012-07-25	2012-07-30 04:14:37.821844	2012-07-30 04:14:37.821844
129	LinkedIn	Sign-In	2	\N	271	2012-07-25	2012-07-30 04:14:37.827856	2012-07-30 04:14:37.827856
130	LinkedIn	Sign-Out	2	\N	59	2012-07-25	2012-07-30 04:14:37.833286	2012-07-30 04:14:37.833286
131	LinkedIn	Loaded	2	\N	10609	2012-07-26	2012-07-30 04:14:37.839391	2012-07-30 04:14:37.839391
132	LinkedIn	Authenticated	2	\N	1513	2012-07-26	2012-07-30 04:14:37.844648	2012-07-30 04:14:37.844648
133	LinkedIn	Redirect	2	\N	248	2012-07-26	2012-07-30 04:14:37.863892	2012-07-30 04:14:37.863892
134	LinkedIn	Sign-In	2	\N	221	2012-07-26	2012-07-30 04:14:37.886612	2012-07-30 04:14:37.886612
135	LinkedIn	Sign-Out	2	\N	89	2012-07-26	2012-07-30 04:14:37.894272	2012-07-30 04:14:37.894272
136	LinkedIn	Loaded	2	\N	15540	2012-07-27	2012-07-30 04:14:37.899866	2012-07-30 04:14:37.899866
137	LinkedIn	Authenticated	2	\N	11046	2012-07-27	2012-07-30 04:14:37.906698	2012-07-30 04:14:37.906698
138	LinkedIn	Redirect	2	\N	11013	2012-07-27	2012-07-30 04:14:37.912115	2012-07-30 04:14:37.912115
139	LinkedIn	Sign-In	2	\N	586	2012-07-27	2012-07-30 04:14:37.917493	2012-07-30 04:14:37.917493
140	LinkedIn	Sign-Out	2	\N	428	2012-07-27	2012-07-30 04:14:37.922706	2012-07-30 04:14:37.922706
141	LinkedIn	Loaded	2	\N	13992	2012-07-28	2012-07-30 04:14:37.927986	2012-07-30 04:14:37.927986
142	LinkedIn	Authenticated	2	\N	1729	2012-07-28	2012-07-30 04:14:37.933209	2012-07-30 04:14:37.933209
143	LinkedIn	Redirect	2	\N	1090	2012-07-28	2012-07-30 04:14:37.939154	2012-07-30 04:14:37.939154
144	LinkedIn	Sign-In	2	\N	681	2012-07-28	2012-07-30 04:14:37.945063	2012-07-30 04:14:37.945063
145	LinkedIn	Sign-Out	2	\N	176	2012-07-28	2012-07-30 04:14:37.950886	2012-07-30 04:14:37.950886
146	LinkedIn	Loaded	2	\N	3424	2012-07-29	2012-07-30 04:14:37.957192	2012-07-30 04:14:37.957192
147	LinkedIn	Authenticated	2	\N	3309	2012-07-29	2012-07-30 04:14:37.965751	2012-07-30 04:14:37.965751
148	LinkedIn	Redirect	2	\N	2951	2012-07-29	2012-07-30 04:14:37.974519	2012-07-30 04:14:37.974519
149	LinkedIn	Sign-In	2	\N	1702	2012-07-29	2012-07-30 04:14:37.985346	2012-07-30 04:14:37.985346
150	LinkedIn	Sign-Out	2	\N	1188	2012-07-29	2012-07-30 04:14:37.991294	2012-07-30 04:14:37.991294
151	LinkedIn	Loaded	2	\N	11795	2012-07-30	2012-07-30 04:14:37.996999	2012-07-30 04:14:37.996999
152	LinkedIn	Authenticated	2	\N	193	2012-07-30	2012-07-30 04:14:38.002129	2012-07-30 04:14:38.002129
153	LinkedIn	Redirect	2	\N	16	2012-07-30	2012-07-30 04:14:38.007365	2012-07-30 04:14:38.007365
154	LinkedIn	Sign-In	2	\N	13	2012-07-30	2012-07-30 04:14:38.012743	2012-07-30 04:14:38.012743
155	LinkedIn	Sign-Out	2	\N	2	2012-07-30	2012-07-30 04:14:38.019955	2012-07-30 04:14:38.019955
156	LinkedIn	Loaded	1	\N	12917	2012-06-30	2012-07-30 04:14:38.032294	2012-07-30 04:14:38.032294
157	LinkedIn	Authenticated	1	\N	885	2012-06-30	2012-07-30 04:14:38.040932	2012-07-30 04:14:38.040932
158	LinkedIn	Redirect	1	\N	108	2012-06-30	2012-07-30 04:14:38.046271	2012-07-30 04:14:38.046271
159	LinkedIn	Sign-In	1	\N	15	2012-06-30	2012-07-30 04:14:38.055993	2012-07-30 04:14:38.055993
160	LinkedIn	Sign-Out	1	\N	1	2012-06-30	2012-07-30 04:14:38.066942	2012-07-30 04:14:38.066942
161	LinkedIn	Loaded	1	\N	6814	2012-07-01	2012-07-30 04:14:38.076963	2012-07-30 04:14:38.076963
162	LinkedIn	Authenticated	1	\N	1903	2012-07-01	2012-07-30 04:14:38.083964	2012-07-30 04:14:38.083964
163	LinkedIn	Redirect	1	\N	1844	2012-07-01	2012-07-30 04:14:38.089805	2012-07-30 04:14:38.089805
164	LinkedIn	Sign-In	1	\N	1537	2012-07-01	2012-07-30 04:14:38.164756	2012-07-30 04:14:38.164756
165	LinkedIn	Sign-Out	1	\N	872	2012-07-01	2012-07-30 04:14:38.173838	2012-07-30 04:14:38.173838
166	LinkedIn	Loaded	1	\N	10568	2012-07-02	2012-07-30 04:14:38.17936	2012-07-30 04:14:38.17936
167	LinkedIn	Authenticated	1	\N	2448	2012-07-02	2012-07-30 04:14:38.188756	2012-07-30 04:14:38.188756
168	LinkedIn	Redirect	1	\N	2281	2012-07-02	2012-07-30 04:14:38.195551	2012-07-30 04:14:38.195551
169	LinkedIn	Sign-In	1	\N	889	2012-07-02	2012-07-30 04:14:38.201011	2012-07-30 04:14:38.201011
170	LinkedIn	Sign-Out	1	\N	368	2012-07-02	2012-07-30 04:14:38.217961	2012-07-30 04:14:38.217961
171	LinkedIn	Loaded	1	\N	13476	2012-07-03	2012-07-30 04:14:38.227126	2012-07-30 04:14:38.227126
172	LinkedIn	Authenticated	1	\N	12564	2012-07-03	2012-07-30 04:14:38.233025	2012-07-30 04:14:38.233025
173	LinkedIn	Redirect	1	\N	2764	2012-07-03	2012-07-30 04:14:38.241369	2012-07-30 04:14:38.241369
174	LinkedIn	Sign-In	1	\N	2277	2012-07-03	2012-07-30 04:14:38.254502	2012-07-30 04:14:38.254502
175	LinkedIn	Sign-Out	1	\N	1797	2012-07-03	2012-07-30 04:14:38.26061	2012-07-30 04:14:38.26061
176	LinkedIn	Loaded	1	\N	6598	2012-07-04	2012-07-30 04:14:38.266451	2012-07-30 04:14:38.266451
177	LinkedIn	Authenticated	1	\N	1655	2012-07-04	2012-07-30 04:14:38.271897	2012-07-30 04:14:38.271897
178	LinkedIn	Redirect	1	\N	430	2012-07-04	2012-07-30 04:14:38.278187	2012-07-30 04:14:38.278187
179	LinkedIn	Sign-In	1	\N	356	2012-07-04	2012-07-30 04:14:38.284195	2012-07-30 04:14:38.284195
180	LinkedIn	Sign-Out	1	\N	70	2012-07-04	2012-07-30 04:14:38.289701	2012-07-30 04:14:38.289701
181	LinkedIn	Loaded	1	\N	8896	2012-07-05	2012-07-30 04:14:38.294607	2012-07-30 04:14:38.294607
182	LinkedIn	Authenticated	1	\N	6329	2012-07-05	2012-07-30 04:14:38.300122	2012-07-30 04:14:38.300122
183	LinkedIn	Redirect	1	\N	1261	2012-07-05	2012-07-30 04:14:38.305188	2012-07-30 04:14:38.305188
184	LinkedIn	Sign-In	1	\N	260	2012-07-05	2012-07-30 04:14:38.309998	2012-07-30 04:14:38.309998
185	LinkedIn	Sign-Out	1	\N	56	2012-07-05	2012-07-30 04:14:38.315332	2012-07-30 04:14:38.315332
186	LinkedIn	Loaded	1	\N	299	2012-07-06	2012-07-30 04:14:38.320376	2012-07-30 04:14:38.320376
187	LinkedIn	Authenticated	1	\N	197	2012-07-06	2012-07-30 04:14:38.325511	2012-07-30 04:14:38.325511
188	LinkedIn	Redirect	1	\N	176	2012-07-06	2012-07-30 04:14:38.330357	2012-07-30 04:14:38.330357
189	LinkedIn	Sign-In	1	\N	118	2012-07-06	2012-07-30 04:14:38.335854	2012-07-30 04:14:38.335854
190	LinkedIn	Sign-Out	1	\N	51	2012-07-06	2012-07-30 04:14:38.340662	2012-07-30 04:14:38.340662
191	LinkedIn	Loaded	1	\N	12476	2012-07-07	2012-07-30 04:14:38.345858	2012-07-30 04:14:38.345858
192	LinkedIn	Authenticated	1	\N	4890	2012-07-07	2012-07-30 04:14:38.350677	2012-07-30 04:14:38.350677
193	LinkedIn	Redirect	1	\N	4568	2012-07-07	2012-07-30 04:14:38.35563	2012-07-30 04:14:38.35563
194	LinkedIn	Sign-In	1	\N	3646	2012-07-07	2012-07-30 04:14:38.361102	2012-07-30 04:14:38.361102
195	LinkedIn	Sign-Out	1	\N	688	2012-07-07	2012-07-30 04:14:38.366934	2012-07-30 04:14:38.366934
196	LinkedIn	Loaded	1	\N	1869	2012-07-08	2012-07-30 04:14:38.375288	2012-07-30 04:14:38.375288
197	LinkedIn	Authenticated	1	\N	422	2012-07-08	2012-07-30 04:14:38.380397	2012-07-30 04:14:38.380397
198	LinkedIn	Redirect	1	\N	238	2012-07-08	2012-07-30 04:14:38.385529	2012-07-30 04:14:38.385529
199	LinkedIn	Sign-In	1	\N	14	2012-07-08	2012-07-30 04:14:38.390456	2012-07-30 04:14:38.390456
200	LinkedIn	Sign-Out	1	\N	9	2012-07-08	2012-07-30 04:14:38.401561	2012-07-30 04:14:38.401561
201	LinkedIn	Loaded	1	\N	14251	2012-07-09	2012-07-30 04:14:38.406529	2012-07-30 04:14:38.406529
202	LinkedIn	Authenticated	1	\N	1434	2012-07-09	2012-07-30 04:14:38.411647	2012-07-30 04:14:38.411647
203	LinkedIn	Redirect	1	\N	1179	2012-07-09	2012-07-30 04:14:38.417225	2012-07-30 04:14:38.417225
204	LinkedIn	Sign-In	1	\N	884	2012-07-09	2012-07-30 04:14:38.422152	2012-07-30 04:14:38.422152
205	LinkedIn	Sign-Out	1	\N	704	2012-07-09	2012-07-30 04:14:38.426987	2012-07-30 04:14:38.426987
206	LinkedIn	Loaded	1	\N	17566	2012-07-10	2012-07-30 04:14:38.431785	2012-07-30 04:14:38.431785
207	LinkedIn	Authenticated	1	\N	16784	2012-07-10	2012-07-30 04:14:38.436684	2012-07-30 04:14:38.436684
208	LinkedIn	Redirect	1	\N	4074	2012-07-10	2012-07-30 04:14:38.441474	2012-07-30 04:14:38.441474
209	LinkedIn	Sign-In	1	\N	3103	2012-07-10	2012-07-30 04:14:38.446458	2012-07-30 04:14:38.446458
210	LinkedIn	Sign-Out	1	\N	875	2012-07-10	2012-07-30 04:14:38.451232	2012-07-30 04:14:38.451232
211	LinkedIn	Loaded	1	\N	19395	2012-07-11	2012-07-30 04:14:38.456517	2012-07-30 04:14:38.456517
212	LinkedIn	Authenticated	1	\N	15142	2012-07-11	2012-07-30 04:14:38.461298	2012-07-30 04:14:38.461298
213	LinkedIn	Redirect	1	\N	11293	2012-07-11	2012-07-30 04:14:38.466393	2012-07-30 04:14:38.466393
214	LinkedIn	Sign-In	1	\N	6305	2012-07-11	2012-07-30 04:14:38.471295	2012-07-30 04:14:38.471295
215	LinkedIn	Sign-Out	1	\N	1772	2012-07-11	2012-07-30 04:14:38.476356	2012-07-30 04:14:38.476356
216	LinkedIn	Loaded	1	\N	2024	2012-07-12	2012-07-30 04:14:38.481159	2012-07-30 04:14:38.481159
217	LinkedIn	Authenticated	1	\N	1303	2012-07-12	2012-07-30 04:14:38.486062	2012-07-30 04:14:38.486062
218	LinkedIn	Redirect	1	\N	1198	2012-07-12	2012-07-30 04:14:38.490855	2012-07-30 04:14:38.490855
219	LinkedIn	Sign-In	1	\N	351	2012-07-12	2012-07-30 04:14:38.495631	2012-07-30 04:14:38.495631
220	LinkedIn	Sign-Out	1	\N	174	2012-07-12	2012-07-30 04:14:38.500468	2012-07-30 04:14:38.500468
221	LinkedIn	Loaded	1	\N	13049	2012-07-13	2012-07-30 04:14:38.505729	2012-07-30 04:14:38.505729
222	LinkedIn	Authenticated	1	\N	4850	2012-07-13	2012-07-30 04:14:38.521044	2012-07-30 04:14:38.521044
223	LinkedIn	Redirect	1	\N	1367	2012-07-13	2012-07-30 04:14:38.526781	2012-07-30 04:14:38.526781
224	LinkedIn	Sign-In	1	\N	370	2012-07-13	2012-07-30 04:14:38.536607	2012-07-30 04:14:38.536607
225	LinkedIn	Sign-Out	1	\N	233	2012-07-13	2012-07-30 04:14:38.542297	2012-07-30 04:14:38.542297
226	LinkedIn	Loaded	1	\N	8950	2012-07-14	2012-07-30 04:14:38.547502	2012-07-30 04:14:38.547502
227	LinkedIn	Authenticated	1	\N	1235	2012-07-14	2012-07-30 04:14:38.552338	2012-07-30 04:14:38.552338
228	LinkedIn	Redirect	1	\N	818	2012-07-14	2012-07-30 04:14:38.557189	2012-07-30 04:14:38.557189
229	LinkedIn	Sign-In	1	\N	268	2012-07-14	2012-07-30 04:14:38.5625	2012-07-30 04:14:38.5625
230	LinkedIn	Sign-Out	1	\N	215	2012-07-14	2012-07-30 04:14:38.567444	2012-07-30 04:14:38.567444
231	LinkedIn	Loaded	1	\N	8513	2012-07-15	2012-07-30 04:14:38.572844	2012-07-30 04:14:38.572844
232	LinkedIn	Authenticated	1	\N	517	2012-07-15	2012-07-30 04:14:38.577682	2012-07-30 04:14:38.577682
233	LinkedIn	Redirect	1	\N	104	2012-07-15	2012-07-30 04:14:38.58252	2012-07-30 04:14:38.58252
234	LinkedIn	Sign-In	1	\N	15	2012-07-15	2012-07-30 04:14:38.587309	2012-07-30 04:14:38.587309
235	LinkedIn	Sign-Out	1	\N	4	2012-07-15	2012-07-30 04:14:38.592136	2012-07-30 04:14:38.592136
236	LinkedIn	Loaded	1	\N	11294	2012-07-16	2012-07-30 04:14:38.597268	2012-07-30 04:14:38.597268
237	LinkedIn	Authenticated	1	\N	1067	2012-07-16	2012-07-30 04:14:38.602099	2012-07-30 04:14:38.602099
238	LinkedIn	Redirect	1	\N	813	2012-07-16	2012-07-30 04:14:38.607002	2012-07-30 04:14:38.607002
239	LinkedIn	Sign-In	1	\N	771	2012-07-16	2012-07-30 04:14:38.612145	2012-07-30 04:14:38.612145
240	LinkedIn	Sign-Out	1	\N	458	2012-07-16	2012-07-30 04:14:38.617162	2012-07-30 04:14:38.617162
241	LinkedIn	Loaded	1	\N	7006	2012-07-17	2012-07-30 04:14:38.621968	2012-07-30 04:14:38.621968
242	LinkedIn	Authenticated	1	\N	292	2012-07-17	2012-07-30 04:14:38.628038	2012-07-30 04:14:38.628038
243	LinkedIn	Redirect	1	\N	112	2012-07-17	2012-07-30 04:14:38.632914	2012-07-30 04:14:38.632914
244	LinkedIn	Sign-In	1	\N	54	2012-07-17	2012-07-30 04:14:38.637736	2012-07-30 04:14:38.637736
245	LinkedIn	Sign-Out	1	\N	35	2012-07-17	2012-07-30 04:14:38.643333	2012-07-30 04:14:38.643333
246	LinkedIn	Loaded	1	\N	4093	2012-07-18	2012-07-30 04:14:38.648435	2012-07-30 04:14:38.648435
247	LinkedIn	Authenticated	1	\N	3692	2012-07-18	2012-07-30 04:14:38.654368	2012-07-30 04:14:38.654368
248	LinkedIn	Redirect	1	\N	3085	2012-07-18	2012-07-30 04:14:38.660263	2012-07-30 04:14:38.660263
249	LinkedIn	Sign-In	1	\N	2258	2012-07-18	2012-07-30 04:14:38.677214	2012-07-30 04:14:38.677214
250	LinkedIn	Sign-Out	1	\N	2065	2012-07-18	2012-07-30 04:14:38.695502	2012-07-30 04:14:38.695502
251	LinkedIn	Loaded	1	\N	12571	2012-07-19	2012-07-30 04:14:38.701119	2012-07-30 04:14:38.701119
252	LinkedIn	Authenticated	1	\N	10670	2012-07-19	2012-07-30 04:14:38.706408	2012-07-30 04:14:38.706408
253	LinkedIn	Redirect	1	\N	8867	2012-07-19	2012-07-30 04:14:38.712023	2012-07-30 04:14:38.712023
254	LinkedIn	Sign-In	1	\N	5940	2012-07-19	2012-07-30 04:14:38.73539	2012-07-30 04:14:38.73539
255	LinkedIn	Sign-Out	1	\N	4316	2012-07-19	2012-07-30 04:14:38.740511	2012-07-30 04:14:38.740511
256	LinkedIn	Loaded	1	\N	3348	2012-07-20	2012-07-30 04:14:38.745803	2012-07-30 04:14:38.745803
257	LinkedIn	Authenticated	1	\N	2615	2012-07-20	2012-07-30 04:14:38.750656	2012-07-30 04:14:38.750656
258	LinkedIn	Redirect	1	\N	1488	2012-07-20	2012-07-30 04:14:38.75549	2012-07-30 04:14:38.75549
259	LinkedIn	Sign-In	1	\N	1026	2012-07-20	2012-07-30 04:14:38.761858	2012-07-30 04:14:38.761858
260	LinkedIn	Sign-Out	1	\N	601	2012-07-20	2012-07-30 04:14:38.768483	2012-07-30 04:14:38.768483
261	LinkedIn	Loaded	1	\N	1182	2012-07-21	2012-07-30 04:14:38.774044	2012-07-30 04:14:38.774044
262	LinkedIn	Authenticated	1	\N	1105	2012-07-21	2012-07-30 04:14:38.779554	2012-07-30 04:14:38.779554
263	LinkedIn	Redirect	1	\N	812	2012-07-21	2012-07-30 04:14:38.784352	2012-07-30 04:14:38.784352
264	LinkedIn	Sign-In	1	\N	489	2012-07-21	2012-07-30 04:14:38.791831	2012-07-30 04:14:38.791831
265	LinkedIn	Sign-Out	1	\N	303	2012-07-21	2012-07-30 04:14:38.797768	2012-07-30 04:14:38.797768
266	LinkedIn	Loaded	1	\N	5280	2012-07-22	2012-07-30 04:14:38.803224	2012-07-30 04:14:38.803224
267	LinkedIn	Authenticated	1	\N	5048	2012-07-22	2012-07-30 04:14:38.808923	2012-07-30 04:14:38.808923
268	LinkedIn	Redirect	1	\N	266	2012-07-22	2012-07-30 04:14:38.814256	2012-07-30 04:14:38.814256
269	LinkedIn	Sign-In	1	\N	146	2012-07-22	2012-07-30 04:14:38.820173	2012-07-30 04:14:38.820173
270	LinkedIn	Sign-Out	1	\N	127	2012-07-22	2012-07-30 04:14:38.826944	2012-07-30 04:14:38.826944
271	LinkedIn	Loaded	1	\N	16779	2012-07-23	2012-07-30 04:14:38.850053	2012-07-30 04:14:38.850053
272	LinkedIn	Authenticated	1	\N	12701	2012-07-23	2012-07-30 04:14:38.861769	2012-07-30 04:14:38.861769
273	LinkedIn	Redirect	1	\N	8660	2012-07-23	2012-07-30 04:14:38.867058	2012-07-30 04:14:38.867058
274	LinkedIn	Sign-In	1	\N	1028	2012-07-23	2012-07-30 04:14:38.87225	2012-07-30 04:14:38.87225
275	LinkedIn	Sign-Out	1	\N	1019	2012-07-23	2012-07-30 04:14:38.884085	2012-07-30 04:14:38.884085
276	LinkedIn	Loaded	1	\N	15646	2012-07-24	2012-07-30 04:14:38.889535	2012-07-30 04:14:38.889535
277	LinkedIn	Authenticated	1	\N	7748	2012-07-24	2012-07-30 04:14:38.894551	2012-07-30 04:14:38.894551
278	LinkedIn	Redirect	1	\N	5212	2012-07-24	2012-07-30 04:14:38.900226	2012-07-30 04:14:38.900226
279	LinkedIn	Sign-In	1	\N	1993	2012-07-24	2012-07-30 04:14:38.90532	2012-07-30 04:14:38.90532
280	LinkedIn	Sign-Out	1	\N	1829	2012-07-24	2012-07-30 04:14:38.910724	2012-07-30 04:14:38.910724
281	LinkedIn	Loaded	1	\N	698	2012-07-25	2012-07-30 04:14:38.915734	2012-07-30 04:14:38.915734
282	LinkedIn	Authenticated	1	\N	378	2012-07-25	2012-07-30 04:14:38.921374	2012-07-30 04:14:38.921374
283	LinkedIn	Redirect	1	\N	57	2012-07-25	2012-07-30 04:14:38.92718	2012-07-30 04:14:38.92718
284	LinkedIn	Sign-In	1	\N	55	2012-07-25	2012-07-30 04:14:38.932696	2012-07-30 04:14:38.932696
285	LinkedIn	Sign-Out	1	\N	17	2012-07-25	2012-07-30 04:14:38.938106	2012-07-30 04:14:38.938106
286	LinkedIn	Loaded	1	\N	11925	2012-07-26	2012-07-30 04:14:38.94347	2012-07-30 04:14:38.94347
287	LinkedIn	Authenticated	1	\N	7174	2012-07-26	2012-07-30 04:14:38.948769	2012-07-30 04:14:38.948769
288	LinkedIn	Redirect	1	\N	4058	2012-07-26	2012-07-30 04:14:38.954199	2012-07-30 04:14:38.954199
289	LinkedIn	Sign-In	1	\N	1579	2012-07-26	2012-07-30 04:14:38.959358	2012-07-30 04:14:38.959358
290	LinkedIn	Sign-Out	1	\N	366	2012-07-26	2012-07-30 04:14:38.964452	2012-07-30 04:14:38.964452
291	LinkedIn	Loaded	1	\N	8510	2012-07-27	2012-07-30 04:14:38.969734	2012-07-30 04:14:38.969734
292	LinkedIn	Authenticated	1	\N	4932	2012-07-27	2012-07-30 04:14:38.975015	2012-07-30 04:14:38.975015
293	LinkedIn	Redirect	1	\N	3608	2012-07-27	2012-07-30 04:14:38.980183	2012-07-30 04:14:38.980183
294	LinkedIn	Sign-In	1	\N	3221	2012-07-27	2012-07-30 04:14:38.996629	2012-07-30 04:14:38.996629
295	LinkedIn	Sign-Out	1	\N	1112	2012-07-27	2012-07-30 04:14:39.019813	2012-07-30 04:14:39.019813
296	LinkedIn	Loaded	1	\N	14741	2012-07-28	2012-07-30 04:14:39.025531	2012-07-30 04:14:39.025531
297	LinkedIn	Authenticated	1	\N	477	2012-07-28	2012-07-30 04:14:39.030589	2012-07-30 04:14:39.030589
298	LinkedIn	Redirect	1	\N	288	2012-07-28	2012-07-30 04:14:39.03982	2012-07-30 04:14:39.03982
299	LinkedIn	Sign-In	1	\N	45	2012-07-28	2012-07-30 04:14:39.047417	2012-07-30 04:14:39.047417
300	LinkedIn	Sign-Out	1	\N	34	2012-07-28	2012-07-30 04:14:39.052595	2012-07-30 04:14:39.052595
301	LinkedIn	Loaded	1	\N	18685	2012-07-29	2012-07-30 04:14:39.057537	2012-07-30 04:14:39.057537
302	LinkedIn	Authenticated	1	\N	12757	2012-07-29	2012-07-30 04:14:39.062399	2012-07-30 04:14:39.062399
303	LinkedIn	Redirect	1	\N	11481	2012-07-29	2012-07-30 04:14:39.06728	2012-07-30 04:14:39.06728
304	LinkedIn	Sign-In	1	\N	5165	2012-07-29	2012-07-30 04:14:39.072152	2012-07-30 04:14:39.072152
305	LinkedIn	Sign-Out	1	\N	3027	2012-07-29	2012-07-30 04:14:39.077147	2012-07-30 04:14:39.077147
306	LinkedIn	Loaded	1	\N	8130	2012-07-30	2012-07-30 04:14:39.081984	2012-07-30 04:14:39.081984
307	LinkedIn	Authenticated	1	\N	573	2012-07-30	2012-07-30 04:14:39.087269	2012-07-30 04:14:39.087269
308	LinkedIn	Redirect	1	\N	295	2012-07-30	2012-07-30 04:14:39.093801	2012-07-30 04:14:39.093801
309	LinkedIn	Sign-In	1	\N	173	2012-07-30	2012-07-30 04:14:39.099	2012-07-30 04:14:39.099
310	LinkedIn	Sign-Out	1	\N	79	2012-07-30	2012-07-30 04:14:39.103822	2012-07-30 04:14:39.103822
311	LinkedIn	Authenticated	1	\N	13	2012-08-04	2012-08-05 05:31:16.453265	2012-08-05 05:31:16.453265
312	LinkedIn	Loaded	1	\N	16	2012-08-04	2012-08-05 05:31:16.63526	2012-08-05 05:31:16.63526
313	LinkedIn	Authenticated	1	\N	6	2012-08-05	2012-08-06 05:31:16.5688	2012-08-06 05:31:16.5688
314	LinkedIn	Loaded	1	\N	7	2012-08-05	2012-08-06 05:31:16.808427	2012-08-06 05:31:16.808427
315	LinkedIn	Redirect	1	\N	1	2012-08-05	2012-08-06 05:31:16.831036	2012-08-06 05:31:16.831036
316	LinkedIn	Sign-In	1	\N	1	2012-08-05	2012-08-06 05:31:16.850178	2012-08-06 05:31:16.850178
317	LinkedIn	Authenticated	1	\N	25	2012-08-07	2012-08-08 05:31:17.594969	2012-08-08 05:31:17.594969
318	LinkedIn	Loaded	1	\N	27	2012-08-07	2012-08-08 05:31:19.19859	2012-08-08 05:31:19.19859
319	LinkedIn	Redirect	1	\N	2	2012-08-07	2012-08-08 05:31:19.267412	2012-08-08 05:31:19.267412
320	LinkedIn	Sign-In	1	\N	4	2012-08-07	2012-08-08 05:31:19.28272	2012-08-08 05:31:19.28272
321	LinkedIn	Sign-Out	1	\N	2	2012-08-07	2012-08-08 05:31:19.292447	2012-08-08 05:31:19.292447
322	LinkedIn	Authenticated	1	\N	8	2012-08-08	2012-08-09 05:31:14.647331	2012-08-09 05:31:14.647331
323	LinkedIn	Loaded	1	\N	15	2012-08-08	2012-08-09 05:31:14.824606	2012-08-09 05:31:14.824606
324	LinkedIn	Redirect	1	\N	1	2012-08-08	2012-08-09 05:31:14.844308	2012-08-09 05:31:14.844308
325	LinkedIn	Sign-In	1	\N	7	2012-08-08	2012-08-09 05:31:14.859387	2012-08-09 05:31:14.859387
326	LinkedIn	Authenticated	1	\N	34	2012-08-11	2012-08-12 05:31:17.164464	2012-08-12 05:31:17.164464
327	LinkedIn	Loaded	1	\N	42	2012-08-11	2012-08-12 05:31:17.363859	2012-08-12 05:31:17.363859
328	LinkedIn	Redirect	1	\N	8	2012-08-11	2012-08-12 05:31:17.37701	2012-08-12 05:31:17.37701
329	LinkedIn	Sign-In	1	\N	6	2012-08-11	2012-08-12 05:31:17.392093	2012-08-12 05:31:17.392093
330	LinkedIn	Authenticated	1	\N	5	2012-08-12	2012-08-13 05:31:45.656323	2012-08-13 05:31:45.656323
331	LinkedIn	Loaded	1	\N	7	2012-08-12	2012-08-13 05:31:45.833412	2012-08-13 05:31:45.833412
332	LinkedIn	Redirect	1	\N	2	2012-08-12	2012-08-13 05:31:45.85982	2012-08-13 05:31:45.85982
333	LinkedIn	Sign-In	1	\N	1	2012-08-12	2012-08-13 05:31:45.880107	2012-08-13 05:31:45.880107
334	LinkedIn	Authenticated	1	\N	14	2012-08-15	2012-08-16 05:31:17.663544	2012-08-16 05:31:17.663544
335	LinkedIn	Loaded	1	\N	17	2012-08-15	2012-08-16 05:31:17.878479	2012-08-16 05:31:17.878479
336	LinkedIn	Redirect	1	\N	7	2012-08-15	2012-08-16 05:31:17.919397	2012-08-16 05:31:17.919397
337	LinkedIn	Authenticated	1	\N	6	2012-08-16	2012-08-17 05:31:13.201272	2012-08-17 05:31:13.201272
338	LinkedIn	Loaded	1	\N	7	2012-08-16	2012-08-17 05:31:13.324564	2012-08-17 05:31:13.324564
339	LinkedIn	Redirect	1	\N	2	2012-08-16	2012-08-17 05:31:13.357036	2012-08-17 05:31:13.357036
340	LinkedIn	Sign-In	1	\N	1	2012-08-16	2012-08-17 05:31:13.439751	2012-08-17 05:31:13.439751
341	LinkedIn	Authenticated	1	\N	7	2012-08-17	2012-08-18 05:31:19.241768	2012-08-18 05:31:19.241768
342	LinkedIn	Loaded	1	\N	10	2012-08-17	2012-08-18 05:31:19.56373	2012-08-18 05:31:19.56373
343	LinkedIn	Redirect	1	\N	4	2012-08-17	2012-08-18 05:31:19.578896	2012-08-18 05:31:19.578896
344	LinkedIn	Sign-In	1	\N	2	2012-08-17	2012-08-18 05:31:19.593443	2012-08-18 05:31:19.593443
345	LinkedIn	Unlink	1	\N	1	2012-08-17	2012-08-18 05:31:19.614092	2012-08-18 05:31:19.614092
346	LinkedIn	Authenticated	1	\N	8	2012-08-23	2012-08-24 05:31:23.83005	2012-08-24 05:31:23.83005
347	LinkedIn	Loaded	1	\N	10	2012-08-23	2012-08-24 05:31:24.061566	2012-08-24 05:31:24.061566
348	LinkedIn	Redirect	1	\N	4	2012-08-23	2012-08-24 05:31:24.068669	2012-08-24 05:31:24.068669
349	LinkedIn	Sign-In	1	\N	5	2012-08-23	2012-08-24 05:31:24.075646	2012-08-24 05:31:24.075646
350	LinkedIn	Unlink	1	\N	4	2012-08-23	2012-08-24 05:31:24.113879	2012-08-24 05:31:24.113879
351	LinkedIn	Authenticated	1	\N	5	2012-08-25	2012-08-26 05:31:20.333582	2012-08-26 05:31:20.333582
352	LinkedIn	Loaded	1	\N	8	2012-08-25	2012-08-26 05:31:20.61664	2012-08-26 05:31:20.61664
353	LinkedIn	Unlink	1	\N	2	2012-08-25	2012-08-26 05:31:20.631474	2012-08-26 05:31:20.631474
354	LinkedIn	Authenticated	1	\N	1	2012-09-01	2012-09-02 05:30:33.593998	2012-09-02 05:30:33.593998
355	LinkedIn	Loaded	1	\N	1	2012-09-01	2012-09-02 05:30:33.727446	2012-09-02 05:30:33.727446
356	LinkedIn	Authenticated	1	\N	5	2012-09-02	2012-09-03 05:30:50.649174	2012-09-03 05:30:50.649174
357	LinkedIn	Loaded	1	\N	5	2012-09-02	2012-09-03 05:30:51.000639	2012-09-03 05:30:51.000639
358	LinkedIn	Authenticated	1	\N	2	2012-09-03	2012-09-04 05:30:33.970302	2012-09-04 05:30:33.970302
359	LinkedIn	Loaded	1	\N	2	2012-09-03	2012-09-04 05:30:34.115062	2012-09-04 05:30:34.115062
360	LinkedIn	Authenticated	1	\N	4	2012-09-05	2012-09-06 05:30:27.737364	2012-09-06 05:30:27.737364
361	LinkedIn	Loaded	1	\N	5	2012-09-05	2012-09-06 05:30:27.90496	2012-09-06 05:30:27.90496
362	LinkedIn	Authenticated	1	\N	8	2012-09-09	2012-09-10 05:30:38.331275	2012-09-10 05:30:38.331275
363	LinkedIn	Loaded	1	\N	13	2012-09-09	2012-09-10 05:30:38.556511	2012-09-10 05:30:38.556511
364	LinkedIn	Authenticated	1	\N	4	2012-09-10	2012-09-11 05:30:35.196681	2012-09-11 05:30:35.196681
365	LinkedIn	Loaded	1	\N	5	2012-09-10	2012-09-11 05:30:35.477657	2012-09-11 05:30:35.477657
366	LinkedIn	Unlink	1	\N	1	2012-09-10	2012-09-11 05:30:35.48989	2012-09-11 05:30:35.48989
367	LinkedIn	Authenticated	1	\N	1	2012-09-11	2012-09-12 05:30:54.804216	2012-09-12 05:30:54.804216
368	LinkedIn	Loaded	1	\N	1	2012-09-11	2012-09-12 05:30:55.013433	2012-09-12 05:30:55.013433
369	LinkedIn	Authenticated	1	\N	1	2012-09-13	2012-09-14 05:30:31.527898	2012-09-14 05:30:31.527898
370	LinkedIn	Loaded	1	\N	1	2012-09-13	2012-09-14 05:30:31.678063	2012-09-14 05:30:31.678063
371	LinkedIn	Authenticated	1	\N	2	2012-09-15	2012-09-16 05:30:35.288047	2012-09-16 05:30:35.288047
372	LinkedIn	Loaded	1	\N	3	2012-09-15	2012-09-16 05:30:35.432321	2012-09-16 05:30:35.432321
373	LinkedIn	Authenticated	1	\N	2	2012-09-16	2012-09-17 05:30:40.994283	2012-09-17 05:30:40.994283
374	LinkedIn	Loaded	1	\N	3	2012-09-16	2012-09-17 05:30:41.191463	2012-09-17 05:30:41.191463
375	LinkedIn	Authenticated	1	\N	5	2012-09-23	2012-09-24 05:30:40.170489	2012-09-24 05:30:40.170489
376	LinkedIn	Find Connections	1	\N	2	2012-09-23	2012-09-24 05:30:40.273397	2012-09-24 05:30:40.273397
377	LinkedIn	Loaded	1	\N	6	2012-09-23	2012-09-24 05:30:40.294127	2012-09-24 05:30:40.294127
378	LinkedIn	Authenticated	1	\N	19	2012-09-29	2012-09-30 05:30:37.152217	2012-09-30 05:30:37.152217
379	LinkedIn	Loaded	1	\N	22	2012-09-29	2012-09-30 05:30:37.346328	2012-09-30 05:30:37.346328
380	LinkedIn	Authenticated	1	\N	8	2012-09-30	2012-10-01 05:30:55.401244	2012-10-01 05:30:55.401244
381	LinkedIn	Loaded	1	\N	8	2012-09-30	2012-10-01 05:30:55.817367	2012-10-01 05:30:55.817367
382	LinkedIn	Authenticated	1	\N	1	2012-10-02	2012-10-03 05:30:33.543491	2012-10-03 05:30:33.543491
383	LinkedIn	Loaded	1	\N	1	2012-10-02	2012-10-03 05:30:33.788961	2012-10-03 05:30:33.788961
384	LinkedIn	Authenticated	1	\N	3	2012-10-03	2012-10-04 05:30:44.246674	2012-10-04 05:30:44.246674
385	LinkedIn	Loaded	1	\N	4	2012-10-03	2012-10-04 05:30:44.379559	2012-10-04 05:30:44.379559
386	LinkedIn	Authenticated	1	\N	6	2012-10-04	2012-10-05 05:30:38.730922	2012-10-05 05:30:38.730922
387	LinkedIn	Loaded	1	\N	7	2012-10-04	2012-10-05 05:30:38.969947	2012-10-05 05:30:38.969947
388	LinkedIn	Loaded	1	\N	2	2012-10-11	2012-10-12 05:30:41.502901	2012-10-12 05:30:41.502901
389	LinkedIn	Authenticated	1	\N	11	2012-10-13	2012-10-14 05:30:35.870178	2012-10-14 05:30:35.870178
390	LinkedIn	Loaded	1	\N	14	2012-10-13	2012-10-14 05:30:36.097073	2012-10-14 05:30:36.097073
391	LinkedIn	Authenticated	1	\N	4	2012-11-18	2012-11-19 05:30:47.141205	2012-11-19 05:30:47.141205
392	LinkedIn	Find Connections	1	\N	21	2012-11-18	2012-11-19 05:30:47.388171	2012-11-19 05:30:47.388171
393	LinkedIn	Loaded	1	\N	5	2012-11-18	2012-11-19 05:30:47.408961	2012-11-19 05:30:47.408961
394	LinkedIn	Authenticated	1	\N	4	2012-11-19	2012-11-20 05:30:44.610598	2012-11-20 05:30:44.610598
395	LinkedIn	Find Connections	1	\N	21	2012-11-19	2012-11-20 05:30:44.928148	2012-11-20 05:30:44.928148
396	LinkedIn	Loaded	1	\N	5	2012-11-19	2012-11-20 05:30:44.951396	2012-11-20 05:30:44.951396
397	LinkedIn	Authenticated	1	\N	4	2012-11-23	2012-11-24 05:30:47.249968	2012-11-24 05:30:47.249968
398	LinkedIn	Find Connections	1	\N	32	2012-11-23	2012-11-24 05:30:47.549694	2012-11-24 05:30:47.549694
399	LinkedIn	Loaded	1	\N	7	2012-11-23	2012-11-24 05:30:47.56393	2012-11-24 05:30:47.56393
400	LinkedIn	Authenticated	1	\N	4	2012-11-24	2012-11-25 05:31:27.989731	2012-11-25 05:31:27.989731
401	LinkedIn	Find Connections	1	\N	32	2012-11-24	2012-11-25 05:31:28.261081	2012-11-25 05:31:28.261081
402	LinkedIn	Loaded	1	\N	8	2012-11-24	2012-11-25 05:31:28.285112	2012-11-25 05:31:28.285112
403	LinkedIn	Loaded	1	\N	1	2012-11-25	2012-11-26 05:30:55.449457	2012-11-26 05:30:55.449457
404	LinkedIn	Loaded	1	\N	1	2012-12-16	2012-12-17 05:30:33.654857	2012-12-17 05:30:33.654857
405	LinkedIn	Authenticated	1	\N	2	2012-12-17	2012-12-18 05:30:40.641975	2012-12-18 05:30:40.641975
406	LinkedIn	Find Connections	1	\N	9	2012-12-17	2012-12-18 05:30:40.807516	2012-12-18 05:30:40.807516
407	LinkedIn	Loaded	1	\N	9	2012-12-17	2012-12-18 05:30:40.889563	2012-12-18 05:30:40.889563
408	LinkedIn	Authenticated	1	\N	2	2012-12-18	2012-12-19 05:30:53.20557	2012-12-19 05:30:53.20557
409	LinkedIn	Find Connections	1	\N	9	2012-12-18	2012-12-19 05:30:53.484027	2012-12-19 05:30:53.484027
410	LinkedIn	Loaded	1	\N	8	2012-12-18	2012-12-19 05:30:53.500984	2012-12-19 05:30:53.500984
411	LinkedIn	Authenticated	1	\N	2	2013-01-12	2013-01-13 05:31:33.617293	2013-01-13 05:31:33.617293
412	LinkedIn	Find Connections	1	\N	20	2013-01-12	2013-01-13 05:31:33.941322	2013-01-13 05:31:33.941322
413	LinkedIn	Loaded	1	\N	4	2013-01-12	2013-01-13 05:31:33.962863	2013-01-13 05:31:33.962863
414	LinkedIn	Sign-In	1	\N	1	2013-01-12	2013-01-13 05:31:33.981714	2013-01-13 05:31:33.981714
415	LinkedIn	Authenticated	1	\N	3	2013-01-13	2013-01-14 05:31:04.731703	2013-01-14 05:31:04.731703
416	LinkedIn	Find Connections	1	\N	27	2013-01-13	2013-01-14 05:31:04.925018	2013-01-14 05:31:04.925018
417	LinkedIn	Loaded	1	\N	5	2013-01-13	2013-01-14 05:31:04.956038	2013-01-14 05:31:04.956038
418	LinkedIn	Sign-In	1	\N	1	2013-01-13	2013-01-14 05:31:04.967634	2013-01-14 05:31:04.967634
419	LinkedIn	Sign in button displayed	1	\N	21	2013-04-22	2013-04-23 03:16:12.661799	2013-04-23 03:16:12.661799
422	LinkedIn	Sign in button displayed	1	\N	21	2013-04-22	2013-04-23 03:50:45.370678	2013-04-23 03:50:45.370678
425	LinkedIn	Sign in button displayed	1	\N	21	2013-04-22	2013-04-23 03:56:53.012923	2013-04-23 03:56:53.012923
426	LinkedIn	Sign in with LinkedIn	1	\N	0	2013-04-22	2013-04-23 03:56:53.333547	2013-04-23 03:56:53.333547
427	LinkedIn	View LinkedIn connections	1	\N	0	2013-04-22	2013-04-23 03:56:53.508416	2013-04-23 03:56:53.508416
436	LinkedIn	Sign in button displayed	1	\N	121	2013-04-23	2013-04-23 20:14:07.953082	2013-04-23 20:14:07.953082
437	LinkedIn	Sign in with LinkedIn	1	\N	31	2013-04-23	2013-04-23 20:14:08.423645	2013-04-23 20:14:08.423645
438	LinkedIn	View LinkedIn connections	1	\N	0	2013-04-23	2013-04-23 20:14:08.714532	2013-04-23 20:14:08.714532
439	LinkedIn	Sign in button displayed	1	\N	25	2013-04-24	2013-04-24 16:14:04.482922	2013-04-24 16:14:04.482922
440	LinkedIn	Sign in with LinkedIn	1	\N	12	2013-04-24	2013-04-24 16:14:04.94435	2013-04-24 16:14:04.94435
441	LinkedIn	View LinkedIn connections	1	\N	0	2013-04-24	2013-04-24 16:14:05.253824	2013-04-24 16:14:05.253824
442	LinkedIn	Sign in button displayed	1	\N	16	2013-04-25	2013-04-25 17:47:41.358667	2013-04-25 17:47:41.358667
443	LinkedIn	Sign in with LinkedIn	1	\N	4	2013-04-25	2013-04-25 17:47:41.843815	2013-04-25 17:47:41.843815
444	LinkedIn	View LinkedIn connections	1	\N	0	2013-04-25	2013-04-25 17:47:42.119258	2013-04-25 17:47:42.119258
445	LinkedIn	Sign in button displayed	1	\N	0	2013-04-28	2013-04-29 02:40:56.015244	2013-04-29 02:40:56.015244
446	LinkedIn	Sign in with LinkedIn	1	\N	1	2013-04-28	2013-04-29 02:40:56.645595	2013-04-29 02:40:56.645595
447	LinkedIn	View LinkedIn connections	1	\N	0	2013-04-28	2013-04-29 02:40:57.034201	2013-04-29 02:40:57.034201
448	LinkedIn	Sign in button displayed	1	\N	0	2013-07-21	2013-07-22 03:47:57.205354	2013-07-22 03:47:57.205354
449	LinkedIn	View Relationship	1	\N	0	2013-07-21	2013-07-22 03:47:58.012818	2013-07-22 03:47:58.012818
450	LinkedIn	Sign in with LinkedIn	1	\N	0	2013-07-21	2013-07-22 03:47:58.582879	2013-07-22 03:47:58.582879
451	LinkedIn	View company connections	1	\N	0	2013-07-21	2013-07-22 03:47:59.135862	2013-07-22 03:47:59.135862
\.


--
-- Name: tracking_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('tracking_events_id_seq', 451, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: jbc
--

COPY users (id, uuid, job_board_id, linkedin_id, created_at, updated_at, user_type, authentication_token) FROM stdin;
23	8qGobFdEVPB_Yv6HG0Mu7A	1	cQQuqfL0UF	2013-09-08 22:35:40.502	2013-09-08 22:35:40.502	job_seeker	\N
24	5XmN7gpri7TvtHFGjsjx6A	1	pgjbgILgYe	2013-10-01 03:16:08.148498	2013-10-01 03:16:08.148498	job_seeker	\N
22	duAM4Q6vqtS-_gDrSidPsg	1	hoss8Lf8rA	2013-09-08 15:38:58.717565	2013-12-02 05:43:04.645508	job_seeker	wpjzXzmBAbsyNKlRsQWWUw
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jbc
--

SELECT pg_catalog.setval('users_id_seq', 24, true);


--
-- Name: clients_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: job_boards_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY job_boards
    ADD CONSTRAINT job_boards_pkey PRIMARY KEY (id);


--
-- Name: js_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY js_versions
    ADD CONSTRAINT js_versions_pkey PRIMARY KEY (id);


--
-- Name: sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: templates_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY templates
    ADD CONSTRAINT templates_pkey PRIMARY KEY (id);


--
-- Name: tracking_events_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY tracking_events
    ADD CONSTRAINT tracking_events_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: jbc; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE INDEX delayed_jobs_priority ON delayed_jobs USING btree (priority, run_at);


--
-- Name: index_clients_on_authentication_token; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_clients_on_authentication_token ON clients USING btree (authentication_token);


--
-- Name: index_clients_on_email; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_clients_on_email ON clients USING btree (email);


--
-- Name: index_clients_on_reset_password_token; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_clients_on_reset_password_token ON clients USING btree (reset_password_token);


--
-- Name: index_job_boards_on_client_id; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE INDEX index_job_boards_on_client_id ON job_boards USING btree (client_id);


--
-- Name: index_job_boards_on_uuid; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_job_boards_on_uuid ON job_boards USING btree (uuid);


--
-- Name: index_js_versions_on_created_at; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE INDEX index_js_versions_on_created_at ON js_versions USING btree (created_at);


--
-- Name: index_js_versions_on_version; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_js_versions_on_version ON js_versions USING btree (version);


--
-- Name: index_sessions_on_user_id; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_sessions_on_user_id ON sessions USING btree (user_id);


--
-- Name: index_sessions_on_uuid; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_sessions_on_uuid ON sessions USING btree (uuid);


--
-- Name: index_templates_on_job_board_id; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE INDEX index_templates_on_job_board_id ON templates USING btree (job_board_id);


--
-- Name: index_tracking_events_on_client_category_date; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE INDEX index_tracking_events_on_client_category_date ON tracking_events USING btree (job_board_id, category, action, recorded_at);


--
-- Name: index_users_on_authentication_token; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_authentication_token ON users USING btree (authentication_token);


--
-- Name: index_users_on_job_board_id_and_linkedin_id; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_job_board_id_and_linkedin_id ON users USING btree (job_board_id, linkedin_id);


--
-- Name: index_users_on_uuid; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_uuid ON users USING btree (uuid);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: jbc; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: public; Type: ACL; Schema: -; Owner: tvu
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM tvu;
GRANT ALL ON SCHEMA public TO tvu;
GRANT ALL ON SCHEMA public TO jbc;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

